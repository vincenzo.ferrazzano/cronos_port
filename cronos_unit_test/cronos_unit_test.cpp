#include "pch.h"
#include "CppUnitTest.h"
#include "arma_model.hpp"
#include <array>
using namespace Microsoft::VisualStudio::CppUnitTestFramework;
typedef std::vector<double> dvec;
template<size_t N, typename T>
using double_array = std::array<std::array<T, N>, N>;

template<size_t N, typename T = double>
using array = std::array<T, N>;

namespace cronosunittest
{
#define STRINGIFY(x) #x
#define EXPAND(x) STRINGIFY(x)
	const int N = 5;

	TEST_CLASS(ARMATests)
	{
		//It keeps track of the tests who originally yield different results than the python implementation
		bool static should_test_fail(int p, int q)
		{
			// Direct porting of cronos yields different results for these cases:
			//		  (2, 2),		  (2, 4),
			//(3, 1),		  (3, 3), (3, 4),
			//(4, 1), (4, 2), (4, 3), (4, 4)
			bool res = false;
			Assert::IsTrue(p < N&& q < N);
			switch (p)
			{
			case 2:
				res = q == 2|| q==4;
				break;
			case 3:
				res = q >=3 || q==1;
				break;
			case 4:
				res = q >= 2;
				break;
			default:
				break;
			}
			return res;
		}
		//Auxiliary function to fill a TS from file
		void static fill_ts(const std::string& filename, TS::TimeSeries& ts)
		{
			std::ifstream infile(filename);
			Assert::IsTrue(infile.good());
			infile >> ts;
		}

		static double compute_ma_acf(int lag, const QuantLib::Array& coeff, double sigma, bool normalized)
		{
			const double sigma2 = sigma * sigma;
			const size_t L = coeff.size();
			double res = 0;
			if (lag == 0) {
				res = 1;
				if (!normalized) {
					for (int p = 0; p < L; ++p)
					{
						res += coeff[p] * coeff[p];
					}
					res *= sigma2;
				}
			}
			else {
				if (lag <= L)
				{
					res = coeff[lag - 1];
					for (int j = 0; j < L - lag; ++j)
					{
						res += coeff[j] * coeff[j + lag];
					}
				}
				res = sigma2 * res;
				if (normalized)
				{
					res /= compute_ma_acf(0, coeff, sigma, false);
				}
			}
			return res;
		}
		///reference log likelihood values, for ref_loglikelihood[p][q], where p and q are the
		///values for the corresponding arma(p,q) model, as fitted by python package
		const static inline
			double_array<N, double> ref_loglikelihood = { array<N>{-15772.572965,-14547.400154,-13948.446033,-13631.575319,-13432.71034},
														array<N>{-12884.332177,-12698.077578,-12678.340428,-12678.314538,-12675.238095},
														array<N>{-12766.637763,-12680.386557,-12678.326052,-12614.251383,-12606.029485},
														array<N>{-12710.458257,-12677.304314,-12604.733039,-12604.702337,-12604.665002},
														array<N>{-12683.13263,-12675.417538,-12676.137482,-12603.710876,-12603.709997}
		};

		const static inline
			double_array<N, double> ref_sigma = { array<N>{44.407665,  29.618758,  24.298467,    21.8821,  20.489914},
												  array<N>{17.0922,  16.071034,  15.966477,  15.966338,  15.950071},
												  array<N>{16.439755,  15.977297,    15.9664,  15.629938,  15.587397},
												  array<N>{16.137064,  15.960994,  15.580717,  15.580564,  15.580379},
												  array<N>{15.99181,  15.951024,  15.954503 ,  15.57513,  15.575118}
		};
		const static inline double_array<N, dvec> ref_ar_coeff = {
			std::array<dvec, N>{							//p=0
				dvec{},											//q=0
				dvec{},											//q=1
				dvec{},											//q=2
				dvec{},											//q=3
				dvec{}},										//q=4
			std::array<dvec, N>{							//p=1
				dvec{0.922719},									//q=0
				dvec{0.978480},									//q=1
				dvec{0.982928},									//q=2
				dvec{0.982835},									//q=3
				dvec{0.981961}},								//q=4
			std::array<dvec, N>{							//p=2
				dvec{0.670206,0.273543},						//q=0
				dvec{1.189688,-0.203155},						//q=1
				dvec{0.962753,0.019780},						//q=2
				dvec{1.975343,-0.977694},						//q=3
				dvec{1.974026,-0.976550}},						//q=4
			std::array<dvec, N>{							//p=3
				dvec{0.617894,0.145606, 0.190910},				//q=0
				dvec{1.129965,-0.207860, 0.061987},				//q=1
				dvec{2.330082,-1.680473, 0.348701},				//q=2
				dvec{2.341170,-1.702471, 0.359634},				//q=3
				dvec{2.402021, -1.822672, 0.419135 }},			//q=4
			std::array<dvec, N>{							//p=4
				dvec{0.592312,0.126095,0.108267,0.133775},		//q=0
				dvec{1.023812,-0.141153,0.043982, 0.053618},	//q=1
				dvec{0.131559,0.916796,-0.145549,0.065267},		//q=2
				dvec{1.333675,0.644147,-1.331558,0.350362 },	//q=3
				dvec{1.336503,0.641423,-1.334511,0.353225 }}	//q=4
		};
		const static inline double_array<N, dvec> ref_ma_coeff = {
			std::array<dvec, N>{							//p=0
				dvec{},											//q=0
				dvec{0.723082},									//q=1
				dvec{0.933873,0.515267},						//q=2
				dvec{0.913303,0.733286, 0.406940},				//q=3
				dvec{0.912955,0.804286, 0.580769,0.323661} },	//q=4
			std::array<dvec, N>{							//p=1
				dvec{},											//q=0
				dvec{-0.449463},								//q=1
				dvec{-0.399967,-0.110200},						//q=2
				dvec{-0.400730,-0.111936, 0.003986},			//q=3
				dvec{-0.401597,-0.120099, -0.014732, 0.042363}},//q=4
			std::array<dvec, N>{							//p=2
				dvec{},											//q=0
				dvec{-0.613086},								//q=1
				dvec{-0.380258,-0.119233},						//q=2
				dvec{-1.453703,0.277289,0.201468},				//q=3
				dvec{-1.440423,0.297755, 0.098616, 0.072219} }, //q=4
			std::array<dvec, N>{							//p=3
				dvec{},											//q=0
				dvec{-0.546798},								//q=1
				dvec{-1.794853, 0.814422},						//q=2
				dvec{-1.806792,0.834519, -0.008357},			//q=3
				dvec{-1.867667,0.920819, -0.019504,-0.015917} },//q=3
			std::array<dvec, N>{							//p=4
				dvec{},											//q=0
				dvec{-0.442108},								//q=1
				dvec{0.452348, -0.542449},						//q=2
				dvec{-0.798324,-0.975215 ,0.812637},			//q=3
				dvec{-0.801414, -0.973177, 0.815799,-0.002206}},//q=4
		};

	public:
		TS::TimeSeries sunspot_ts;
		TS::TimeSeries rainfall_ts;

		const static int num_iteration = 5000;
		const double param_abs_tol = 1e-3;
		const double ll_rel_tol = 0.001;
		const int acf_lags = 12;

		static
		TS::ARMAModel fit_model(int p, int q, TS::TimeSeries& ts, int method_type = TS::METHOD_MLE) {
			TS::ARMAModel model;
			model.SetOrder(p, q);
			model.SetFracDiff(0.0);
			std::ostringstream res, supplemental;

			const int result = model.FitModel(ts,
			                                  method_type,
			                                  num_iteration,
			                                  nullptr,
			                                  nullptr,
			                                  res,
			                                  supplemental,
			                                  true);

			if (result == 0)
			{
				Logger::WriteMessage(res.str().c_str());
				std::cout << supplemental.str() << "\n";
			}
			Assert::IsTrue(model.CheckModel(true));
			Assert::IsTrue(result);
			return model;
		}

		ARMATests()
		{	//Constructor: loading the data file
			std::string proj_dir = EXPAND(PROJDIR);
			proj_dir.erase(0, 1); // erase the first quote
			proj_dir.erase(proj_dir.size() - 2); // erase the last quote and the dot
			const std::string sunspots_path = proj_dir + "deps\\cronos\\data\\sunspots.dat";
			fill_ts(sunspots_path, this->sunspot_ts);
			const std::string rainfall_path = proj_dir + "deps\\cronos\\data\\rainfall.dat";
			fill_ts(rainfall_path, this->rainfall_ts);
		}

		TEST_METHOD(CompareLL)
		{
			QuantLib::Array cronos_ar_coeff;
			QuantLib::Array cronos_ma_coeff;
			const int N_test = 5; /*Working only for p,q<=2. I suspect due to subpar optimization routine*/
			for (int p = 0; p < N_test; p++) {
				for (int q = 0; q < N_test; q++) {
					const std::string model_str = "ARMA(" + std::to_string(p) + "," + std::to_string(q) + ")";
					Logger::WriteMessage(("Running " + model_str+"... ").c_str());
					auto model = fit_model(p, q, this->sunspot_ts, TS::METHOD_MLE);
					const auto cronos_ll = model.GetLogLikelihood();
					const auto ref_ll = ref_loglikelihood[p][q];
					const double ll_rel_diff = (cronos_ll - ref_ll) / ref_ll;
					// Currently results are comparable only some p and q, probably due to bad optimization routine
					// if that changes, the test fail. Update the function below accordingly
					const bool failure = should_test_fail(p, q);
					const double ref_sigma_value = ref_sigma[p][q];
					model.GetCoefficients(cronos_ar_coeff, cronos_ma_coeff);
					const auto& l_ref_ma_coeff = ref_ma_coeff[p][q];
					const auto& l_ref_ar_coeff = ref_ar_coeff[p][q];
					if (std::abs(ll_rel_diff) < ll_rel_tol) { //if log-likelihood are similar
						Assert::AreEqual(0.0, ll_rel_diff, ll_rel_tol);
						//Checking ma coefficients
						for (int lq = 0; lq < q; lq++)
						{
							Assert::AreEqual(l_ref_ma_coeff[lq], cronos_ma_coeff[lq], param_abs_tol);
						}
						//Checking ar coefficients
						for (int lp = 0; lp < p; lp++)
						{
							Assert::AreEqual(l_ref_ar_coeff[lp], cronos_ar_coeff[lp], param_abs_tol);
						}
						//Checking innovations errors
						Assert::AreEqual(ref_sigma_value, model.GetSigma(), param_abs_tol);
						Assert::IsTrue(!failure);
						Logger::WriteMessage("seems ok!\n");
					}
					else {
						std::string msg;
						if (cronos_ll < ref_ll)
						{
							msg = "Cronos provides worse LL than python for ";
							//Check if sigma is consistent with the LL
							Assert::IsTrue(model.GetSigma() > ref_sigma_value);
						}
						else
						{
							msg = "Cronos provides better LL than python for ";
							Assert::IsTrue(model.GetSigma() < ref_sigma_value);
							std::string d_msg = "Currently results are always worse. If this happens.. Hurray! Remove the assert";
							Logger::WriteMessage(d_msg.c_str());
							Assert::IsTrue(false);
						}
						Assert::IsTrue(failure);
						const double scale = 1000.0;
						auto ll_rel_diff_str = std::to_string(static_cast<int>(ll_rel_diff * scale) / scale * 100);
						Logger::WriteMessage((msg + model_str + ", we cannot really compare the results. ").c_str());
						double max_diff_q=0;
						for (int lq = 0; lq < q; lq++)
						{
							max_diff_q = std::max(max_diff_q, abs(l_ref_ma_coeff[lq] - cronos_ma_coeff[lq]));
						}
						if (q > 0) {
							Logger::WriteMessage((", Max diff theta: " + std::to_string(max_diff_q)).c_str());
						}
						double max_diff_p = 0;
						for (int lp = 0; lp < p; lp++)
						{
							max_diff_p = std::max(max_diff_p, std::abs(l_ref_ar_coeff[lp] - cronos_ar_coeff[lp]));
						}
						Logger::WriteMessage(("LL rel difference: " + ll_rel_diff_str + "%").c_str());
						if (p > 0) {
							Logger::WriteMessage((", Max diff phi: " + std::to_string(max_diff_p)).c_str());
						}
						Logger::WriteMessage("\n");

					}
				}
			}
		}
		
		TEST_METHOD(AR1_ACF)
		{
			QuantLib::Array cronos_ar_coeff;
			QuantLib::Array cronos_ma_coeff;
			auto model = fit_model(1, 0, this->sunspot_ts, TS::METHOD_MLE);
			model.GetCoefficients(cronos_ar_coeff, cronos_ma_coeff);
			const double phi = cronos_ar_coeff[0];
			QuantLib::Array acf(acf_lags);
			model.ComputeACFPACF(acf, nullptr, true);
			//Expected form: phi^i
			for (int i = 0; i < acf_lags; ++i)
			{
				Assert::AreEqual(std::pow(phi, i), acf[i], this->param_abs_tol);
			}
		}
		
		TEST_METHOD(MA1_ACF)
		{
			QuantLib::Array cronos_ar_coeff;
			QuantLib::Array cronos_ma_coeff;
			auto model = fit_model(0, 1, this->rainfall_ts, TS::METHOD_MLE);
			model.GetCoefficients(cronos_ar_coeff, cronos_ma_coeff);
			const double theta = cronos_ma_coeff[0];
			QuantLib::Array acf(acf_lags);
			model.ComputeACFPACF(acf, nullptr, true);
			const double normalization = 1.0 + theta * theta;
			//Expected form: 1, theta/(1+theta^2), 0, ... , 0
			Assert::AreEqual(1.0, acf[0], this->param_abs_tol);
			Assert::AreEqual(theta / normalization, acf[1], this->param_abs_tol);
			for (int i = 2; i < acf_lags; ++i)
			{
				Assert::AreEqual(0.0, acf[i], this->param_abs_tol);
			}
		}
		
		TEST_METHOD(MA2_ACF)
		{
			QuantLib::Array cronos_ar_coeff;
			QuantLib::Array cronos_ma_coeff;
			auto model = fit_model(0, 2, this->rainfall_ts, TS::METHOD_MLE);
			model.GetCoefficients(cronos_ar_coeff, cronos_ma_coeff);
			const double theta1 = cronos_ma_coeff[0];
			const double theta2 = cronos_ma_coeff[1];

			QuantLib::Array acf(acf_lags);
			model.ComputeACFPACF(acf, nullptr, true);
			const double normalization = 1.0 + theta1 * theta1 + theta2 * theta2;
			//Expected form: 1, (theta1 + theta1 * theta2)/normalization, (theta2)/normalization, ... , 0
			Assert::AreEqual(1.0, acf[0], this->param_abs_tol);
			Assert::AreEqual((theta1 + theta1 * theta2) / normalization, acf[1], this->param_abs_tol);
			Assert::AreEqual(theta2 / normalization, acf[2], this->param_abs_tol);
			for (int i = 3; i < acf_lags; ++i)
			{
				Assert::AreEqual(0.0, acf[i], this->param_abs_tol);
			}
		}
		
		TEST_METHOD(MA_ACF_Sunspots)
		{
			QuantLib::Array cronos_ar_coeff;
			QuantLib::Array cronos_ma_coeff;
			for (int q = 1; q < 10; ++q) {
				auto model = fit_model(0, q, this->sunspot_ts, TS::METHOD_MLE);
				model.GetCoefficients(cronos_ar_coeff, cronos_ma_coeff);
				const double sigma = model.GetSigma();
				QuantLib::Array acf(acf_lags);
				model.ComputeACFPACF(acf, nullptr, true);
				for (int i = 0; i < acf_lags; ++i)
				{
					const double expected_val = compute_ma_acf(i, cronos_ma_coeff, sigma, true);
					Assert::AreEqual(expected_val,
						acf[i],
						this->param_abs_tol);
				}
			}
		}
		
		TEST_METHOD(ARMA21_ACF)
		{
			//From Brockwell & Davis, Example 3.3.1
			TS::ARMAModel model;
			model.SetOrder(2, 1);
			// Model
			// (1-B+1/4 B^2) X_t = (1+B)Z_t
			// notice the inverted sign on the ar_coeffs
			std::array<double, 2> ar_coeff{ 1.0,-1.0 / 4.0 };
			std::array<double, 1> ma_coeff{ 1.0 };
			model.SetCoefficients(ar_coeff.data(), ma_coeff.data());
			const double sigma2 = 3.14;
			model.SetSigma2(sigma2);
			QuantLib::Array acf(acf_lags);
			model.ComputeACFPACF(acf, nullptr, true);
			auto th_acf = [](int lag, double sigma2, bool normalize)
			{
				const double variance = sigma2 * 32.0 / 3.0;
				double res = sigma2 * std::pow(2.0, -lag) * (32.0 / 3.0 + 8.0 * lag);
				if (normalize)
				{
					res = res / variance;
				}
				return res;
			};
			//Normalized
			for (int i = 0; i < acf_lags; ++i)
			{
				Assert::AreEqual(th_acf(i, sigma2, true), acf[i], this->param_abs_tol);
			}
			//not normalized
			model.ComputeACFPACF(acf, nullptr, false);
			for (int i = 0; i < acf_lags; ++i)
			{
				Assert::AreEqual(th_acf(i, sigma2, false), acf[i], this->param_abs_tol);
			}
		}
	};
}
