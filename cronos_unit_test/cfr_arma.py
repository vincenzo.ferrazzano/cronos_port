import pandas as pd
import numpy as np
from statsmodels.tsa.arima_model import ARMA
import warnings
warnings.simplefilter("ignore")

if __name__ == "__main__":
    ar_min = 0
    ar_max = 5
    ma_min = 0
    ma_max = 5
    filename = ".\deps\cronos\data\sunspots.dat"
    data = pd.read_csv(filename, header=None, skiprows=7)[0]
    loglike_df = pd.DataFrame(index=range(ar_min, ar_max),
                              columns=range(ma_min, ma_max))
    parmas = {}
    for p in loglike_df.index:
        for q in loglike_df.columns:
            model = ARMA(data, order=(p, q))
            fit_res = model.fit(full_output=0)
            loglike_df[q][p] = fit_res.llf
            c_pars = np.hstack((fit_res.arparams, fit_res.maparams))
            para_df = fit_res.bse[1:]
            para_df.name = "std"
            para_df = para_df.to_frame()
            para_df["values"] = c_pars
            parmas[(p, q)] = para_df[["values", "std"]]
            # print(p, q, para_df)

    print(loglike_df)
    for p in loglike_df.index:
        for q in loglike_df.columns:
            print(f"---- {p}, {q} ----")
            print(parmas[(p, q)])
