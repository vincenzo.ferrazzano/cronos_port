set msbuild2019="%VS2019INSTALLDIR%\MSBuild\Current\Bin\msbuild.exe"
set origDir=%CD%
mkdir ..\build\lib & mkdir ..\build\lib\Release\ & mkdir ..\build\lib\Debug
call nlopt_compile.bat
cd %origDir%

cd boost
call bootstrap
call .\b2 --address-model=64 --architecture=ia64 -j 8
call .\b2 --address-model=64 --architecture=ia64 --with-test -j 8
mkdir ..\build\lib\boost_lib
copy stage\lib\*.* ..\..\build\lib\boost_lib\.

cd %origDir%
call quantlib_compile.bat
rmdir /Q/S boost\stage