set msbuild2019="%VS2019INSTALLDIR%\MSBuild\Current\Bin\msbuild.exe"
echo "*** BUILDING QUANTLIB ***"

cd quantlib
mkdir build & cd build
cmake -G "Visual Studio 16 2019" -DCMAKE_CXX_FLAGS="/EHsc /MP" -DBoost_INCLUDE_DIR=%~dp0boost -DBUILD_TESTING=OFF -DCMAKE_INSTALL_PREFIX=%~dp0..\build\ .. -B . 

%msbuild2019% INSTALL.vcxproj /p:configuration=debug
move ..\..\..\build\lib\QuantLib-x64-mt-gd.lib ..\..\..\build\lib\Debug

%msbuild2019% INSTALL.vcxproj /p:configuration=release
move ..\..\..\build\lib\QuantLib-x64-mt.lib ..\..\..\build\lib\Release

cd ..\..
rmdir /Q/S quantlib\build