set msbuild2019="%VS2019INSTALLDIR%\MSBuild\Current\Bin\msbuild.exe"
echo "*** BUILDING NLOPT ***"
cd %~dp0\nlopt\
mkdir build & cd build
cmake -G "Visual Studio 16 2019" -DBUILD_SHARED_LIBS=OFF -DCMAKE_INSTALL_PREFIX=%~dp0..\build\ .. -B . 
%msbuild2019% INSTALL.vcxproj /t:build /p:configuration=debug /m:4
move ..\..\..\build\lib\nlopt.lib ..\..\..\build\lib\Debug
%msbuild2019% INSTALL.vcxproj /t:build /p:configuration=release /m:4
move ..\..\..\build\lib\nlopt.lib ..\..\..\build\lib\Release
cd ..\..
rmdir /Q/S nlopt\build