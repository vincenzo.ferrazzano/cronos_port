/*
 * -------------------------------------------------------------------
 *
 * Copyright 2004,2005,2006 Anthony Brockwell
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * -------------------------------------------------------------------
 *
 * This module contains the source code defining a
 * TimeSeries class.  Operations for reading from a file,
 * writing to a file, etc. are included.
 * The module  also contains the
 * base TimeSeriesModel and derived ARMAModel class.
 */

#include "pch.hpp"
#include "timeseries.hpp"
#include "conversion_layer.hpp"

//--------------------------------------------------+
// TimeSeries stuff                                 :
//--------------------------------------------------+

TS::TimeSeries::TimeSeries():
						 data(n_components,1),
						 missing(n_components, 1)
{
}

TS::TimeSeries::TimeSeries(int nobs, int n_components = 1) :
	current_length(nobs),
	nallocated(nobs),
	n_components(n_components),
	data(n_components, nobs),
	missing(n_components, nobs)
{
}


bool TS::TimeSeries::HasAnyMissing() const
{
	return anymissing;
}

QuantLib::Array TS::TimeSeries::GetDataVector() const
{
	QuantLib::Array temp(current_length);
	for (int i = 0; i < current_length; ++i)
		temp[i] = data[current_component][i];
	return temp;
}

QuantLib::Matrix TS::TimeSeries::GetDataMatrix() const
{
	QuantLib::Matrix tm(current_length, n_components);
	for (int i = 0; i < current_length; ++i)
		for (int j = 0; j < n_components; ++j)
			tm[i][j] = data[j][i];
	return tm;
}

QuantLib::Array TS::TimeSeries::GetMissingVector() const
{
	QuantLib::Array temp(current_length);
	for (int i = 0; i < current_length; ++i)
		temp[i] = missing[current_component][i];
	return temp;
}

QuantLib::Matrix TS::TimeSeries::GetMissingMatrix() const
{
	QuantLib::Matrix tm(current_length, n_components);
	for (int i = 0; i < current_length; ++i)
		for (int j = 0; j < n_components; ++j)
			tm[i][j] = missing[j][i];
	return tm;
}

void TS::TimeSeries::SetMissingVector(QuantLib::Array& mv)
{
	anymissing = false;
	for (int i = 0; i < current_length; ++i)
	{
		missing[current_component][i] = (mv[i] != 0);
		anymissing |= (mv[i] != 0);
	}
}

void TS::TimeSeries::SetMissing(int t, bool msng)
{
	missing[current_component][t] = msng;
	anymissing |= msng;
}

void TS::TimeSeries::ClearOut(int dim)     // wipes it out and sets dimension
{
	//throw std::runtime_error("NotImplemented");
	current_length = 0;
	anymissing = false;
	nallocated = 0;
	description_ = "";   title_ = "";
	if (dim != 0)
		n_components = dim;
	data = ql_resize(data,n_components, 1);
	missing = ql_resize(missing, n_components, 1);
}

double* TS::TimeSeries::GetData() const
{
	//return data[current_component][0];
	return &data(current_component, 0);
}

double& TS::TimeSeries::operator[](int index) const
{
	//return data[current_component][index];
	return data( current_component, index);
}

bool TS::TimeSeries::IsMissing(int t) const
{
	//return missing[current_component][t];
	return missing(current_component, t);
}

bool TS::TimeSeries::IsMissing(int component, int t) const
{
	//return missing[component][t];
	return missing(component, t);
}

double TS::TimeSeries::SampleMean() const
{
	int i/*, n = GetN()*/, m;
	double total = 0.0;
	for (i = 0, m = 0; i < current_length; ++i)
		if (!missing[current_component][i])
		{
			total += data[current_component][i];
			++m;
		}
	if (m > 0)
		total /= m;
	return total;
}

double TS::TimeSeries::SampleMin() const
{
	double min = 1e100;
	for (int i = 0; i < current_length; ++i)
		if (!missing[current_component][i])
			if (data[current_component][i] < min)
				min = data[current_component][i];
	return min;
}

QuantLib::Matrix Innovations(double& retvhat, int maxm, const QuantLib::Array& acfs)
{
	int j;
	QuantLib::Matrix thetas(maxm, maxm);
	QuantLib::Array vhats(maxm + 1);
	vhats[0] = acfs[0];
	for (int m = 1; m <= maxm; ++m)
	{
		// compute thetahat(i,1,...,i)
		for (int k = 0; k < m; ++k)
		{
			thetas[m - 1][m - k - 1] = acfs[m - k];
			for (j = 0; j < k; ++j)
				thetas[m - 1][m - k - 1] -= thetas[m - 1][m - j - 1] * thetas[k - 1][k - j - 1]
				* vhats[j];
			thetas[m - 1][m - k - 1] /= vhats[k];
		}
		vhats[m] = acfs[0];
		for (j = 0; j < m; ++j)
			vhats[m] -= thetas[m - 1][m - j - 1] * thetas[m - 1][m - j - 1] * vhats[j];
	}

	retvhat = vhats[maxm];
	return thetas;
}
/// <summary>
/// Computes the sample ACF from the data
/// TODO: Consider doing if via FFT
/// </summary>
/// <param name="acv_function">ACF vector, to be filled</param>
/// <param name="pacf_storage">optional PACF vector, to be filled</param>
/// <param name="normalize_acf">Ensure that the ACF is normalized</param>
void TS::TimeSeries::ComputeSampleACF(QuantLib::Array& acv_function , QuantLib::Array* pacf_storage,
	bool normalize_acf) const
{
	int i;
	size_t j;
	const size_t maxlag = acv_function.size() - 1;
	if (current_length == 0)
		return;
	const double mean = SampleMean();
	double total;
	//double* tdata = &data[current_component][0];
	//double* tmissing = &missing[current_component][0];
	double* tdata = &data(current_component,0);
	double* tmissing = &missing(current_component,0);
	// First compute the sample autocovariance function.
	if (!HasAnyMissing())
		for (i = 0; i <= maxlag; ++i)
		{
			total = 0;
			for (j = i; j < current_length; ++j)
				total += (tdata[j] - mean) * (tdata[j - i] - mean);
			acv_function[i] = total / current_length;
		}
	else
	{
		int non_missing = 0;
		for (i = 0; i < current_length; ++i)
			non_missing += static_cast<int>(floor(1.5 - tmissing[i]));
		for (i = 0; i <= maxlag; ++i)
		{
			total = 0;
			for (j = i; j < current_length; ++j)
				if ((tmissing[j] == 0) && (tmissing[j - i] == 0))
					total += (tdata[j] - mean) * (tdata[j - i] - mean);
			acv_function[i] = total / non_missing;
		}
	}

	// then the sample PACF using the Durbin-Levinson algorithm (p.169 B&D)
	if (pacf_storage != nullptr)
		ACFtoPACF(acv_function, *pacf_storage);

	// now normalize the sample ACFs
	if (normalize_acf) {
		for (auto& v : acv_function) {
			v /= acv_function[0];
		}
	}
}

void TS::TimeSeries::Append(double x, bool miss)
{
	//int i;
	//double* tx;
	//bool* tb;

	if (n_components > 1)
	{
		std::cout << "Error: appending scalar to multivariate TS." << std::endl;
		return;
	}

	if (nallocated < current_length + 1)
	{
		nallocated += alloc_unit;
		data = ql_resize(data,n_components, nallocated);
		missing = ql_resize(missing,n_components, nallocated);
	}

	data[0][current_length] = x;
	missing[0][current_length++] = miss;
	anymissing |= miss;
}

void TS::TimeSeries::SetValues(int pos, double x, bool miss)
{
	//int i;
	//double* tx;
	//bool* tb;

	if (n_components > 1)
	{
		std::cout << "Error: appending scalar to multivariate TS." << std::endl;
		return;
	}
	if (pos>=current_length)
	{
		std::cout << "Exceeding boundaries" << std::endl;
		return;
	}
	
	data[0][pos] = x;
	missing[0][pos] = miss;
	anymissing |= miss;
}


void TS::TimeSeries::Append(QuantLib::Array& x, QuantLib::Array& miss)
{
	//throw std::runtime_error("NotImplemented");

	if (n_components != x.size())
	{
		std::cout << "Error: Append() dimension mismatch." << std::endl;
		return;
	}

	if (nallocated < current_length + 1)
	{
		nallocated += alloc_unit;
		data = ql_resize(data,n_components, nallocated);
		missing = ql_resize(missing,n_components, nallocated);
	}

	for (int k = 0; k < x.size(); ++k)
	{
		data[k][current_length] = x[k];
		missing[k][current_length] = miss[k];
		anymissing = (miss[k] == 1);
	}
	++current_length;
}


void TS::TimeSeries::Clip(int /*n0*/, int /*n1*/)
{
	throw std::runtime_error("NotImplemented");
	//int i;
	//data = data.submatrix(0, n0, n_components, n1 + 1);
	//missing = missing.submatrix(0, n0, n_components, n1 + 1);
	//n = (n1 - n0 + 1);
	//nallocated = n;
}


TS::TimeSeries& TS::TimeSeries::operator=(const TimeSeries& other)
{
	//int i;

	ClearOut();
	current_length = other.current_length;
	nallocated = other.nallocated;
	if (current_length > 0) {
		data = other.data;
		missing = other.missing;
		nallocated = current_length;
	}
	anymissing = other.anymissing;
	current_component = other.current_component;

	return (*this);
}

bool TS::TimeSeries::operator==(const TimeSeries& other) const
{
	bool retval = true;
	if (current_length != other.current_length)
		retval = false;
	for (int j = 0; j < n_components; ++j)
		for (int i = 0; i < current_length; ++i)
		{
			const double tx = other.missing[j][i];
			if (missing[j][i] != tx)
				retval = false;
			if (!missing[j][i])
				if (data[j][i] != other.data[j][i])
					retval = false;
		}
	return retval;
}

void TS::TimeSeries::ParseDescriptor(char* cp)
{
	int b, e;
	// first determine the command
	std::istrstream is(cp);
	char command[40], temps[1024], cr = 13;

	std::string white = "\t\n ";
	white = white + cr;

	command[0] = 0;
	is >> command;

	if (strcmp(command, "Title:") == 0)
	{
		is.getline(temps, 1024, '\n');
		title_ = temps;
		b = static_cast<int>(title_.find_first_not_of(white));
		e = static_cast<int>(title_.find_last_not_of(white));
		title_ = std::string(title_, b, e - b + 1);
	}

	if (strcmp(command, "Description:") == 0)
	{
		is.getline(temps, 1024, '\n');
		description_ = temps;
		b = static_cast<int>(description_.find_first_not_of(white));
		e = static_cast<int>(description_.find_last_not_of(white));
		description_ = std::string(description_, b, e - b + 1);
		// now replace %s with newlines
		int i = static_cast<int>(description_.find('%'));
		while (i != -1)
		{
			description_[i] = '\n';
			i = static_cast<int>(description_.find('%'));
		}
	}
}

namespace TS
{

	std::ostream& operator<<(std::ostream& os, TS::TimeSeries& ts)
	{
		for (int i = 0; i < ts.current_length; ++i)
			for (int j = 0; j < ts.n_components; ++j)
			{
				if (!ts.IsMissing(j, i))
					os << ts.data[j][i] << std::endl;
				else
					os << "NA" << std::endl;
			}
		return(os);
	}

	std::istream& operator>>(std::istream& is, TS::TimeSeries& ts)
	{
		int i = 0, /*j,*/ k, nfields = 0;
		const int maxlinelength = 65536;
		char temps[maxlinelength],/* temps2[maxlinelength],*/ c;
		//double tx;
		bool isok;
		int streampos;
		std::string s1;

		// First check to see if file contains lines which are
		// numeric, with std::endl at end, or NA (missing).  
		// If not, return with timeseries length=0.
		streampos = static_cast<int>(is.tellg());
		isok = true;
		for (i = 0; i == 0; )
		{
			is.getline(temps, maxlinelength);
			std::istrstream inst(temps);
			double tx;
			inst >> std::ws;             // skip over white space
			c = static_cast<char>(inst.peek());        // and see what's next

			// check it
			if ((c == 'N') || (c >= '0' && c <= '9')
				|| (c == 'n') || (c == '+') || (c == '-')
				|| (c == '.'))
			{
				i = 1;  // found a line of data, so break and assume the remaining lines are OK
				// but first count the number of fields
				nfields = 0;
				bool done = false;
				while (!done)
				{
					inst >> tx;
					if (inst)
						++nfields;
					else
					{
						if (inst.eof())
							done = true;
						else if (inst.fail()) // it's probably a NaN! still counts as a field
						{
							inst.clear();
							inst >> s1;
							++nfields;
						}
					}
					if (inst.eof())
						done = true;
				}
			}
			else if (c == '%')
			{
			} // do nothing if it's just a comment line
			else
			{ // if it's not numeric and it's not a comment, then break and return
				std::cout << "Error, character " << static_cast<int>(c) << " found." << std::endl;
				isok = false;
				i = 1;
			}
		}

		is.seekg(streampos);  // put it back to where it was
		if (!isok)
			return is;          // read failed for some reason

		  // read data
		ts.ClearOut(nfields);
		QuantLib::Array tv(nfields), tm(nfields);

		temps[0] = 1;
		while ((!is.eof()) && (temps[0] != 0))
		{
			is.getline(temps, maxlinelength);
			std::istrstream inst(temps);

			// remove white space at beginning of line
			inst >> std::ws;
			c = static_cast<char>(inst.peek());

			// now read contents of line
			if (c != -1) // -1 appears to happen at eof
				if (c == '%')
				{
					inst >> s1;
					char* cp = static_cast<char*>((void*)s1.c_str());
					ts.ParseDescriptor(cp);
				}
				else // read in nfields fields
				{
					for (k = 0; k < nfields; ++k)
					{
						inst >> tv[k];
						if (inst)
							tm[k] = 0;          // not missing
						else
						{
							inst.clear();
							inst >> s1;       // skip over  it
							tm[k] = 1;
							tv[k] = 0;
						}
					}
					ts.Append(tv, tm);
				}
		}
		return is;
	}
}


void TS::TimeSeries::RenderInto(std::ostringstream& os) const
{
	//int i;

	os << "\\b";  // make it bold
	if (title_.length() == 0)
		os << "Time Series";
	else
		os << title_;
	os << ":" << std::endl;
	os << "\\n";  // make it normal

	if (description_.length() > 0)
		os << description_ << std::endl << std::endl;

	os << "n = " << GetN() << std::endl;
	os << std::endl;
}

QuantLib::Matrix TS::TimeSeries::GetInnovations(double& retvhat, int maxm) const
// this procedure returns a Matrix containing thetahat_{m,k}
// \hat{v} is returned in vhat
{
	const int nacfs = maxm + 1;
	QuantLib::Array acfs(nacfs + 1);
	ComputeSampleACF(acfs, nullptr, 0);
	QuantLib::Matrix tm = Innovations(retvhat, maxm, acfs);
	return tm;
}

double my_hyperg1(double t, void* parms)
{
	auto* p = static_cast<QuantLib::Array*>(parms);
	const double a = (*p)[0], c = (*p)[1], rho_re = (*p)[2], rho_im = (*p)[3];

	complex tc(1 - t * rho_re, -rho_im);
	complex rval = (c - 1) * pow(tc, -a) * pow(1 - t, c - 2);
	return rval.real();
}

double my_hyperg2(double t, void* parms)
{
	auto* p = static_cast<QuantLib::Array*>(parms);
	const double a = (*p)[0], c = (*p)[1], rho_re = (*p)[2], rho_im = (*p)[3];

	complex tc(1 - t * rho_re, -rho_im);
	complex rval = (c - 1) * pow(tc, -a) * pow(1 - t, c - 2);
	return rval.imag();
}

void my_err_handler(const char* reason, const char* /*file*/,
	int /*line*/, int gsl_err_num)
{
	std::cout << "GSL Err #" << gsl_err_num << ": ";
	std::cout << reason << std::endl;
}

#ifdef _ALLOW_SPECTRAL_METHODS
QuantLib::Array TS::TimeSeries::ComputePeriodogram(QuantLib::Array* omegas) const
{
	complex sum, consti(0, 1), tc;
	//double omega_j;
	int i/*, j, t*/;
	QuantLib::Array periodogram(n);
	const double* tdata = &data[current_component][0];

	if (n == 0)
		return periodogram;  // can't compute periodogram of nothing.


	  // new efficient way (same but enormously faster)
	for (i = 0; i < n; ++i)
		periodogram[i] = tdata[i];

	gsl_fft_real_workspace* work = gsl_fft_real_workspace_alloc(n);
	gsl_fft_real_wavetable* real = gsl_fft_real_wavetable_alloc(n);
	gsl_fft_real_transform(&periodogram[0], 1, n,
		real, work);

	// now we have it stored in the efficient halfcomplex format
	// check gsl documentation (info gsl in linux) for details
	// so we unpack it into standard format
	const auto complex_coeffs = new double[2 * n + 2];
	gsl_fft_halfcomplex_unpack(&periodogram[0], complex_coeffs, 1, n);

	// and free up memory
	gsl_fft_real_wavetable_free(real);
	gsl_fft_real_workspace_free(work);

	for (i = 0; i < n; ++i)
		periodogram[i] = ((complex_coeffs[2 * i] * complex_coeffs[2 * i])
			+ (complex_coeffs[2 * i + 1] * complex_coeffs[2 * i + 1])) / n;

	// fill in frequencies if necessary
	if (omegas != nullptr)
	{
		omegas->resize(n);
		for (i = 0; i < n; ++i)
			(*omegas)[i] = 2 * M_PI * i / n;
	}
	delete[] complex_coeffs;
	return periodogram;
}
#endif