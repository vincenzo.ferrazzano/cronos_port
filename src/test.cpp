#include "pch.hpp"
#include "arma_model.hpp"

int main()
{
	TS::TimeSeries ts;
	std::ifstream infile("./deps/cronos/data/sunspots.dat");
	if (!infile.good())
	{
		return 0;
	}

	infile >> (ts);
	infile.close();
	TS::ARMAModel model;
	model.SetOrder(2, 1);
	model.SetFracDiff(0.0);
	std::ostringstream res, supplemental;

	int methodtype = TS::METHOD_MLE;
	int result = model.FitModel(ts, 
		methodtype, 
		1000,
		nullptr,
		nullptr, 
		res,
		supplemental,
		true);
	std::cout <<"res:\n" << res.str()<<"\n";
	std::cout << "suppl:\n" << supplemental.str() << "\n";

	return result;
}