
 // This module handles operations with polynomials.
 // Polynomials are specified by two parameters:
 //  (a) The order is p.
 //  (b) $a_0, a_1, \ldots, a_p$ are coefficients of the
 //      constant term, the $x$ term, the $x^2$ term, up to the $x^p$ term.
#pragma once
#include "pch.hpp"

 namespace TS::poly
 {
	 std::vector<complex> RootsOf(int order, const QuantLib::Array& coeffs);
	 std::vector<complex> CoefficientsOf(int order, const std::vector<complex>& root, complex normalize = 1.0);
 }

