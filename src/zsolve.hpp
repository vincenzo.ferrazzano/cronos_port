#pragma once
#include "pch.hpp"

namespace TS::poly
{
	struct complex_workspace
	{
		size_t nc;
		QuantLib::Array matrix;

		explicit complex_workspace(const size_t n);

		~complex_workspace() = default;
	};

	void set_companion_matrix(const double* a, size_t nc, double* m);

	void balance_companion_matrix(double* m, const size_t nc);

	int qr_companion(double* h, size_t nc, QuantLib::Array& zroot_vec);


	int
	poly_complex_solve(const QuantLib::Array& a,
			size_t n,
			complex_workspace& w,
			QuantLib::Array& z);
}