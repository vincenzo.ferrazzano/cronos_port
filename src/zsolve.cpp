#include "pch.hpp"
#include "zsolve.hpp"

#define SET_COMPLEX_PACKED(zp,n,x,y) do {*((zp)+2*(n))=(x); *((zp)+(2*(n)+1))=(y);} while(0)

/* C-style matrix elements */
#define MAT(m,i,j,n) ((m)[(i)*(n) + (j)])

/* Fortran-style matrix elements */
#define FMAT(m,i,j,n) ((m)[((i)-1)*(n) + ((j)-1)])

#define RADIX 2
#define RADIX2 (RADIX*RADIX)

TS::poly::complex_workspace::complex_workspace(const size_t n) : nc(n - 1), matrix(nc* nc)
{
	if (n == 0)
	{
		throw std::runtime_error("matrix size n must be positive integer");
	}
}
void TS::poly::set_companion_matrix(const double* a, size_t nc, double* m)
{
	for (size_t i = 0; i < nc; i++)
	{
		for (size_t j = 0; j < nc; j++)
		{
			MAT(m, i, j, nc) = 0.0;
		}
	}

	for (size_t i = 1; i < nc; i++)
	{
		MAT(m, i, i - 1, nc) = 1.0;
	}

	for (size_t i = 0; i < nc; i++)
	{
		MAT(m, i, nc - 1, nc) = -a[i] / a[nc];
	}
}

void TS::poly::balance_companion_matrix(double* m, const size_t nc)
{
	int not_converged = 1;

	double row_norm;
	double col_norm;

	while (not_converged)
	{
		size_t j;

		not_converged = 0;

		for (size_t i = 0; i < nc; i++)
		{
			/* column norm, excluding the diagonal */

			if (i != nc - 1)
			{
				col_norm = fabs(MAT(m, i + 1, i, nc));
			}
			else
			{
				col_norm = 0;

				for (j = 0; j < nc - 1; j++)
				{
					col_norm += fabs(MAT(m, j, nc - 1, nc));
				}
			}

			/* row norm, excluding the diagonal */

			if (i == 0)
			{
				row_norm = fabs(MAT(m, 0, nc - 1, nc));
			}
			else if (i == nc - 1)
			{
				row_norm = fabs(MAT(m, i, i - 1, nc));
			}
			else
			{
				row_norm = (fabs(MAT(m, i, i - 1, nc))
					+ fabs(MAT(m, i, nc - 1, nc)));
			}

			if (col_norm == 0 || row_norm == 0)
			{
				continue;
			}

			double g = row_norm / RADIX;
			double f = 1;
			const double s = col_norm + row_norm;

			while (col_norm < g)
			{
				f *= RADIX;
				col_norm *= RADIX2;
			}

			g = row_norm * RADIX;

			while (col_norm > g)
			{
				f /= RADIX;
				col_norm /= RADIX2;
			}

			if ((row_norm + col_norm) < 0.95 * s * f)
			{
				not_converged = 1;

				g = 1 / f;

				if (i == 0)
				{
					MAT(m, 0, nc - 1, nc) *= g;
				}
				else
				{
					MAT(m, i, i - 1, nc) *= g;
					MAT(m, i, nc - 1, nc) *= g;
				}

				if (i == nc - 1)
				{
					for (j = 0; j < nc; j++)
					{
						MAT(m, j, i, nc) *= f;
					}
				}
				else
				{
					MAT(m, i + 1, i, nc) *= f;
				}
			}
		}
	}
}

int TS::poly::qr_companion(double* h, size_t nc, QuantLib::Array& zroot_vec)
{
	auto* zroot = zroot_vec.begin();
	double t = 0.0;

	size_t e;
	size_t i;
	size_t j;
	size_t m;

	double s, z;

	double p = 0, q = 0, r = 0;

	/* FIXME: if p,q,r, are not set to zero then the compiler complains
	   that they ``might be used uninitialized in this
	   function''. Looking at the code this does seem possible, so this
	   should be checked. */

	size_t n = nc;

next_root:

	if (n == 0)
		return 0;

	size_t iterations = 0;

next_iteration:

	for (e = n; e >= 2; e--)
	{
		const double a1 = fabs(FMAT(h, e, e - 1, nc));
		const double a2 = fabs(FMAT(h, e - 1, e - 1, nc));
		const double a3 = fabs(FMAT(h, e, e, nc));

		if (a1 <= std::numeric_limits<double>::epsilon() * (a2 + a3))
			break;
	}

	double x = FMAT(h, n, n, nc);

	if (e == n)
	{
		SET_COMPLEX_PACKED(zroot, n - 1, x + t, 0); /* one real root */
		n--;
		goto next_root;
		/*continue;*/
	}

	double y = FMAT(h, n - 1, n - 1, nc);
	double w = FMAT(h, n - 1, n, nc) * FMAT(h, n, n - 1, nc);

	if (e == n - 1)
	{
		p = (y - x) / 2;
		q = p * p + w;
		y = sqrt(fabs(q));

		x += t;

		if (q > 0) /* two real roots */
		{
			if (p < 0)
				y = -y;
			y += p;

			SET_COMPLEX_PACKED(zroot, n - 1, x - w / y, 0);
			SET_COMPLEX_PACKED(zroot, n - 2, x + y, 0);
		}
		else
		{
			SET_COMPLEX_PACKED(zroot, n - 1, x + p, -y);
			SET_COMPLEX_PACKED(zroot, n - 2, x + p, y);
		}
		n -= 2;

		goto next_root;
		/*continue;*/
	}

	/* No more roots found yet, do another iteration */

	if (iterations == 120) /* increased from 30 to 120 */
	{
		/* too many iterations - give up! */

		return false;
	}

	if (iterations % 10 == 0 && iterations > 0)
	{
		/* use an exceptional shift */

		t += x;

		for (i = 1; i <= n; i++)
		{
			FMAT(h, i, i, nc) -= x;
		}

		s = fabs(FMAT(h, n, n - 1, nc)) + fabs(FMAT(h, n - 1, n - 2, nc));
		y = 0.75 * s;
		x = y;
		w = -0.4375 * s * s;
	}

	iterations++;

	for (m = n - 2; m >= e; m--)
	{
		z = FMAT(h, m, m, nc);
		r = x - z;
		s = y - z;
		p = FMAT(h, m, m + 1, nc) + (r * s - w) / FMAT(h, m + 1, m, nc);
		q = FMAT(h, m + 1, m + 1, nc) - z - r - s;
		r = FMAT(h, m + 2, m + 1, nc);
		s = fabs(p) + fabs(q) + fabs(r);
		p /= s;
		q /= s;
		r /= s;

		if (m == e)
			break;

		double a1 = fabs(FMAT(h, m, m - 1, nc));
		double a2 = fabs(FMAT(h, m - 1, m - 1, nc));
		double a3 = fabs(FMAT(h, m + 1, m + 1, nc));

		if (a1 * (fabs(q) + fabs(r)) <= std::numeric_limits<double>::epsilon() * fabs(p) * (a2 + a3))
			break;
	}

	for (i = m + 2; i <= n; i++)
	{
		FMAT(h, i, i - 2, nc) = 0;
	}

	for (i = m + 3; i <= n; i++)
	{
		FMAT(h, i, i - 3, nc) = 0;
	}

	/* double QR step */

	for (size_t k = m; k <= n - 1; k++)
	{
		const int notlast = (k != n - 1);

		if (k != m)
		{
			p = FMAT(h, k, k - 1, nc);
			q = FMAT(h, k + 1, k - 1, nc);
			r = notlast ? FMAT(h, k + 2, k - 1, nc) : 0.0;

			x = fabs(p) + fabs(q) + fabs(r);

			if (x == 0)
				continue; /* FIXME????? */

			p /= x;
			q /= x;
			r /= x;
		}

		s = sqrt(p * p + q * q + r * r);

		if (p < 0)
			s = -s;

		if (k != m)
		{
			FMAT(h, k, k - 1, nc) = -s * x;
		}
		else if (e != m)
		{
			FMAT(h, k, k - 1, nc) *= -1;
		}

		p += s;
		x = p / s;
		y = q / s;
		z = r / s;
		q /= p;
		r /= p;

		/* do row modifications */

		for (j = k; j <= n; j++)
		{
			p = FMAT(h, k, j, nc) + q * FMAT(h, k + 1, j, nc);

			if (notlast)
			{
				p += r * FMAT(h, k + 2, j, nc);
				FMAT(h, k + 2, j, nc) -= p * z;
			}

			FMAT(h, k + 1, j, nc) -= p * y;
			FMAT(h, k, j, nc) -= p * x;
		}

		j = (k + 3 < n) ? (k + 3) : n;

		/* do column modifications */

		for (i = e; i <= j; i++)
		{
			p = x * FMAT(h, i, k, nc) + y * FMAT(h, i, k + 1, nc);

			if (notlast)
			{
				p += z * FMAT(h, i, k + 2, nc);
				FMAT(h, i, k + 2, nc) -= p * r;
			}
			FMAT(h, i, k + 1, nc) -= p * q;
			FMAT(h, i, k, nc) -= p;
		}
	}

	goto next_iteration;
}

int TS::poly::poly_complex_solve(const QuantLib::Array& a, size_t n, complex_workspace& w, QuantLib::Array& z)
{
	if (n == 0)
	{
		throw std::runtime_error("number of terms must be a positive integer");
	}

	if (n == 1)
	{
		throw std::runtime_error("cannot solve for only one term");
	}

	if (a[n - 1] == 0)
	{
		throw std::runtime_error("leading term of polynomial must be non-zero");
	}

	if (w.nc != n - 1)
	{
		throw std::runtime_error("size of workspace does not match polynomial");
	}

	set_companion_matrix(a.begin(), n - 1, w.matrix.begin());
	balance_companion_matrix(w.matrix.begin(), n - 1);
	const int status = qr_companion(w.matrix.begin(), n - 1, z);

	if (status)
	{
		throw std::runtime_error("root solving qr method failed to converge");
	}

	return 0;
}
