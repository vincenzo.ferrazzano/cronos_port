﻿#pragma once
#include "poly.hpp"
#include "timeseries_model.hpp"
// Here we define ARMAModel as a special case of the more general TimeSeriesModel.
namespace TS
{
	class ARMAModel : public TimeSeriesModel {
	protected:
		int p_;
		int q_;
		double fracdiff_;
		double sigma_;
		double mean_;

		/// <summary>
		/// Faster implementation
		/// </summary>
		/// <param name="i"></param>
		/// <param name="j"></param>
		/// <param name="m"></param>
		/// <param name="acfs"></param>
		/// <returns></returns>
		[[nodiscard]]
		double kappaNew(int i, int j, int m, const QuantLib::Array& acfs) const;
		/// <summary>
		/// Original implementation. A bit slow
		/// </summary>
		/// <param name="i"></param>
		/// <param name="j"></param>
		/// <param name="m"></param>
		/// <param name="acfs"></param>
		/// <returns></returns>
		[[nodiscard]]
		double kappaOld(int i, int j, int m, const QuantLib::Array& acfs) const;

		/// this is autocovariance of the modified X_t process (Ansley 1979)
		/**
		 * Used for prediction
		 *
		 */
		template<bool _useNew = true, bool _debug = false>
		[[nodiscard]]
		double kappa(int i, int j, int m, const QuantLib::Array& acf) const;

		double phi_[maxorder];
		double theta_[maxorder];

		// some private functions
		double LogPrior(bool& isvalid);
		void BayesianFit(const TimeSeries& ts, QuantLib::Array& mask,
			void (*callback)(int, void*) = nullptr, void* cb_parms = nullptr);
		[[nodiscard]]
		bool is_short_mem() const;

		void ComputeSpecialResiduals(const TimeSeries& tp, TimeSeries* rret, double* rstore) const;
		void ComputeSpecialResidualsWithMissing(const TimeSeries& tp, TimeSeries* residreturn, double* rstore) const;
		void ForecastWithMissing(const TimeSeries&, int nsteps, double* fret, double* fmse) const;
		//B&D pg 257, eq 8.7.7. To be implemented
		double ComputeReducedLogLikelihood(TimeSeries* tp, double* bestsig = nullptr);
		[[nodiscard]]
		QuantLib::Matrix cfuncsfor(double d, double rho_real, double rho_complex,
			int power, int h, int extent) const;

		[[nodiscard]]
		QuantLib::Array ValidityConstraint() const override;

	public:
		ARMAModel();
		~ARMAModel() override = default;
		ARMAModel& operator=(const ARMAModel& other);
		int FitModel(const TimeSeries&, const int method, const int numits,
			void (*itercallback)(int, void*), void* cb_parms,
			std::ostringstream& msg, std::ostringstream& supplementalinfo,
			bool get_parameter_cov) override;
		void SimulateInto(TimeSeries&, int, bool) const override;
		[[nodiscard]]
		QuantLib::Matrix SimulatePredictive(const TimeSeries&, int nsteps, int nsims) const override;
		void Forecast(const TimeSeries&, int nsteps, double* fret, double* fmse) override;
		void ComputeStdResiduals(const TimeSeries& tp, TimeSeries& resids) override;
		void RenderInto(std::ostringstream&) override;
		bool RenderCtsVersionInto(std::ostringstream& output, std::ostringstream& error, double delta) override;
		void ComputeACFPACF(QuantLib::Array& acf, QuantLib::Array* pacf, bool normalizeacf) const override;

		[[nodiscard]]
		double ComputeLogLikelihood(const TimeSeries& tp) const override;
		void ComputeAdditionalStats(const TimeSeries&) override;
		[[nodiscard]]
		QuantLib::Array ParameterBundle() const override;
		void UnbundleParameters(const QuantLib::Array& v) override;
		[[nodiscard]]
		QuantLib::Array GetMask() const override;
		QuantLib::Array GetDefaultParameters(QuantLib::Array& mask, const TimeSeries& tsp) override;
		void StreamParameterName(std::ostringstream& os, int pnum) override;
		// functions for getting/setting model
		void SetOrder(int arorder, int maorder);

		void SetSigma2(double sigma2);
		void SetCoefficients(const double* phis, const double* thetas);
		void SetMean(double m);

		void SetFracDiff(double d);
		[[nodiscard]]
		int GetP() const { return p_; }
		[[nodiscard]]
		int GetQ() const { return q_; }
		[[nodiscard]]
		double GetFracDiff() const;
		[[nodiscard]]
		double GetMean() const;
		[[nodiscard]]
		double GetSigma() const;
		void GetCoefficients(QuantLib::Array& phi, QuantLib::Array& theta);
		bool IsCausal(bool causalify = false);
		bool IsInvertible(bool invertibilify = false);
		bool CheckModel(bool flipit) override;
		[[nodiscard]]
		double MinAbsRoot() const;
#ifdef _ALLOW_SPECTRAL_METHODS
	protected:
		bool using_whittle_;
		// cached info for whittle likelihood
		bool whittle_cached_;
		TimeSeries whittle_ts_;
		QuantLib::Array whittle_pdgm_;
	public:
		QuantLib::Array ComputeSpectralDensity(QuantLib::Array& omegas) override;
		void SetWhittleMode(bool wm) { using_whittle_ = wm; }
		bool GetWhittleMode() const { return using_whittle_; }
#endif
	};

	template <bool _useNew, bool _debug>
	double ARMAModel::kappa(int i, int j, int m, const QuantLib::Array& acf) const
	{
		double res;
		if constexpr (_useNew)
		{
			res = kappaNew(i, j, m, acf);
			if constexpr (_debug)
			{
				const double old_res = kappaOld(i, j, m, acf);
				if (std::abs(old_res - res) > TINY)
				{
					throw std::runtime_error("Error! Different behaviour!");
				}
			}
		}
		else
		{
			res = kappaOld(i, j, m, acf);
		}
		return res;
	}
}