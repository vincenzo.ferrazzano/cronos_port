﻿#include "pch.hpp"
#include "arma_model.hpp"

complex sf_hyperg_2F1(double a, double /*b*/, double c, double rho_real, double rho_imag)
{
	const double magrho = sqrt(rho_real * rho_real + rho_imag * rho_imag);
	const double rhostar = 1 - (1 - magrho) / 2.0;
	complex chro(rho_real, rho_imag);
	const double tolerance = 0.0001;

	// determine how many terms to use
	int k = static_cast<int>(std::ceil((fabs(a) * magrho + fabs(c) * rhostar) / (magrho - rhostar)) + 0.5);
	if (k < 2)
		k = 2;
	int m = static_cast<int>(std::ceil(log(tolerance) / log(rhostar)) + 0.5);
	if (m < 3)
		m = 3;

	complex total = 1.0;
	double paonpc = 1.0;
	complex chroprod = 1.0;

	for (int i = 0; i < k + m; ++i)
	{
		paonpc *= (a + i) / (c + i);
		chroprod *= chro;
		total += paonpc * chroprod;
	}
	return total;
}

TS::ARMAModel& TS::ARMAModel::operator=(const TS::ARMAModel& other)
{
	p_ = other.p_;   q_ = other.q_;
	mean_ = other.mean_;
	sigma_ = other.sigma_;
	fracdiff_ = other.fracdiff_;
	estimation_mask_ = other.estimation_mask_;
	for (int i = 0; i < maxorder; ++i)
	{
		phi_[i] = other.phi_[i];
		theta_[i] = other.theta_[i];
	}
#ifdef _ALLOW_SPECTRAL_METHODS
	using_whittle_ = other.using_whittle_;
	whittle_cached_ = other.whittle_cached_;
	whittle_ts_ = other.whittle_ts_;
	whittle_pdgm_ = other.whittle_pdgm_;
#endif
	return *this;
}


TS::ARMAModel::ARMAModel()
	: TimeSeriesModel()
{
	p_ = q_ = 0;
	mean_ = 0;
	sigma_ = 1;		// default is white noise with mean 0, variance 1
	fracdiff_ = 0;
	int i;
	for (i = 0; i < maxorder; ++i)
		phi_[i] = theta_[i] = 0;
	estimation_mask_.resize(3);
	estimation_mask_[0] = 1;
	estimation_mask_[1] = 0;  // default: don't estimate fracdiff parm
	estimation_mask_[2] = 1;
#ifdef _ALLOW_SPECTRAL_METHODS
	using_whittle_ = false;
	whittle_cached_ = false;
#endif
}

void TS::ARMAModel::SetCoefficients(const double* phis, const double* thetas)
{
	int i;
	for (i = 0; i < p_; ++i)
		phi_[i] = phis[i];
	for (i = 0; i < q_; ++i)
		theta_[i] = thetas[i];
}

void TS::ARMAModel::SetMean(double m)
{
	mean_ = m;
}

void TS::ARMAModel::SetFracDiff(double d)
{
	fracdiff_ = d;
}

double TS::ARMAModel::GetFracDiff() const
{
	return fracdiff_;
}

double TS::ARMAModel::GetMean() const
{
	return mean_;
}

double TS::ARMAModel::GetSigma() const
{
	return sigma_;
}

void TS::ARMAModel::GetCoefficients(QuantLib::Array& phis, QuantLib::Array& thetas)
{
	int i;
	phis.resize(p_);
	for (i = 0; i < p_; ++i)
		phis[i] = phi_[i];
	thetas.resize(q_);
	for (i = 0; i < q_; ++i)
		thetas[i] = theta_[i];
}

bool TS::ARMAModel::is_short_mem() const
{
	return !(fabs(fracdiff_) > zero);
}

void TS::ARMAModel::SimulateInto(TimeSeries& ts, int nn, bool ovw) const
// This function is perfectly fine.  I no longer
// have complaints about it.
{
	int i, j /*, q = GetQ(), p = GetP()*/;
	double sum;

	if (!ovw)
	{
		ts.ClearOut();
		for (i = 0; i < nn; ++i)
			ts.Append(0);
	}
	else
		nn = ts.GetN();

	// uses Durbin-Levinson recursions to simulate from
	// successive one-step predictive d-ns
	// (works for long-memory T-S, unlike the obvious
	//  constructive approach)

	QuantLib::Array acf(nn), nu(nn), olda(nn), a(nn);
	ComputeACFPACF(acf, nullptr, false);  // fill in ACF

	nu[0] = acf[0];  // nu(0) = 1-step pred. variance of X(0)
	ts.data[0][0] = norm_random(MT, 0, nu[0]);
	ql_zeros(a);
	for (i = 1; i < nn; ++i)
	{
		for (j = 0; j < nn; ++j)
			olda[j] = a[j];

		// compute the new a vector
		for (j = 1, sum = 0.0; j < i; ++j)
			sum += olda[j - 1] * acf[i - j];
		a[i - 1] = 1 / nu[i - 1] * (acf[i] - sum);
		for (int k = 0; k < i - 1; ++k)
			a[k] = olda[k] - a[i - 1] * olda[i - 2 - k];

		// update nu
		nu[i] = nu[i - 1] * (1 - a[i - 1] * a[i - 1]);

		// compute xhat
		for (j = 0, sum = 0.0; j < i; ++j)
			sum += a[j] * ts.data[0][i - 1 - j];
		ts.data[0][i] = norm_random(MT, sum, nu[i]);
	}
	for (i = 0; i < nn; ++i)
		ts.data[0][i] += mean_;
}

QuantLib::Matrix TS::ARMAModel::SimulatePredictive(const TimeSeries& ts, int nsteps, int nsims) const
{
	// note: handling of missing values is not optimal.
	//       they are just filled in with draws from one-step predictive d-ns
	//       (instead of the full-conditional distributions)
	int i, j, sim_num, n0 = ts.GetN(), nn = n0 + nsteps;
	double sum;

	// uses Durbin-Levinson recursions to simulate from
	// successive one-step predictive d-ns
	// (works for long-memory T-S, unlike the obvious
	//  constructive approach)

	QuantLib::Array acf(nn), nu(nn), olda(nn), a(nn);
	ComputeACFPACF(acf, nullptr, false);  // fill in ACF
	QuantLib::Array new_ts(nn);
	QuantLib::Matrix new_sims(nsims, nsteps);

	for (i = 0; i < n0; ++i)
		new_ts[i] = ts.data[0][i] - mean_;

	nu[0] = acf[0];  // nu(0) = 1-step pred. variance of X(0)
	if (n0 > 0)
		if (ts.IsMissing(0))
			new_ts[0] = norm_random(MT, 0, nu[0]);
	if (n0 == 0)
		if (nsteps > 0)
			for (sim_num = 0; sim_num < nsims; ++sim_num)
				new_sims[sim_num][0] = norm_random(MT, 0, nu[0]);
	ql_zeros(a);
	for (i = 1; i < nn; ++i)
	{
		for (j = 0; j < nn; ++j)
			olda[j] = a[j];

		// compute the new a vector
		for (j = 1, sum = 0.0; j < i; ++j)
			sum += olda[j - 1] * acf[i - j];
		a[i - 1] = 1 / nu[i - 1] * (acf[i] - sum);
		for (int k = 0; k < i - 1; ++k)
			a[k] = olda[k] - a[i - 1] * olda[i - 2 - k];

		// update nu
		nu[i] = nu[i - 1] * (1 - a[i - 1] * a[i - 1]);

		// compute xhat
		if (i < n0) // nothing but filling in missing values
		{
			for (j = 0, sum = 0.0; j < i; ++j)
				sum += a[j] * new_ts[i - 1 - j];

			if (ts.IsMissing(i))
				new_ts[i] = norm_random(MT, sum, nu[i]);
		}
		else // we need multiple sims
			for (sim_num = 0; sim_num < nsims; ++sim_num)
			{
				for (j = 0, sum = 0.0; j < i; ++j)
					sum += a[j] * (i - j <= n0 ? new_ts[i - 1 - j] : new_sims[sim_num][i - 1 - j - n0]);
				new_sims[sim_num][i - n0] = norm_random(MT, sum, nu[i]);
			}
	}

	for (sim_num = 0; sim_num < nsims; ++sim_num)
		for (i = 0; i < nsteps; ++i)
			new_sims[sim_num][i] += mean_;

	return new_sims;
}

void TS::ARMAModel::RenderInto(std::ostringstream& os)
// Create text description of the current model.
// The special characters (bold, greek, etc.)
// are for use in the "TextRenderer" class.
{
	int i;

	os << "\\b";  // make it bold
	os << "AR";
	if (!is_short_mem())
		os << "FI";
	os << "MA(" << p_ << ",";
	if (!is_short_mem())
		os << fracdiff_ << ",";
	os << q_ << ") Model" << std::endl;
	os << "\\n";  // make it normal
	os << "X(t)";
	for (i = 1; i <= p_; ++i)
	{
		if (phi_[i - 1] > 0)
			os << std::setiosflags(std::ios::fixed) << std::setprecision(3) << " - " << phi_[i - 1] << "X";
		else
			os << std::setiosflags(std::ios::fixed) << std::setprecision(3) << " + " << -phi_[i - 1] << "X";
		os << "(t-" << i << ")";
	}
	os << " = Z(t)";
	for (i = 1; i <= q_; ++i)
	{
		if (theta_[i - 1] > 0)
			os << std::setiosflags(std::ios::fixed) << std::setprecision(3) << " + " << theta_[i - 1] << "Z";
		else
			os << std::setiosflags(std::ios::fixed) << std::setprecision(3) << " - " << -theta_[i - 1] << "Z";
		os << "(t-" << i << ")";
	}

	os << std::endl << "Mean = " << std::setprecision(5) << mean_ << ",  " << std::endl;
	//os << "\u03c3";  // UTF-8 unicode for greek lower case sigma
	os << "\\s2\\n = " << std::setprecision(5) << sigma_ * sigma_;
	if (loglikelihood_is_valid_)
	{
		os << std::endl << std::endl << "Log Likelihood = " << GetLogLikelihood() << std::endl;
		os << "AICC = " << GetAICC() << std::endl;
	}
	else
	{
		os << std::endl << std::endl << "Log Likelihood = <Not Computed>" << std::endl;
		os << "AICC = <Not Computed>" << std::endl;
	}
}

bool TS::ARMAModel::RenderCtsVersionInto(std::ostringstream& /*output*/, std::ostringstream& error, double /*delta*/)
{
	error << "Feature not yet supported for ARMA models." << std::ends;
	return false;
}

double TS::ARMAModel::MinAbsRoot() const
{
	int i, p2 = 0, q2 = 0;
	double min, tx;

	// first construct autoregressive polynomial
	for (i = 0; i < p_; ++i)
		if (fabs(phi_[i]) > zero)
			p2 = i + 1;					// p2 is the effective AR order, is < p if phi_p=0
	if (p2 == 0)
		min = 1e6;                                  // a bad approximation to \infty
	else
	{
		QuantLib::Array cp(p2 + 1);
		cp[0] = 1.0;
		for (i = 1; i <= p2; ++i)
			cp[i] = -phi_[i - 1];

		auto roots = FindComplexRoot(p2, cp);

		// now find minimum magnitude
		min = abs(roots[0]);
		for (i = 1; i <= p2; ++i)
		{
			tx = abs(roots[i - 1]);
			if (tx < min)
				min = tx;
		}
	}

	// then construct moving average polynomial
	for (i = 0; i < q_; ++i)
		if (fabs(theta_[i]) > zero)
			q2 = i + 1;					// q2 is the effective MA order
	if (q2 > 0)
	{
		QuantLib::Array cp(q2 + 1);
		cp[0] = 1.0;
		for (i = 1; i <= q2; ++i)
			cp[i] = theta_[i - 1];

		auto roots = FindComplexRoot(q2, cp);

		// now find minimum magnitude
		for (i = 1; i <= q2; ++i)
		{
			tx = abs(roots[i - 1]);
			if (tx < min)
				min = tx;
		}
	}

	return min; // this is min of roots both AR and MA polynomials (or 1e6)
}

bool TS::ARMAModel::IsCausal(bool causalify)
{
	int i, p2 = 0;
	double tx;
	bool nonstationary = false;

	// first construct autoregressive polynomial
	for (i = 0; i < p_; ++i)
		if (fabs(phi_[i]) > zero)
			p2 = i + 1;					// p2 is the effective AR order, is < p if phi_p=0
	if (p2 == 0)
		return true;

	QuantLib::Array cp(p_ + 1);
	cp[0] = 1.0;
	for (i = 1; i <= p_; ++i)
		cp[i] = -phi_[i - 1];

	auto roots = FindComplexRoot(p2, cp);

	// now find minimum magnitude
	double min = abs(roots[0]);
	for (i = 1; i <= p2; ++i)
	{
		tx = abs(roots[i - 1]);
		if (tx < min)
			min = tx;
		if (fabs(tx - 1.0) < zero)
			nonstationary = true;
	}
	if ((min > 1.0001) && (!nonstationary))
	{
		return true;
	}

	// otherwise it is non-causal,
	// we may now force it to be causal if causalify is true

	if (!causalify)
	{
		return false;
	}

	for (i = 1; i <= p2; ++i)
	{
		tx = abs(roots[i - 1]);
		double ty = arg(roots[i - 1]);
		if (tx < 1.0002)
			// map it into the interval (1.0001,1.0002)
		{
			const double tz = (1.0002 - tx) / 1.0002;
			tx = 1.0001 + 0.0001 * pow((1 - tz), 2.0);
		}
		roots[i - 1] = std::polar(tx, ty);
	}

	std::vector<complex> recon = poly::CoefficientsOf(p2, roots);
	for (i = 0; i < p2; ++i)
		phi_[i] = -real(recon[i + 1] / recon[0]);

	return true;
}

bool TS::ARMAModel::IsInvertible(bool invertibilify)
{
	int i, q2 = 0;

	// first construct moving average polynomial
	for (i = 0; i < q_; ++i)
		if (fabs(theta_[i]) > zero)
			q2 = i + 1;					// p2 is the effective MA order
	if (q2 == 0)
		return true;

	QuantLib::Array cp(q_ + 1);
	cp[0] = 1.0;
	for (i = 1; i <= q_; ++i)
		cp[i] = theta_[i - 1];

	auto roots = FindComplexRoot(q2, cp);

	// now find minimum magnitude
	double min = abs(roots[0]);
	for (i = 1; i <= q2; ++i)
		if (abs(roots[i - 1]) < min)
			min = abs(roots[i - 1]);
	if (min > 1.0001)
	{
		return true;
	}

	// otherwise we may force it to be causal!
	if (!invertibilify)
	{
		return false;
	}

	for (i = 1; i <= q2; ++i)
	{
		double tx = abs(roots[i - 1]);
		double ty = arg(roots[i - 1]);

		if (tx < 1.0002)
			// map it into the interval (1.0001,1.0002)
		{
			const double tz = (1.0002 - tx) / 1.0002;
			tx = 1.0001 + 0.0001 * pow((1 - tz), 2.0);
		}

		roots[i - 1] = std::polar(tx, ty);
	}
	auto recon = poly::CoefficientsOf(q2, roots);
	for (i = 0; i < q2; ++i)
		theta_[i] = real(recon[i + 1] / recon[0]);

	return true;
}
/**
 * Check if the model is ok. If something is wrong, then it changes it to make it ok: that is:
 *	makes it invertible and causal
 */
bool TS::ARMAModel::CheckModel(bool flipit /**< if true, rectifies the model*/)
{
	const bool causal = IsCausal(flipit);
	const bool invertible = IsInvertible(flipit);
	if ((!causal) || (!invertible))
		return false;

	// then frac diff parm d
	double d = GetFracDiff()/*, intpart*/;
	if (flipit)
	{
		if (d > 0.499)
			d = 0.499;
		if (d < -0.499)
			d = -0.499;
		SetFracDiff(d);
	}
	else if ((d >= 0.5) || (d <= -0.5))
		return false;

	// then sigma >= 0
	double s = GetSigma();
	if (flipit)
	{
		if (s < zero)
			s = zero;
		SetSigma2(s * s);
	}
	else
		if (s < zero)
			return false;

	return true;
}



QuantLib::Array TS::ARMAModel::ValidityConstraint() const
{
	QuantLib::Array retval(3);

	// 3 constraints
	// (a) d in (-1/2,1/2)
	double tx = GetFracDiff();
	tx = 0.2499 - tx * tx;
	retval[0] = tx;

	// (b) sigma > 0
	retval[1] = GetSigma();

	// (c) min |root| > 1
	retval[2] = MinAbsRoot() - 1.0001;

	return retval;
}


QuantLib::Array TS::ARMAModel::GetDefaultParameters(QuantLib::Array& mask, const TimeSeries& tsp)
// can modify mask if necessary
{
	QuantLib::Array parms = ParameterBundle();
	// mean is handled differently
	if (mask[0]) // mean is to be estimated
	{
		mask[0] = 0;
		parms[0] = tsp.SampleMean();
	}

	// initialize all non-masked parameters now
	QuantLib::Array tacf(1);
	tsp.ComputeSampleACF(tacf, nullptr, 0);
	for (int i = 0; i < mask.size(); ++i)
		if (mask[i])
			if (i == 2)
				parms[i] = sqrt(tacf[0]);
			else
				parms[i] = 0.0;
	return parms;
}

const double epsilon = 1e-10;  // used to keep roots away from unit circle

double logit(double x)
{
	const double tx = exp(x);
	return (1.0 / (1.0 + tx));
}

double invlogit(double x)
{
	return log(1.0 / x - 1.0);
}

void RCTranslate(double x, double y, complex& c1, complex& c2)
{
	if (y < 0) // make a complex-conjugate pair
	{
		const double r = logit(x); // magnitude
		const double theta = y;
		c1 = std::polar(r, theta);
		c2 = std::polar(r, -theta);
	}
	else // make a real pair
	{
		c1 = logit(x + y) * 2 - 1;
		c2 = logit(x - y) * 2 - 1;
	}
}

// The following functions use the
// general parameter vector format:
//
// parm[0,...,p-1] = phi_1,...phi_p
// parm[p,...,p+q-1] = theta_1,...,theta_q 
// parm[p+q] = sigma
// parm[p+q+1] = d
// parm[p+q+2] = mean
//

QuantLib::Array TS::ARMAModel::ParameterBundle() const
{
	int i/*, p = GetP(), q = GetQ()*/;
	QuantLib::Array pvec(p_ + q_ + 3);
	for (i = 0; i < p_; ++i)
		pvec[i + 3] = phi_[i];
	for (i = 0; i < q_; ++i)
		pvec[i + p_ + 3] = theta_[i];
	pvec[0] = GetMean();
	pvec[1] = GetFracDiff();
	pvec[2] = GetSigma();
	return pvec;
}

void TS::ARMAModel::UnbundleParameters(const QuantLib::Array& pvec)
{
	/*int p = GetP(), q = GetQ();*/
	SetMean(pvec[0]);
	SetFracDiff(pvec[1]);
	SetSigma2(pvec[2] * pvec[2]);
	const double* phis = p_ == 0 ? nullptr : &pvec.begin()[3];
	const double* thetas = q_ == 0 ? nullptr : &pvec.begin()[p_ + 3];
	SetCoefficients(phis, thetas);
}

void TS::ARMAModel::StreamParameterName(std::ostringstream& os, int pnum)
{
	//int p = GetP(), q = GetQ();
	switch (pnum) {
	case 0:
		//os << "\u03bc = mean"; break;
		os << "= mean"; break;
	case 1:
		os << "d = frac. diff. parm"; break;
	case 2:
		/*os << "\u03c3";*/ break;   // sigma
	default:
		if (pnum < 3 + p_)
			os << /*"\u03c6" <<*/ "(" << (pnum - 2) << ")";
		else
			os << /*"\u03b8("*/"(" << (pnum - 2 - p_) << ")";
	}
}

void TS::ARMAModel::SetOrder(int arorder, int maorder)
{
	p_ = arorder;
	q_ = maorder;
}

void TS::ARMAModel::SetSigma2(double sigma2)
{
	sigma_ = sqrt(sigma2);
}

QuantLib::Array TS::ARMAModel::GetMask() const
{
	int i;
	const size_t np = p_ + q_ + 3;
	QuantLib::Array retval(np);

	for (i = 0; i < std::min(estimation_mask_.size(), np); ++i)
		retval[i] = estimation_mask_[i];
	for (; i < np; ++i)
	{
		switch (i)
		{
		case 0:  case 2: retval[i] = 1;   break;
		case 1: retval[i] = 0;   break;
		default: retval[i] = 1;   break;
		}
	}
	return retval;
}

double TS::ARMAModel::LogPrior(bool& isvalid)
{
	isvalid = true;

	// check for causality, invertibility
	if ((!IsCausal()) || (!IsInvertible()))
		isvalid = false;

	const double tx = lognorm_density(log(GetSigma()), 0, 10000);

	return tx;
}

void TS::ARMAModel::BayesianFit(const TimeSeries& ts, QuantLib::Array& mask, void (*callback)(int, void*), void* cb_parms)
{
	int i;
	bool isok, accept;
	QuantLib::Array acf(1);
	double ty = 0;

	// initialize parameter vector pvec=(\mu,d,\sigma,\phi,\theta)
	// - only initializing unmasked elements
	QuantLib::Array localmask = GetMask();
	QuantLib::Array pvec = GetDefaultParameters(mask, ts);
	UnbundleParameters(pvec);

	double tx = ComputeLogLikelihood(ts);
	tx += LogPrior(isok);
	if (!isok)
		std::cout << "ERROR: Initial model not valid." << std::endl;
	QuantLib::Matrix allparms = ql_transpose_vec(pvec);
	// go through the MCMC simulation process
	for (iterationcounter_ = 0; iterationcounter_ < iterationlimit_;
		++iterationcounter_)
	{
		// register the iteration
		if (callback != nullptr)
			(*callback)(2, cb_parms);

		// carry out phi and theta updates
		for (i = 0; i < p_ + q_; ++i)
			if (localmask[i + 3])
			{
				const double delta = norm_random(MT, 0, 0.004);
				pvec[i + 3] += delta;

				// unbundle params
				UnbundleParameters(pvec);
				isok = CheckModel(false);
				accept = false;
				if (isok)
				{
					ty = LogPrior(isok);
					accept = isok;
					if (isok)
					{
						ty += ComputeLogLikelihood(ts);
						accept = log(MT.next().value) < (ty - tx);
					}
				}
				if (accept)
					tx = ty;
				else
					pvec[i + 3] -= delta;
			}


		// carry out sigma update
		const double scale = exp(norm_random(MT, 0, 0.0001));
		if (localmask[2])
		{
			pvec[2] *= scale;
			UnbundleParameters(pvec);
			ty = LogPrior(isok);
			accept = isok;
			if (isok)
			{
				ty += ComputeLogLikelihood(ts);
				accept = (log(MT.next().value) < (ty - tx));
			}
			if (accept)
				tx = ty;
			else
				pvec[2] /= scale;
		}

		// carry out mu update

		// carry out d update

		// make sure parameters are correct
		UnbundleParameters(pvec);

		// update matrix
		ql_append_bottom(allparms, pvec);
	} // end of iteration loop

	error_code_ = SUCCESS;

	// now fitted model is posterior mean
	ql_zeros(pvec);
	for (i = 100; i < iterationlimit_; ++i)
		for (int j = 0; j < pvec.size(); ++j)
			pvec[j] += allparms[i][j];
	pvec *= (1.0 / (iterationlimit_ - 100));

	UnbundleParameters(pvec);
}

void TS::TimeSeriesModel::StreamMLESummary(std::ostringstream& supplemental, const QuantLib::Matrix& parmcovs, bool get_parameter_cov)
// fill in info about fit
{
	int i, j;
	QuantLib::Array ps = ParameterBundle();
	const size_t np = ps.size();

	supplemental << "\\bMaximum Likelihood Estimation Results\\n" << std::endl << std::endl;
	supplemental << "\\uParameter estimates\\n" << std::endl << std::endl;
	for (i = 0; i < np; ++i)
	{
		supplemental << "Param. #" << (i + 1) << ": ";
		StreamParameterName(supplemental, i);
		if (get_parameter_cov)
		{
			supplemental << "\\n = " << ps[i] << " (";
			if (parmcovs[i][i] > 0)
				supplemental << sqrt(parmcovs[i][i]);
			if (parmcovs[i][i] == 0)
				supplemental << "NA";
			if (parmcovs[i][i] == -1.0)
				supplemental << "US";
			supplemental << ")";
		}
		supplemental << std::endl;
	}

	if (get_parameter_cov)
	{
		supplemental << std::endl << "Standard errors are given in parentheses." << std::endl;
		supplemental << "NA indicates parameter was held fixed or not estimated by maximizing the likelihood." << std::endl;
		supplemental << "US indicates that numerical differentation of the log-likelihood failed for the parameter." << std::endl;

		supplemental << std::endl << "\\uCorrelations of Parameter Estimates\\n" << std::endl << std::endl;

		supplemental << "\\m            ";  // switch to monospacing mode so table entries line up
		for (j = 0; j < np; ++j)
			supplemental << std::setw(2) << (j + 1) << "|";
		supplemental << std::endl;
		for (i = 0; i < np; ++i)
		{
			supplemental << "Param. #" << std::setw(2) << (i + 1) << " |";
			for (j = 0; j < np; ++j)
			{
				double corr = parmcovs[i][j];
				if (corr == -1.0)
					corr = 0;
				double tx = (parmcovs[i][i] * parmcovs[j][j]);
				if ((parmcovs[i][i] == -1.0) || (parmcovs[j][j] == -1.0))
					tx = 0.0;
				if (fabs(tx) < zero)
					supplemental << "--|";
				else
				{
					tx = sqrt(tx);
					corr /= tx;
					if (corr < -0.7)
						supplemental << "\\0  \\m|";
					else if (corr < -0.4)
						supplemental << "\\1  \\m|";
					else if (corr < -0.1)
						supplemental << "\\2  \\m|";
					else if (corr < 0.1)
						supplemental << "  |";
					else if (corr < 0.4)
						supplemental << "\\3  \\m|";
					else if (corr < 0.7)
						supplemental << "\\4  \\m|";
					else
						supplemental << "\\5  \\m|";
				}
			}
			supplemental << std::endl;
		}

		supplemental << std::endl << "\\nCorrelation Legend:\\m" << std::endl;
		supplemental << "|\\0  \\m| = [-1.0,-0.7]" << std::endl << "|\\1  \\m| = [-0.7,-0.4]" << std::endl << "|\\2  \\m| = [-0.4,-0.1]" << std::endl;
		supplemental << "|\\m  \\m| = [-0.1, 0.1]" << std::endl;
		supplemental << "|\\3  \\m| = [ 0.1, 0.4]" << std::endl << "|\\4  \\m| = [ 0.4, 0.7]" << std::endl << "|\\5  \\m| = [ 0.7, 1.0]" << std::endl << std::endl;
	}

	supplemental << std::ends;
}


int TS::ARMAModel::FitModel(const TimeSeries& ts, const int method, const int ilimit,
	void (*callback)(int, void*), void* cb_parms,
	std::ostringstream& msg, std::ostringstream& supplemental, bool get_parameter_cov)
{
	QuantLib::Array parms = ParameterBundle();
	//double corr, tx;

	// create mask
	QuantLib::Array mask = GetMask();
	iterationlimit_ = ilimit;

	if (method == METHOD_MLE)
	{
		if (ts.HasAnyMissing())
			if (!is_short_mem())
			{
				msg << "Unable to fit fractionally differenced model by maximum likelihood estimation when time series has missing observations."
					<< std::ends;
				return UNABLE;
			}

		const QuantLib::Matrix parmcovs = MLEFit(ts, mask, callback, cb_parms, get_parameter_cov);
		StreamMLESummary(supplemental, parmcovs, get_parameter_cov);

		msg << "Fitted model computed using " << iterationcounter_
			<< " likelihood evaluations." << std::endl;
		if (error_code_ != SUCCESS)
			msg << "Warning: Optimizer did not converge within specified tolerance."
			<< std::endl;
		msg << std::ends;
	}
	if (method == METHOD_BAYESIAN)
	{
		BayesianFit(ts, mask, callback, cb_parms);
		msg << "MCMC estimation carried out with " << ilimit
			<< " iterations." << std::endl << std::endl;
		supplemental << "Analysis of MCMC output not yet implemented." << std::ends;
	}

	int retval = SUCCESS;
	if (error_code_ != SUCCESS)
		retval = NONCONVERGENT;
	return retval;
}

/**
 * Given a fitted model, fill the vectors with the needed ACF and optionally the PACF
 */
void TS::ARMAModel::ComputeACFPACF(QuantLib::Array& acf/**<ACF vector*/, QuantLib::Array* pacf /**<Optional PACF vector*/, bool normalizeacf) const
// Method 3: B\&D, p. 95
{

	const size_t max_lag = acf.size() - 1;
	QuantLib::Array psi(q_ + 1);
	//int i;
	//double /*tx,*/ ty;
	//int j;
	if (is_short_mem()) // i.e. if it's not fractionally integrated
	{
		// first step: compute $\psi_0,\ldots,\psi_q$.
		psi[0] = 1.0;
		for (int i = 1; i <= q_; ++i)
		{
			if (i <= q_)
				psi[i] = theta_[i - 1];
			else
				psi[i] = 0.0;
			for (int k = 1; k <= i; ++k)
				if (k <= p_)
					psi[i] += phi_[k - 1] * psi[i - k];
		}

		// second step: solve Y-W for $\gamma(0),\ldots,\gamma(p)$
		QuantLib::Matrix phistuff(p_ + 1, p_ + 1);
		QuantLib::Array gammas(p_ + 1), psistuff(p_ + 1);
		ql_zeros(phistuff);
		for (int i = 0; i <= p_; ++i)
			for (int j = 0; j <= p_; ++j)
			{
				double tx = 1.0;
				if (j > 0)
					if (j <= p_)
						tx = -phi_[j - 1];
					else
					{
						tx = 0;
					}
				phistuff[i][abs(i - j)] += tx;
			}
		for (int i = 0; i <= p_; ++i)
		{
			psistuff[i] = 0;
			for (int j = i; j <= q_; ++j)
			{
				double tx = 1.0;
				if (j > 0)
					tx = theta_[j - 1];
				if (j > q_)
					tx = 0.0;
				psistuff[i] += tx * psi[j - i];
			}
			psistuff[i] *= sigma_ * sigma_;
		}

		//gammas = psistuff;
		//phistuff.solve_system(&gammas);
		gammas = ql_solve_system(phistuff, psistuff);
		// copy into return array
		for (int i = 0; i <= std::min(static_cast<size_t>(p_), max_lag); ++i)
			acf[i] = gammas[i];

		// and then compute the rest recursively
		for (int i = p_ + 1; i <= max_lag; ++i)
		{
			double tx = 0.0;
			for (int j = 1; j <= p_; ++j)
				tx += phi_[j - 1] * acf[i - j];
			if (i < std::max(p_, q_ + 1))
				for (int j = i; j <= q_; ++j)
				{
					double ty = 1.0;
					if (j > 0)
						ty = theta_[j - 1];
					if (j > q_)
						ty = 0.0;
					tx += sigma_ * sigma_ * ty * psi[j - i];
				}
			acf[i] = tx;
		}
	} // end of normal fracdiff==0.0 case
	else
	{
		// fractionally differenced case
		QuantLib::Array psis(q_ + 1);
		QuantLib::Matrix zetas(p_ + 1, 2);
		std::vector<complex> roots;
		complex tc;
		complex tc2;
		complex tc3;
		double tx2;
		int p2, l;
		p2 = 0;
		for (int i = 0; i < p_; ++i)
			if (fabs(phi_[i]) > zero)
				p2 = i + 1;				// p2 is the effective AR order, is < p if phi_p

				// compute psis
		for (int i = 0; i <= q_; ++i) {
			psis[i] = 0;
			for (int j = i; j <= q_; ++j)
			{
				double tx = j > 0 ? theta_[j - 1] : 1.0;
				tx2 = j > i ? theta_[j - 1 - i] : 1.0;
				psis[i] += tx * tx2;
			}
		}
		// get AR polynomial roots
		QuantLib::Array cp(p2 + 1);
		cp[0] = 1.0;
		for (int i = 1; i <= p2; ++i)
			cp[i] = -phi_[i - 1];
		if (p2 > 0)
			roots = FindComplexRoot(p2, cp);

		// transform to inverse roots
		for (int i = 0; i < p2; ++i)
			roots[i] = 1.0 / roots[i];

		// get zetas
		for (int i = 0; i < p2; ++i)
		{
			tc = 1.0;
			for (int j = 0; j < p2; ++j)
				tc *= (1.0 - roots[i] * roots[j]);
			for (int j = 0; j < p2; ++j)
				if (j != i)
					tc *= (roots[i] - roots[j]);
			tc = 1.0 / tc;
			zetas[i][0] = tc.real();
			zetas[i][1] = tc.imag();
		}

		// compute gamma
		QuantLib::Matrix C;
		int h;
		if (p2 == 0)
		{
			ql_zeros(acf);
			for (l = -q_; l <= q_; ++l)
			{
				int al = l < 0 ? -l : l;
				double tx = std::tgamma(1 - 2 * fracdiff_) * std::tgamma(fracdiff_ + l) /
					(std::tgamma(fracdiff_) * std::tgamma(1.0 - fracdiff_) * std::tgamma(1.0 - fracdiff_ + l));
				acf[0] += psis[al] * tx;
				for (h = 1; h <= max_lag; ++h)
				{
					tx *= (1.0 - fracdiff_ - h + l) / (fracdiff_ - h + l);
					acf[h] += psis[al] * tx;
				}
				acf = acf * sigma_ * sigma_;
			}
		}
		else
		{
			ql_zeros(acf);
			for (int j = 0; j < p2; ++j)
			{
				auto extent = static_cast<int>(2 * q_ + max_lag + 1);
				C = cfuncsfor(fracdiff_, roots[j].real(), roots[j].imag(),
					p2, p2 + q_, extent);
				for (l = -q_; l <= q_; ++l)
					for (h = 0; h <= max_lag; ++h)
					{
						tc = complex(zetas[j][0], zetas[j][1]);
						tc2 = complex(C[h + q_ - l][0], C[h + q_ - l][1]);
						tc3 = sigma_ * sigma_ * psis[l < 0 ? -l : l] * tc * tc2;
						acf[h] += tc3.real();  // we know imag. parts will cancel!
					}
			}
		}
	}

	if (normalizeacf)
		for (int i = static_cast<int>(max_lag); i >= 0; --i)
			acf[i] /= acf[0];

	// Second part: compute the model partial autocorrelation function
	if (pacf != nullptr)
		ACFtoPACF(acf, *pacf);
}

//original implementation
double TS::ARMAModel::kappaOld(int i, int j, int m, const QuantLib::Array& acf) const
{
	int m1 = std::min(i, j), m2 = std::max(i, j), ti, r;
	double sum, /*tx,*/ t1, t2;

	if (m2 <= m)
		return acf[abs(i - j)] / (sigma_ * sigma_);
	if (m1 > m)
	{
		sum = 0.0;
		for (r = 0; r <= q_; ++r)
		{
			if (r == 0)
				t1 = 1.0;
			else
				t1 = theta_[r - 1];
			ti = r + abs(i - j);
			if (ti == 0)
				t2 = 1.0;
			else if (ti <= q_)
				t2 = theta_[ti - 1];
			else
				t2 = 0.0;
			sum += t1 * t2;
		}
		return sum;
	}
	if ((m1 <= m) && (m2 > m) && (m2 <= 2 * m))
	{
		sum = acf[abs(i - j)];
		for (r = 1; r <= p_; ++r)
		{
			ti = r - abs(i - j);
			sum -= phi_[r - 1] * acf[abs(ti)];
		}
		return sum / (sigma_ * sigma_);
	}
	return 0;
}

//new implementation with better performance
double TS::ARMAModel::kappaNew(const int i, const int j, int m, const QuantLib::Array& acf) const
{
	const int m1 = std::min(i, j);
	const int m2 = std::max(i, j);
	const unsigned int delta_i_j = std::abs(i - j);
	const double sigma2 = sigma_ * sigma_;
	double res = 0;
	if (m2 <= m) {
		res = acf[delta_i_j] / sigma2;
	}
	else {
		if (m1 > m)
		{
			double sum = 0.0;
			for (unsigned int r = 0; r <= static_cast<unsigned int>(q_); ++r)
			{
				const double t1 = (r == 0 ? 1.0 : theta_[r - 1]);
				const unsigned int ti = r + delta_i_j;
				if (ti <= static_cast<unsigned int>(q_))
				{
					const double t2 = (ti == 0 ? 1.0 : theta_[ti - 1]);
					sum += t1 * t2;
				}
			}
			res = sum;
		}
		else {
			if ((m1 <= m) && (m2 > m) && (m2 <= 2 * m))
			{
				double sum = acf[delta_i_j];
				for (unsigned int r = 1; r <= static_cast<unsigned int>(p_); ++r)
				{
					const int ti = std::abs<int>(r - delta_i_j);
					sum -= phi_[r - 1] * acf[ti];
				}
				res = sum / sigma2;
			}
		}
	}
	return res;
}

void TS::ARMAModel::ForecastWithMissing(const TimeSeries& tp, int nsteps,
	double* forecret, double* fmse) const
{
	// This function uses the state-space model formulation of the ARMA model
	// to get one-step predictors, residuals, and additional forecasts.
	// See comments under Forecast(...) below for details on parameters
	// If fracdiff!=0 then this routine fails!
	int i, /*j,*/ t, nobs = tp.GetN();

	if (is_short_mem())
	{
		// set up standard SS model representation for ARMA model
		// (note: ARMA model is zero-mean version)
		QuantLib::Matrix F, G, Q;
		QuantLib::Array resids(nobs), rs(nobs + nsteps);
		double yhat, ysig2, ri, ss, slri;
		int nonmissing = 0;

		int r = std::max(p_, q_ + 1);
		F = ql_resize(F, r, r);
		ql_zeros(F);

		for (i = 0; i < r - 1; ++i)
			F[i][i + 1] = 1.0;
		for (i = 0; i < p_; ++i)
			F[r - 1][r - 1 - i] = phi_[i];
		G = ql_resize(G, 1, r);
		ql_zeros(G);
		for (i = 1; i <= q_; ++i)
			G[0][r - 1 - i] = theta_[i - 1];
		G[0][r - 1] = 1.0;
		Q = ql_resize(Q, r, r);
		ql_zeros(Q);
		Q[r - 1][r - 1] = sigma_ * sigma_;

		// now run the Kalman Filter
		QuantLib::Matrix Sigmat(r, r);  // filtering d-n variance
		QuantLib::Array mut(r);       // filtering d-n mean 
		QuantLib::Matrix Ktp1, Ptp1;

		// initialize them
		ql_zeros(mut);

		Sigmat = Q;
		for (i = 0; i < KFburnin; ++i)
		{
			mut = F * mut;
			Sigmat = F * Sigmat * transpose(F) + Q;
		}
		// now we have mut and Sigmat for t==-1

		/*loglikelihood_ =*/ ss = slri = 0.0;  // really this will be reduced log-likelihood
		for (t = 0; t < nobs; ++t)
			// update for t
		{
			// 1. add in contribution to reduced log-likelihood 
			// (see B&D, eq. 8.7.4, from
			//    one-step predictive based on current mut, Sigmat
			yhat = (G * F * mut)[0];
			ysig2 = (G * (F * Sigmat * transpose(F) + Q) * transpose(G))[0][0];
			ri = ysig2 / (sigma_ * sigma_);
			rs[t] = ri;
			if (!tp.IsMissing(t))
			{
				++nonmissing;
				slri += log(ri);
				resids[t] = (yhat - (tp.data[0][t] - mean_)) / sqrt(ri);
				ss += resids[t] * resids[t];
			}
			else
				// else just add the constant 0.0 to loglikelihood, and set resid=0
				resids[t] = 0.0;

			// 2. iterate to obtain next mut, Sigmat
			if (!tp.IsMissing(t))
			{
				Ptp1 = F * Sigmat * transpose(F) + Q;
				double g = (G * Ptp1 * transpose(G))[0][0];
				Ktp1 = Ptp1 * transpose(G) * (1.0 / g);
				Sigmat = Ptp1 - Ktp1 * G * Ptp1;  // Sigmat<-Sigma_{t+1}
				const auto t_mat = Ktp1 * ((tp.data[0][t] - mean_) - (G * F * mut)[0]);
				mut = F * mut + to_array(t_mat);
			}
			else
			{
				mut = F * mut;
				Sigmat = F * Sigmat * transpose(F) + Q;
			}
		}

		for (i = 0; i < nsteps; ++i) // fill in forecasts if necessary
		{
			yhat = (G * F * mut)[0];
			ysig2 = (G * (F * Sigmat * transpose(F) + Q) * transpose(G))[0][0];
			forecret[i] = yhat + mean_;
			fmse[i] = ysig2;
			mut = F * mut;
			Sigmat = F * Sigmat * transpose(F) + Q;
		}
	}
	else
	{
		std::cout << "Missing data not supported when d not equal to 0." << std::endl;
	}
}

void TS::ARMAModel::ComputeSpecialResidualsWithMissing(const TimeSeries& tp, TimeSeries* residreturn, double* rstore) const
{
	// This function uses the state-space model formulation of the ARMA model
	// to get one-step predictors, residuals, and additional forecasts.
	// See comments under Forecast(...) below for details on parameters
	// If fracdiff!=0 then this routine fails!
	int i, /*j,*/ t, nobs = tp.GetN();

	if (is_short_mem())
	{
		// set up standard SS model representation for ARMA model
		// (note: ARMA model is zero-mean version)
		QuantLib::Matrix F, G, Q;
		QuantLib::Array resids(nobs), rs(nobs);
		double yhat;
		double ysig2;
		double ri;
		double ss;
		double slri;
		int nonmissing = 0;

		int r = std::max(p_, q_ + 1);
		F = ql_resize(F, r, r);
		ql_zeros(F);
		for (i = 0; i < r - 1; ++i)
			F[i][i + 1] = 1.0;
		for (i = 0; i < p_; ++i)
			F[r - 1][r - 1 - i] = phi_[i];
		G = ql_resize(G, 1, r);
		ql_zeros(G);
		for (i = 1; i <= q_; ++i)
			G[0][r - 1 - i] = theta_[i - 1];
		G[0][r - 1] = 1.0;
		Q = ql_resize(Q, r, r);
		ql_zeros(Q);
		Q[r - 1][r - 1] = sigma_ * sigma_;

		// now run the Kalman Filter
		QuantLib::Matrix Sigmat(r, r);  // filtering d-n variance
		QuantLib::Array mut(r);       // filtering d-n mean 
		QuantLib::Matrix Ktp1, Ptp1;

		// initialize them
		ql_zeros(mut);
		Sigmat = Q;
		for (i = 0; i < KFburnin; ++i)
		{
			mut = F * mut;
			Sigmat = F * Sigmat * transpose(F) + Q;
		}
		// now we have mut and Sigmat for t==-1

		/*loglikelihood_ =*/ ss = slri = 0.0;  // really this will be reduced log-likelihood
		for (t = 0; t < nobs; ++t)
			// update for t
		{
			// 1. add in contribution to reduced log-likelihood 
			// (see B&D, eq. 8.7.4, from
			//    one-step predictive based on current mut, Sigmat
			yhat = (G * F * mut)[0];
			ysig2 = (G * (F * Sigmat * transpose(F) + Q) * transpose(G))[0][0];
			ri = ysig2 / (sigma_ * sigma_);
			rs[t] = ri;
			if (!tp.IsMissing(t))
			{
				++nonmissing;
				slri += log(ri);
				resids[t] = (yhat - (tp.data[0][t] - mean_)) / sqrt(ri);
				ss += resids[t] * resids[t];
			}
			else
				// else just add the constant 0.0 to loglikelihood, and set resid=0
				resids[t] = 0.0;

			// 2. iterate to obtain next mut, Sigmat
			if (!tp.IsMissing(t))
			{
				Ptp1 = F * Sigmat * transpose(F) + Q;
				double g = (G * Ptp1 * transpose(G))[0][0];
				Ktp1 = Ptp1 * transpose(G) * (1.0 / g);
				Sigmat = Ptp1 - Ktp1 * G * Ptp1;  // Sigmat<-Sigma_{t+1}
				const auto t_mat = Ktp1 * ((tp.data[0][t] - mean_) - (G * F * mut)[0]);
				mut = F * mut + to_array(t_mat);
			}
			else
			{
				mut = F * mut;
				Sigmat = F * Sigmat * transpose(F) + Q;
			}
		}

		if (residreturn != nullptr)
		{
			residreturn->ClearOut();
			for (i = 0; i < nobs; ++i)
				residreturn->Append(resids[i], tp.IsMissing(i));
		}
		if (rstore != nullptr)
			for (i = 0; i < nobs; ++i)
				rstore[i] = rs[i];
		//loglikelihood_is_valid_ = true;
	}
	else
	{
		// fill in garbage
		if (residreturn != nullptr)
		{
			residreturn->ClearOut();
			for (i = 0; i < nobs; ++i)
				residreturn->Append(0, false);
		}
		if (rstore != nullptr)
			for (i = 0; i < nobs; ++i)
				rstore[i] = 1.0;
		//loglikelihood_is_valid_ = false;
	}
}

void TS::ARMAModel::ComputeStdResiduals(const TimeSeries& tp, TimeSeries& resids)
{
	const int n = tp.GetN();
	QuantLib::Array rret(n);

	ComputeSpecialResiduals(tp, &resids, rret.begin());
	for (int i = 0; i < n; ++i)
		resids[i] /= sigma_;
}

void TS::ARMAModel::ComputeSpecialResiduals(const TimeSeries& tp, TimeSeries* rret, double* rstore) const
{
	// This function uses the approach described
	// in Section 8.7 of B&D if fracdiff parm==0
	// to get one-step predictors, residuals, and additional forecasts

	// If fracdiff!=0 then it uses the D-L algorithm
	// with ACF computed by Sowell's formula.

	// 1.  resids are stored in rret if it's non-NULL
	// 2.  rs[...] are one-step predictive MSEs / sigma^2,
	//     based on sigma when this routine was called
	//     rs[...] are put in rstore if it's non-NULL
	// 3.  this function returns (*rret) =  [ (x_i - \hat{x}_i)/\sqrt{rs_{i-1}} ]

	if (tp.HasAnyMissing())
	{
		ComputeSpecialResidualsWithMissing(tp, rret, rstore);
		return;
	}

	int /*h, */i, j, k, n, nobs = tp.GetN();
	QuantLib::Array acf, xhat(std::max(nobs + 1, 1)), residuals(nobs);
	double t1, sum, tx;
	QuantLib::Array rs(nobs + 1);
	double* tsdata = tp.GetData();

	if (is_short_mem())
	{
		const int m = std::max(p_, q_);
		acf.resize(m + 1);
		// first we need to compute Gamma(0..m)
		ComputeACFPACF(acf, nullptr, false);

		// then apply the innovations algorithm to compute $theta_{n,j}$s
		const int minwidth = std::max(std::max(q_, m - 1), 1);
		QuantLib::Matrix thetas(nobs, minwidth);

		rs[0] = kappa(1, 1, m, acf);
		for (n = 1; n <= nobs; ++n)
		{
			// first find $\theta_{n,q},\ldots,\theta_{n,1}$
			for (k = std::max(n - minwidth, 0); k < n; ++k)
			{
				sum = kappa(n + 1, k + 1, m, acf);
				for (j = std::max(n - minwidth, 0); j < k; ++j)
				{
					if ((k - j - 1) < minwidth)
						t1 = thetas[k - 1][k - j - 1];
					else
						t1 = 0.0;
					const double t2 = thetas[n - 1][n - j - 1];
					sum -= t1 * t2 * rs[j];
				}
				thetas[n - 1][n - k - 1] = sum / rs[k];
			}

			// then find $r_n$
			rs[n] = kappa(n + 1, n + 1, m, acf);
			for (j = std::max(n - minwidth, 0); j < n; ++j)
			{
				t1 = thetas[n - 1][n - j - 1];
				rs[n] -= t1 * t1 * rs[j];
			}
		}

		// now compute all the one-step predictors and the residuals    
		xhat[0] = mean_;
		for (n = 1; n < nobs; ++n)
		{
			// work out $\hat{X}_{n+1}$
			sum = 0.0;
			if (n < m)
			{
				for (j = 1; j <= std::min(n, minwidth); ++j)
					sum += thetas[n - 1][j - 1] * (tsdata[n - j] - xhat[n - j]);
			}
			else
			{
				for (j = 1; j <= p_; ++j)
					sum += phi_[j - 1] * (tsdata[n - j] - mean_);
				for (j = 1; j <= minwidth; ++j)
					sum += thetas[n - 1][j - 1] * (tsdata[n - j] - xhat[n - j]);
			}
			xhat[n] = sum + mean_;
		}
	} // end of fracdiff==0.0 case
	else
	{
		// USE the standard Durbin-Levison Algorithm
		acf.resize(nobs);
		// first we need to compute Gamma(0..m)
		ComputeACFPACF(acf, nullptr, 0);

		// Now apply the Durbin-Levinson algorithm to compute 
		// all the one-step predictors and their variances
		const int ncols = nobs;
		QuantLib::Array a(ncols), olda(ncols);

		rs[0] = acf[0];  // nu(0)
		xhat[0] = mean_;
		for (i = 1; i <= ncols; ++i)
		{
			for (j = 0; j < ncols; ++j)
				olda[j] = a[j];

			// compute the new a vector
			for (j = 1, sum = 0.0; j < i; ++j)
				sum += olda[j - 1] * acf[i - j];
			a[i - 1] = 1 / rs[i - 1] * (acf[i] - sum);
			for (k = 0; k < i - 1; ++k)
				a[k] = olda[k] - a[i - 1] * olda[i - 2 - k];

			// update nu
			rs[i] = rs[i - 1] * (1 - a[i - 1] * a[i - 1]);

			for (j = 0, tx = 0.0; j < i; ++j)
				tx += a[j] *
				(i - 1 - j < nobs ? (tsdata[i - 1 - j] - mean_) : (xhat[i - 1 - j] - mean_));
			xhat[i] = tx + mean_;
		}

		for (i = 0; i < ncols; ++i)
			rs[i] = rs[i] / (sigma_ * sigma_);
	}

	// store the results
	for (n = 0; n < nobs; ++n) {
		residuals[n] = (tsdata[n] - xhat[n]) / sqrt(rs[n]);;
	}

	// construct new time series for the residuals
	if (rret != nullptr)
	{
		//Does reallocation
		if (rret->current_length != nobs)
		{
			rret->ClearOut();
			for (i = 0; i < nobs; ++i) {
				rret->Append(residuals[i]);
			}
		}
		else
		{
			for (i = 0; i < nobs; ++i) {
				rret->SetValues(i, residuals[i]);
			}
		}
	}

	if (rstore != nullptr)
		for (i = 0; i < nobs; ++i)
			rstore[i] = rs[i];

	//loglikelihood_is_valid_ = true;
}

void TS::ARMAModel::Forecast(const TimeSeries& tp, int nsteps, double* fret, double* fmse)
{
	// This function uses the approach described
	// in Section 8.7 of B&D if fracdiff parm==0
	// to get one-step predictors, residuals, and additional forecasts

	// If fracdiff!=0 then it uses the D-L algorithm
	// with ACF computed by Sowell's formula.

	// 1.  forecasts (1..nsteps predictive means) are stored in fret if it's non-NULL
	// 2.  predictive variances are returned in fmse if it's non-NULL


	if (tp.HasAnyMissing())
	{
		ForecastWithMissing(tp, nsteps, fret, fmse);
		return;
	}

	int h, i, j, k, n, nobs = tp.GetN();
	QuantLib::Array acf, xhat(std::max(nobs + nsteps + 1, 1)), residuals(nobs);
	double t1, t2, sum, tx;
	QuantLib::Array rs(nobs + 1 + nsteps), local_fmse(nsteps + 1);
	double* tsdata = tp.GetData();

	if (is_short_mem())
	{
		int m = std::max(p_, q_);

		// first we need to compute Gamma(0..m)
		acf.resize(m + 1);
		ComputeACFPACF(acf, nullptr, 0);

		// then apply the innovations algorithm to compute $theta_{n,j}$s
		int minwidth = std::max(std::max(q_, m - 1), 1);
		QuantLib::Matrix thetas(nobs + nsteps, minwidth);

		rs[0] = kappa(1, 1, m, acf);
		for (n = 1; n <= nobs + nsteps; ++n)
		{
			// first find $\theta_{n,q},\ldots,\theta_{n,1}$
			for (k = std::max(n - minwidth, 0); k < n; ++k)
			{
				sum = kappa(n + 1, k + 1, m, acf);
				for (j = std::max(n - minwidth, 0); j < k; ++j)
				{
					if ((k - j - 1) < minwidth)
						t1 = thetas[k - 1][k - j - 1];
					else
						t1 = 0.0;
					t2 = thetas[n - 1][n - j - 1];
					sum -= t1 * t2 * rs[j];
				}
				thetas[n - 1][n - k - 1] = sum / rs[k];
			}

			// then find $r_n$
			rs[n] = kappa(n + 1, n + 1, m, acf);
			for (j = std::max(n - minwidth, 0); j < n; ++j)
			{
				t1 = thetas[n - 1][n - j - 1];
				rs[n] -= t1 * t1 * rs[j];
			}
		}

		// now compute all the one-step predictors and the residuals    
		xhat[0] = mean_;
		for (n = 1; n < nobs; ++n)
		{
			// work out $\hat{X}_{n+1}$
			sum = 0.0;
			if (n < m)
			{
				for (j = 1; j <= std::min(n, minwidth); ++j)
					sum += thetas[n - 1][j - 1] * (tsdata[n - j] - xhat[n - j]);
			}
			else
			{
				for (j = 1; j <= p_; ++j)
					sum += phi_[j - 1] * (tsdata[n - j] - mean_);
				for (j = 1; j <= minwidth; ++j)
					sum += thetas[n - 1][j - 1] * (tsdata[n - j] - xhat[n - j]);
			}
			xhat[n] = sum + mean_;
		}

		// now compute the h-step predictors and their MSEs: 
		//   store the MSEs(/sig^2) in local_fmse[0,...,h-1]
		QuantLib::Array psis(nsteps);
		psis[0] = 1.0;            // other components are updated in loop below
		for (h = 1; h <= nsteps; ++h)
		{
			// first get P_n X_{n+h}
			sum = 0.0;
			if (h <= m - nobs)
			{
				for (j = h; j <= nobs + h - 1; ++j)
					if (j - 1 < minwidth)
						sum += thetas[nobs + h - 2][j - 1] *
						(tsdata[nobs + h - j - 1] - xhat[nobs + h - j - 1]);
			}
			else
			{
				for (i = 1; i <= p_; ++i)
				{
					if (h > i)
						sum += phi_[i - 1] * (xhat[nobs + h - i - 1] - mean_);
					else
						sum += phi_[i - 1] * (tsdata[nobs + h - i - 1] - mean_);
				}
				for (j = h; j <= minwidth; ++j)
					sum += thetas[nobs + h - 2][j - 1] *
					(tsdata[nobs + h - j - 1] - xhat[nobs + h - j - 1]);
			}
			xhat[nobs + h - 1] = sum + mean_;

			// then get its MSE: using approximation (5.3.24) from B&D
			if (h == 1)
				local_fmse[h - 1] = 1.0;
			else
			{
				// compute psis[h-1]
				j = h - 1;
				psis[j] = (j - 1 < q_ ? theta_[j - 1] : 0);
				for (k = 1; k <= p_; ++k)
					psis[j] += phi_[k - 1] * (j >= k ? psis[j - k] : 0);
				// update mse
				local_fmse[h - 1] = local_fmse[h - 2] + psis[h - 1] * psis[h - 1];
			}
		}
		// now fix local_fmse
		local_fmse = local_fmse * sigma_ * sigma_;

	} // end of fracdiff==0.0 case
	else
	{
		// USE the standard Durbin-Levison Algorithm
		acf.resize(nobs + nsteps);
		// first we need to compute Gamma(0..m)
		ComputeACFPACF(acf, nullptr, 0);

		// Now apply the Durbin-Levinson algorithm to compute 
		// all the one-step predictors and their variances
		int ncols = nobs + nsteps;
		QuantLib::Array a(ncols), olda(ncols);

		rs[0] = acf[0];  // nu(0)
		xhat[0] = mean_;
		for (i = 1; i <= ncols; ++i)
		{
			for (j = 0; j < ncols; ++j)
				olda[j] = a[j];

			// compute the new a vector
			for (j = 1, sum = 0.0; j < i; ++j)
				sum += olda[j - 1] * acf[i - j];
			a[i - 1] = 1 / rs[i - 1] * (acf[i] - sum);
			for (k = 0; k < i - 1; ++k)
				a[k] = olda[k] - a[i - 1] * olda[i - 2 - k];

			// update nu
			rs[i] = rs[i - 1] * (1 - a[i - 1] * a[i - 1]);

			for (j = 0, tx = 0.0; j < i; ++j)
				tx += a[j] *
				(i - 1 - j < nobs ? (tsdata[i - 1 - j] - mean_) : (xhat[i - 1 - j] - mean_));
			xhat[i] = tx + mean_;
		}

		for (i = 0; i < ncols; ++i)
			rs[i] = rs[i] / (sigma_ * sigma_);

		// now compute MSEs: use same approx. as above, but
		// compute psis by phi(B) (1-B)^d psi(B) = theta(B) and matching coefficients!
		QuantLib::Array psis(nsteps), xis(nsteps);
		psis[0] = 1.0;
		xis[0] = 1.0;

		// first compute xis = expansion (1-B)^d = \sum B^j xis[j]
		for (i = 1; i < nsteps; ++i)
			xis[i] = -xis[i - 1] * (fracdiff_ - (i - 1)) / static_cast<double>(i);

		// then do psis: the recursion I derived is
		//
		//  \psi_k = \theta_k + \sum_{m=0}^{k-1} \psi_m \sum_{i=0}^{\max(p,k-m)} \phi_i \xi_{k-m-i},
		//  with the convention that \phi_0 = -1.0
		for (k = 1; k < nsteps; ++k)
		{
			psis[k] = k - 1 < q_ ? theta_[k - 1] : 0;
			for (j = 0; j < k; ++j)
			{
				sum = 0.0;
				for (i = 0; i <= std::max(p_, k - j); ++i)
					sum += (i == 0 ? -1.0 : phi_[i - 1]) * xis[k - j - i];
				psis[k] += psis[j] * sum;
			}
		}

		// finally, use approx. (B&D eqn (5.3.24)) as before
		local_fmse[0] = 1.0;
		for (i = 1; i < nsteps; ++i)
			local_fmse[i] = local_fmse[i - 1] + psis[i] * psis[i];
		local_fmse = local_fmse * sigma_ * sigma_;
	}

	// actual forecasts
	if (fret != nullptr)
		for (i = 1; i <= nsteps; ++i)
			fret[i - 1] = xhat[nobs - 1 + i];

	// fill in the predictive variances
	if (fmse != nullptr)
		for (i = 0; i < nsteps; ++i)
			fmse[i] = local_fmse[i];

}

double TS::ARMAModel::ComputeLogLikelihood(const TimeSeries& tp) const
// computes loglikelihood and AICC, possibly using data with
// missing values (fracdiff not yet supported for missing vals),
// returns loglikelihood, also sets loglikelihood and AICC
// members of TS::ARMAModel class
{
	int i;
	const int nobs = tp.GetN();
	int nonmissing;
	TimeSeries resids(nobs, 1);

	QuantLib::Array rs(nobs + 1);
	double loglikelihood = 0.0;
	if (nobs == 0)
		return 0.0;  // no data!
#ifdef _ALLOW_SPECTRAL_METHODS

	if (using_whittle_)
	{
		// use Whittle's approximation to likelihood!
		loglikelihood_ = nobs * log(2 * M_PI) + 2 * nobs * log(sigma_);
		QuantLib::Array omegas, pdgm;
		bool using_cache = false;
		if (whittle_cached_)
			if ((*tp) == whittle_ts_)
				using_cache = true;
			else
				whittle_cached_ = false;  // reset cache when time series doesnt match
		if (using_cache)
		{
			pdgm = whittle_pdgm_;
			omegas.resize(nobs);
			for (i = 0; i < nobs; ++i)
				omegas[i] = 2 * M_PI * i / nobs;
		}
		else
		{
			pdgm = tp->ComputePeriodogram(&omegas);
			whittle_pdgm_ = pdgm;
			whittle_cached_ = true;
			whittle_ts_ = (*tp);
		}

		omegas[0] = omegas[1];  // no zero frequency!
		QuantLib::Array actualspecdens = ComputeSpectralDensity(omegas);

		actualspecdens *= (2 * M_PI / (sigma_ * sigma_));
		for (i = 1; i < pdgm.size(); ++i)
		{
			loglikelihood_ += pdgm[i] / actualspecdens[i] / (sigma_ * sigma_);
			loglikelihood_ += log(actualspecdens[i]);
		}
		loglikelihood_ /= -2.0;
		loglikelihood_is_valid_ = true;
	}
	else
#endif
	{
		ComputeSpecialResiduals(tp, &resids, rs.begin());
		double* res = resids.GetData();

		// now it is easy to compute the likelihood
		// this is a straightforward implementation of eqn (8.7.4) in Brockwell & Davis,
		// Time Series: Theory and Methods (2nd edition)
		for (i = 0, nonmissing = 0; i < nobs; ++i)
			if (!tp.IsMissing(i))
			{
				++nonmissing;
				loglikelihood -= std::log(rs[i]) / 2;
				loglikelihood -= res[i] * res[i] / (2 * sigma_ * sigma_);
			}
		loglikelihood -= std::log(2 * M_PI * sigma_ * sigma_) * nonmissing / 2.0;
	}
	return loglikelihood;
}

//From B&D Section 9.3, pg. 303
void TS::ARMAModel::ComputeAdditionalStats(const TimeSeries& tp)
{
	aic_ = 0;
	aicc_ = 0;
	bic_ = 0;
	int nonmissing = 0;
	double m2 = 0;
	for (int i = 0; i < tp.GetN(); ++i)
		if (!tp.IsMissing(i))
		{
			m2 += std::pow(tp.GetData()[i], 2);
			++nonmissing;
		}
	const int p_plus_q = p_ + q_;
	aic_ = -2 * loglikelihood_ + 2 * (p_plus_q + 1.0) * nonmissing;
	if (nonmissing  > p_plus_q+ 2)
	{
		aicc_ = -2 * loglikelihood_ + 2 * (p_plus_q + 1.0) * nonmissing / (nonmissing - p_plus_q - 2.0);
		if (fabs(fracdiff_) > zero) {
			aicc_ += 2.0;
		}
	}
	if (nonmissing > p_plus_q){
		const double sigma2 = sigma_ * sigma_;
		bic_ = (nonmissing - p_plus_q) * std::log(nonmissing * sigma2 / (nonmissing - p_plus_q));
		bic_ += nonmissing * (1 + std::log(sqrt(2 * M_PI)));
		bic_ += p_plus_q * std::log((m2 - nonmissing * sigma2) / p_plus_q);
	}
}


QuantLib::Matrix TS::ARMAModel::cfuncsfor(double d, double rho_real, double rho_imag,
                                      int power, int h, int extent) const
	// returns vector C^*(d,h,rho)...C^*(d,h-extent,rho)
{
	int i, j;
	complex rho(rho_real, rho_imag);
	QuantLib::Matrix result(extent, 2);
	ql_zeros(result);
	// Deal with numerical problems as in Doornik & Ooms
	const int glen = 2 * (extent - 2) + 1, gmid = extent - 2;
	QuantLib::Matrix gval(glen, 2);

	// Step 1: compute gval vector
	//    gval(j) = G(d+i,1-d+i,rho), i=j-gmid
	double c = 1 - d + extent - 2;
	double a = d + extent - 2;
	complex tc = sf_hyperg_2F1(a, 1, c, rho_real, rho_imag);
	tc -= complex(1, 0);
	tc /= rho; // fix as rho->0 !
	gval(glen - 1, 0) = tc.real();
	gval(glen - 1, 1) = tc.imag();


	// then the rest are computed recursively
	for (i = extent - 3, j = 2; i >= -extent + 2; --i, ++j)
	{
		c = 1 - d + i;
		a = d + i;
		tc = complex(gval[glen - j + 1][0], gval[glen - j + 1][1]);
		tc = (a / c) * (1.0 + rho * tc);
		gval(glen - j, 0) = tc.real();
		gval(glen - j, 1) = tc.imag();
	}

	// Step 2: compute C function
	const double c0 = std::tgamma(1 - 2 * d) * std::tgamma(d + h);
	const double c1 = std::tgamma(1 - d + h) * std::tgamma(1 - d) * std::tgamma(d);
	double c0onc1 = c0 / c1;
	complex tc2 = complex(gval[gmid + h][0], gval[gmid + h][1]);
	complex tc3 = complex(gval[gmid - h][0], gval[gmid - h][1]);
	tc = c0onc1 * (pow(rho, 2 * power) * tc2 + pow(rho, 2 * power - 1) + tc3);
	result[0][0] = tc.real();
	result[0][1] = tc.imag();
	for (i = 1; i < extent; ++i)
	{
		const int lh = h - i;
		c0onc1 *= (1 - d + lh) / (d + lh);
		tc2 = complex(gval[gmid + h - i][0], gval[gmid + h - i][1]);
		tc3 = complex(gval[gmid - h + i][0], gval[gmid - h + i][1]);
		tc = c0onc1 * (pow(rho, 2 * power) * tc2 + pow(rho, 2 * power - 1) + tc3);
		result[i][0] = tc.real();
		result[i][1] = tc.imag();
	}

	return result;
}
#ifdef _ALLOW_SPECTRAL_METHODS
QuantLib::Array TS::ARMAModel::ComputeSpectralDensity(QuantLib::Array& omegas)
{
	int j;
	QuantLib::Array retval(omegas.size());
	complex tc2, tsum;

	for (int i = 0; i < omegas.size(); ++i)
	{
		complex ilambda(0, -omegas[i]);
		complex eil = exp(ilambda);

		complex tc1 = tsum = 1.0;
		for (j = 0; j < q_; ++j)
		{
			tc1 *= eil;
			tsum += theta_[j] * tc1;
		}
		// now tsum is theta(exp(-i lambda)))
		complex thetapart = tsum;

		tc1 = tsum = 1.0;
		for (j = 0; j < p_; ++j)
		{
			tc1 *= eil;
			tsum -= phi_[j] * tc1;
		}
		// now tsum is phi(exp(-i lambda)))
		complex phipart = tsum;

		retval[i] = sigma_ * sigma_ / (2 * M_PI)
			* (abs(thetapart) * abs(thetapart)) / (abs(phipart) * abs(phipart))
			* pow(abs(1.0 - eil), -2 * fracdiff_);
	}

	return retval;
}

#endif
