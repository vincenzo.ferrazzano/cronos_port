﻿#pragma once
#include "conversion_layer.hpp"
#include "timeseries.hpp"

// The following class defines a model.
namespace TS
{
	class TimeSeriesModel;

	class MinimizerInfoContainer {
	public:
		TimeSeriesModel* tsmp;
		const TimeSeries* tsp;
		QuantLib::Array themask;
	};
	
	//Class of packed mll parameters
	class ql_mll_parm {
	public:
		int i;
		int j;

		QuantLib::Array x;
		MinimizerInfoContainer* minparms;
	};
	
	//Why this global????
	inline double penaltymultiplier = 0.0;

	class TimeSeriesModel {
	protected:
		double loglikelihood_ = 0;
		double aic_ = 0;
		double aicc_ = 0;
		double bic_ = 0;
		bool loglikelihood_is_valid_ = false;
		QuantLib::Array estimation_mask_;            // mask used in estimation (1=estimate, 0=hold fixed)
		int iterationcounter_ = 0;
		int iterationlimit_ = 0;
		int error_code_ = 0;

	public:
		TimeSeriesModel() = default;
		virtual ~TimeSeriesModel() = default;

		QuantLib::Matrix MLEFit(const TimeSeries& ts, QuantLib::Array& mask,
			void (*callback)(int, void*) = nullptr, void* cb_parms = nullptr,
			bool get_parameter_cov = true);    // returns covariance 
	   // matrix of parameter estimates

//---------------------------------------------------------------------+
// Here are the functions which MUST be overridden in derived classes. |
//---------------------------------------------------------------------+

		virtual int FitModel(const TimeSeries& ts,
			const int method,
			const int numits,  // typically this f-n calls MLEFit
			void (*itercallback)(int, void*),
			void* cb_parms,
			std::ostringstream& msg,
			std::ostringstream& supplementalinfo,
			bool get_parameter_cov = true) = 0;

		virtual void SimulateInto(TimeSeries&, int n, bool ovw) const = 0;               // generates a simulated realization
		virtual void RenderInto(std::ostringstream& output) = 0;                         // streams a description of the model
																				  // ends i NOT appended
		virtual void ComputeACFPACF(QuantLib::Array& acf, QuantLib::Array* pacf, bool normalizeacf = true) const = 0;                    // autocorrelation/covariance+PACF

		[[nodiscard]]
		virtual void Forecast(const TimeSeries&, int nsteps, double* fret, double* fmse) = 0;  // fill in predictive means and MSEs

		[[nodiscard]]
		virtual QuantLib::Matrix SimulatePredictive(const TimeSeries&, int nsteps, int nsims) const = 0;             // simulate nsteps into the future,
																				  // returning future values in a vector 
		virtual void ComputeStdResiduals(const TimeSeries& tp, TimeSeries& resids) = 0; // if model is correct, these should be
																						// realizations of iid std. normals
		[[nodiscard]]
		virtual double ComputeLogLikelihood(const TimeSeries&) const = 0;                      // compute log-likelihood with curr. parms
		virtual void ComputeAdditionalStats(const TimeSeries&) = 0;
		[[nodiscard]]
		virtual QuantLib::Array ParameterBundle() const = 0;                                     // returns all parms bundled into vector
		virtual void UnbundleParameters(const QuantLib::Array& v) = 0;                           // takes parms from vector
																				  // and copies into model
		[[nodiscard]]
		virtual QuantLib::Array ValidityConstraint() const = 0;                                    // checks validity of model parms:
																				  // all vec. components must be >= 0
		[[nodiscard]]
		virtual QuantLib::Array GetMask() const = 0;                                               // returns estimation_mask, 
																				  // padded with defaults (1=estimate)
		virtual QuantLib::Array GetDefaultParameters(QuantLib::Array& mask, const TimeSeries& tsp) = 0;     // returns initial guesses for 
																									  // non-held model parameters; 
																				  // other parameters are fixed at current values

		// These remaining functions are defined for TimeSeriesModel, and do not necessarily need to be overridden
		void SetMask(QuantLib::Array& m) { estimation_mask_ = m; }
		static QuantLib::Array param_mask(const QuantLib::Array& parms, const QuantLib::Array& mask);

		//static
		//QuantLib::Array param_expand(const other_vector* x, const QuantLib::Array& curparms, const QuantLib::Array& mask);

		template <typename T>
		static QuantLib::Array param_expand(const T& x, const QuantLib::Array& curparms, const QuantLib::Array& mask)
		{
			QuantLib::Array retval = curparms;
			int i, j;
			for (i = 0, j = 0; i < curparms.size(); ++i)
				if (mask[i])
				{
					retval[i] = x[j++];
				}
			return retval;
		}

		void StreamMLESummary(std::ostringstream&, const QuantLib::Matrix& parmcovs, bool get_parameter_cov);
		virtual void StreamParameterName(std::ostringstream& strm, int parmnum) = 0;     // text descrip. of parm (no ends!)
		// constructs cts-time representation
		// and streams it
		virtual bool RenderCtsVersionInto(std::ostringstream& output, std::ostringstream& error, double delta) = 0;

		virtual bool CheckModel(bool flipit = false) = 0;
		[[nodiscard]]
		double GetLogLikelihood() const;
		[[nodiscard]]
		double GetAICC() const;
		[[nodiscard]]
		double GetAIC() const;
		[[nodiscard]]
		double GetBIC() const;
#ifdef _ALLOW_SPECTRAL_METHODS
		virtual QuantLib::Array ComputeSpectralDensity(QuantLib::Array& omegas) = 0;                // model spec. dens. eval'd at omegas
#endif
	};


	// here's a generic function used in MLE
	template<typename _arrayType>
	double minus_log_like_for(const _arrayType x, void* minparms)
	{
		type_check<_arrayType>();
		auto* ic = static_cast<MinimizerInfoContainer*>(minparms);

		// take vector x and expand it into a proper parameter vector
		TimeSeriesModel* ap = ic->tsmp;
		const QuantLib::Array curp = ap->ParameterBundle();
		QuantLib::Array p = TimeSeriesModel::param_expand(x, curp, ic->themask);

		// set parameters in current model
		ap->UnbundleParameters(p);

		// compute penalty for invalid parms
		QuantLib::Array penalty = ap->ValidityConstraint();
		double ppenalty = 0.0;
		for (int i = 0; i < penalty.size(); ++i) {
			ppenalty += penaltymultiplier * (penalty[i] < 0 ? -penalty[i] : 0.0);
		}
		// now map parameters to valid parms to likelihood evaluation works
		ap->CheckModel(true);  // make sure parameters are OK

		// and compute -log like + penalty for constraints
		const double retval = -ic->tsmp->ComputeLogLikelihood(*ic->tsp) + ppenalty;

		// now restore parameters (just in case CheckModel changed them)
		ap->UnbundleParameters(p);

		return retval;
	}
}