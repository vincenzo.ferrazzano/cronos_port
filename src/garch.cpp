/* -------------------------------------------------------------------
 *
 * Copyright 2004,2005,2006 Anthony Brockwell
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * -------------------------------------------------------------------
 *
 * This module contains the source code defining
 * the GARCHModel class, derived from TimeSeriesModel.
 */
#include "pch.hpp"
#include "garch.hpp"

#include "poly.hpp"

TS::GARCHModel::GARCHModel()
	: TimeSeriesModel()
{
	p_ = q_ = 0;
	as_.resize(1);
	as_[0] = 1.0;
	bs_.resize(1);
	mean_ = 0.0;
	//loglikelihood_is_valid = false;
}

void TS::GARCHModel::SetParameters(int p0, int q0, QuantLib::Array& as0, QuantLib::Array& bs0, double m)
{
	int i;
	p_ = p0;   q_ = q0;
	as_.resize(p_ + 1);
	bs_.resize(q_);
	for (i = 0; i <= p_; ++i)
		as_[i] = as0[i];
	for (i = 0; i < q_; ++i)
		bs_[i] = bs0[i];
	mean_ = m;
}

void TS::GARCHModel::GetParameters(int& p1, int& q1, QuantLib::Array& as1, QuantLib::Array& bs1, double& mu) const
{
	p1 = p_;   q1 = q_;
	as1 = as_;   bs1 = bs_;
	mu = mean_;
}

double TS::GARCHModel::LogPrior(bool& isok) const
{
	int i;
	double tx = 0.0;
	isok = as_[0] > 0.0;
	for (i = 1; i <= p_; ++i)
	{
		isok &= as_[i] >= 0.0;
		tx += as_[i];
	}
	for (i = 0; i < q_; ++i)
	{
		isok &= bs_[i] >= 0.0;
		tx += bs_[i];
	}
	isok &= (tx < 1.0);
	return 0.0;
}

QuantLib::Array TS::GARCHModel::ParameterBundle() const
{
	int i;
	QuantLib::Array pvec(p_ + q_ + 2);
	pvec[0] = mean_;
	for (i = 0; i <= p_; ++i)
		pvec[i + 1] = as_[i];
	for (i = 0; i < q_; ++i)
		pvec[i + p_ + 2] = bs_[i];
	return pvec;
}

void TS::GARCHModel::UnbundleParameters(const QuantLib::Array& pvec)
{
	int i;
	mean_ = pvec[0];
	for (i = 0; i <= p_; ++i)
		as_[i] = pvec[i + 1];
	for (i = 0; i < q_; ++i)
		bs_[i] = pvec[i + p_ + 2];
}

void TS::GARCHModel::StreamParameterName(std::ostringstream& os, int pnum)
{
	if (pnum == 0)
		os << /*"\u03bc*/ "= mean";
	else if (pnum <= (p_ + 1))
		os << /*"\u03b1*/"(" << (pnum - 1) << ")";
	else
		os << /*"\u03b2*/"(" << (pnum - p_ - 1) << ")";
}

double TS::GARCHModel::GetVar() const
{
	int i;
	double tx = as_[0], beta = 0.0;
	for (i = 1; i <= p_; ++i)
		beta += as_[i];
	for (i = 0; i < q_; ++i)
		beta += bs_[i];
	tx /= (1.0 - beta);
	return tx;
}

void TS::GARCHModel::BayesianFit(int new_p, int new_q, const TimeSeries& ts,
                                 void (*callback)(int, void*), void* cb_parms)
{
	int i;
	double ty=0;
	double delta;
	bool accept;
	bool isok;

	// start with IIDN model
	const double mu = ts.SampleMean();
	QuantLib::Array new_as(new_p + 1);
	QuantLib::Array new_bs(new_q + 1);
	QuantLib::Array acf(1);

	ts.ComputeSampleACF(acf, nullptr, 0);
	ql_zeros(new_as);
	ql_zeros(new_bs);
	new_as[0] = acf[0];
	SetParameters(new_p, new_q, new_as, new_bs, mu);
	QuantLib::Array pvec = ParameterBundle();

	// create copy of timeseries with missing values filled in
	TimeSeries filled;
	for (i = 0; i < ts.GetN(); ++i)
	{
		if (!ts.IsMissing(i))
			filled.Append(ts[i]);
		else
			filled.Append(norm_random(MT,mu, new_as[0]));
	}

	// now iterate
	QuantLib::Matrix allparms = ql_transpose_vec(pvec);
	double tx = ComputeLogLikelihood(filled) + LogPrior(isok);
	for (int iteration = 0; iteration < iterationlimit_; ++iteration)
	{
		// register the iteration
		if (callback != nullptr)
			(*callback)(2, cb_parms);

		// do an update for each of the parameters
		// make sure they preserve variance!
		for (i = 1; i <= new_p + new_q; ++i)
		{
			double scale = GetVar();
			delta = norm_random(MT, 0, 0.0001);
			pvec[i + 1] += delta;
			UnbundleParameters(pvec);
			scale /= GetVar();
			pvec[1] *= scale;        // scale by changing a[0]
			UnbundleParameters(pvec);
			LogPrior(accept);  // if it's invalid, don't accept
			if (accept)
			{
				ty = ComputeLogLikelihood(filled) + LogPrior(isok);
				accept = (std::log(MT.next().value) < (ty - tx));
			}
			if (accept)
				tx = ty;
			else
			{
				pvec[i + 1] -= delta;
				pvec[1] /= scale;
				UnbundleParameters(pvec);
			}
		}

		// do a variance update (pvec[0] = sigma^2)
		delta = norm_random(MT, 0, 0.0001);
		delta = exp(delta);
		pvec[1] *= delta;
		UnbundleParameters(pvec);
		ty = ComputeLogLikelihood(filled) + LogPrior(isok);
		if (std::log(MT.next().value) < (ty - tx))
			// accept
			tx = ty;
		else
		{
			pvec[1] /= delta;
			UnbundleParameters(pvec);
		}

		// update the missing values in filled
		// ???

		// record results
		ql_append_bottom(allparms, pvec);
	}

	// now fitted model is posterior mean
	ql_zeros(pvec);
	for (i = 100; i < iterationlimit_; ++i)
		for (int j = 0; j < pvec.size(); ++j)
			pvec[j] += allparms[i][j];
	pvec = pvec * (1.0 / (iterationlimit_ - 100));


	UnbundleParameters(pvec);
}

int TS::GARCHModel::FitModel(const TimeSeries& ts, const int method, const int ilimit,
                             void (*itercallback)(int, void*), void* cb_parms,
                             std::ostringstream& msg, std::ostringstream& supplemental,
                             bool get_parameter_cov)
{
	QuantLib::Array localas(p_ + 1), localbs(q_);
	QuantLib::Array acf(1), mask;
	QuantLib::Matrix parmcovs;

	iterationlimit_ = ilimit;
	mask = GetMask();  // get current mask (pad if necessary with default values)

	if (method == METHOD_MLE)
	{
		if (ts.HasAnyMissing())
		{
			msg << "Unable to fit GARCH model when time series has missing observations."
				<< std::ends;
			return UNABLE;
		}

		parmcovs = MLEFit(ts, mask, itercallback, cb_parms, get_parameter_cov);
		StreamMLESummary(supplemental, parmcovs, get_parameter_cov);

		msg << "GARCH model fit in " << iterationcounter_ << " iterations." << std::ends;

		CheckModel(false);
	}
	if (method == METHOD_BAYESIAN)
	{
		BayesianFit(p_, q_, ts, itercallback, cb_parms);
		msg << "MCMC estimation carried out with " << ilimit
			<< " iterations." << std::ends;
		supplemental << "Analysis of MCMC output not yet implemented." << std::ends;
	}
	return SUCCESS;
}

void TS::GARCHModel::SimulateInto(TimeSeries& ts, int n, bool ovw) const
{
	int i, nn;


	// clean up time series
	if (!ovw)
	{
		nn = n;
		ts.ClearOut();
		for (i = 0; i < nn; ++i)
			ts.Append(0);
	}
	else
		nn = ts.GetN();

	// simulate
	QuantLib::Array acf(1);
	ComputeACFPACF(acf, nullptr, 0);     // get variance
	QuantLib::Array nu(nn);
	nu[0] = acf[0];
	ts[0] = norm_random(MT, mean_, acf[0]);
	for (i = 1; i < nn; ++i)
	{
		nu[i] = OneStepVariance(&ts[i - 1], &nu[i - 1], i - 1, acf[0]);
		ts[i] = norm_random(MT, mean_, nu[i]);
	}
}

QuantLib::Matrix TS::GARCHModel::SimulatePredictive(const TimeSeries& ts, int nsteps, int nsims) const
{
	// note: handling of missing values is not optimal.
	//       they are just filled in with draws from one-step predictive d-ns
	//       (instead of the full-conditional distributions)

	int i;
	const int n0 = ts.GetN();
	const int nn = n0 + nsteps;
	QuantLib::Matrix retval(nsims, nsteps);

	// simulate
	QuantLib::Array acf(1);
	ComputeACFPACF(acf, nullptr, 0);     // get variance
	QuantLib::Array nu(nn), new_ts(nn);

	for (int sim_num = 0; sim_num < nsims; ++sim_num)
	{
		nu[0] = acf[0];
		if (ts.IsMissing(0))
			new_ts[0] = norm_random(MT, mean_, acf[0]);
		else
			new_ts[0] = ts[0];
		for (i = 1; i < nn; ++i)
		{
			nu[i] = OneStepVariance(&new_ts[i - 1], &nu[i - 1], i - 1, acf[0]);
			if (ts.IsMissing(i) || i >= n0)
				new_ts[i] = norm_random(MT, mean_, nu[i]);
			else
				new_ts[i] = ts[i];
		}
		for (i = 0; i < nsteps; ++i)
			retval[sim_num][i] = new_ts[i + n0];
	}

	// return tail subvector
	return retval;
}

void TS::GARCHModel::RenderInto(std::ostringstream& os)
{
	int i;
	os << "\\b";  // make it bold
	os << "GARCH(" << p_ << "," << q_ << ") Model" << std::endl;
	os << "\\n";  // make it normal
	os << "X(t) = \\gs\\n(t) Z(t)" << std::endl;
	os << "\\gs\\n(t)\\s2\\n = ";
	os << std::setiosflags(std::ios::fixed) << std::setprecision(3) << as_[0];
	for (i = 1; i <= p_; ++i)
		os << " + " << std::setiosflags(std::ios::fixed) << std::setprecision(3) << as_[i] << "X(t-" << i << ")\\s2\\n";
	for (i = 1; i <= q_; ++i)
		os << " + " << std::setiosflags(std::ios::fixed) << std::setprecision(3) << bs_[i - 1] << "\\gs(t-" << i << ")\\s2\\n";
	os << std::endl << "Mean = " << std::setprecision(5) << mean_ << ",  " << std::endl;
	if (loglikelihood_is_valid_)
	{
		os << std::endl << std::endl << "Log Likelihood = " << GetLogLikelihood() << std::endl;
		os << "AICC = " << GetAICC() << std::endl;
	}
	else
	{
		os << std::endl << std::endl << "Log Likelihood = <Not Computed>" << std::endl;
		os << "AICC = <Not Computed>" << std::endl;
	}
}

bool TS::GARCHModel::RenderCtsVersionInto(std::ostringstream&/*output*/, std::ostringstream& error, double /*delta*/)
{
	error << "This feature is not yet supported for GARCH models." << std::ends;
	return false;
}

void TS::GARCHModel::ComputeACFPACF(QuantLib::Array& acf, QuantLib::Array* pacf, bool normalize) const
{

	double tx;
	ql_zeros(acf);
	if (pacf != nullptr)
	{
		(*pacf)[0] = 1.0;
		ql_zeros(*pacf);
	}
	if (normalize)
		acf[0] = 1.0;
	else
	{
		tx = 1.0 - ql_sum(as_) + as_[0];
		if (bs_.size() > 0)
			tx -= ql_sum(bs_);
		tx = as_[0] / tx;
		acf[0] = tx;
	}
}

void TS::GARCHModel::Forecast(const TimeSeries& tp, int nsteps,
                              double* fret, double* fmse)
{
	int i;
	const int n = tp.GetN();
	QuantLib::Array acf(1);


	ComputeACFPACF(acf, nullptr, 0);  // get variance only

	// compute predictive variances
	QuantLib::Array nu(n + nsteps);
	nu[0] = acf[0];
	for (i = 1; i < n + nsteps; ++i)
		nu[i] = OneStepVariance(&tp[i - 1], &nu[i - 1], i - 1, acf[0]);


	// fill in forecasts
	for (i = 0; i < nsteps; ++i)
	{
		if (fret != nullptr)
			fret[i] = mean_;
		if (fmse != nullptr)
			fmse[i] = nu[i + n];
	}
}

void TS::GARCHModel::ComputeStdResiduals(const TimeSeries& tp, TimeSeries& resids)
{
	int i;
	const int n = tp.GetN();
	QuantLib::Array acf(1);


	ComputeACFPACF(acf, nullptr, 0);  // get variance only

	// compute predictive variances
	QuantLib::Array nu(n);
	nu[0] = acf[0];
	for (i = 1; i < n; ++i)
		nu[i] = OneStepVariance(&tp[i - 1], &nu[i - 1], i - 1, acf[0]);

	// resids are one-step predictors divided by their predictive variances
	resids.ClearOut();
	for (i = 0; i < n; ++i)
		resids.Append((tp[i] - mean_) / sqrt(nu[i]));
}

double TS::GARCHModel::OneStepVariance(double* xt, double* st, int t, double gamma0) const
{
	// This function returns conditional variance of x[t+1], given x[t]=*xt and sigma^2[t]=*st,
	// ..., x[0]=*(xt-t) and sigma^2[0]=*(st-t).  We assume that there is no valid
	// data stored at *(xt-t-k) or *(st-t-k) for k>=1.  Note how this function
	// is overridden in the derived class EGARCHModel.

	double sum = 0;
	int j, m = std::max(p_, q_);
	if (t + 1 >= m)
	{
		sum = as_[0];
		for (j = 1; j <= p_; ++j)
			sum += as_[j] * (xt[1 - j] - mean_) * (xt[1 - j] - mean_);
		for (j = 0; j < q_; ++j)
			sum += bs_[j] * st[-j];
		return sum;
	}
	else
	{
		sum = as_[0];
		for (j = 1; j <= p_; ++j)
			if (1 - j >= -t)
				sum += as_[j] * (xt[1 - j] - mean_) * (xt[1 - j] - mean_);
			else
				sum += as_[j] * gamma0;
		for (j = 0; j < q_; ++j)
			if (j >= -t)
				sum += bs_[j] * st[-j];
			else
				sum += bs_[j] * gamma0;
		return sum;
	}
}

double TS::GARCHModel::ComputeLogLikelihood(const TimeSeries& tp) const
{
	// computes conditional log-likelihood
	int i, m = std::max(p_, q_), nn = tp.GetN();
	double tx = 0;
	QuantLib::Array st(nn), acf(1);

	if (nn == 0)
		return 0.0;

	ComputeACFPACF(acf, nullptr, 0);
	for (i = 0; i < m; ++i)
		st[i] = acf[0];
	for (i = m; i < nn; ++i)
	{
		st[i] = OneStepVariance(&tp[i - 1], &st[i - 1], i - 1, acf[0]);
		tx += lognorm_density(tp[i], mean_, st[i]);
	}

	tx *= static_cast<double>(nn) / (nn - m);   // correct for length of time series (so that AICC makes sense)

	double loglikelihood = tx;
	//loglikelihood_is_valid_ = true;

	return loglikelihood;
}

void TS::GARCHModel::ComputeAdditionalStats(const TimeSeries&)
{
	//Not sure if those are defined
	bic_ = 0.0;
	aic_ = 0.0;
	aicc_ = -2 * loglikelihood_ + 2 * (p_ + 1 + q_);
}

bool TS::GARCHModel::CheckModel(bool flipit)
{
	bool retval;
	if (!flipit)
	{
		QuantLib::Array tv = ValidityConstraint();
		retval = (tv[0] >= 0) && (tv[1] >= 0);
	}
	else // actually fix it
	{
		int i;
		double sum = 0.0;
		for (i = 0; i <= p_; ++i)
		{
			as_[i] = fabs(as_[i]);
			if (i > 0)
				sum += as_[i];
		}
		for (i = 0; i < q_; ++i)
		{
			bs_[i] = fabs(bs_[i]);
			sum += bs_[i];
		}
		if (sum > (1 - 1e-10))
		{
			double scale = (1 - 1e-10) / sum;
			for (i = 1; i <= p_; ++i)
				as_[i] *= scale;
			for (i = 1; i <= q_; ++i)
				bs_[i - 1] *= scale;
		}
		retval = true;
	}
	return retval;
}

QuantLib::Array TS::GARCHModel::ValidityConstraint() const
{
	int i;
	QuantLib::Array retval(2);

	// first constraint: all parms non-neg.
	// put in minimum
	retval[0] = as_[0] - zero;
	for (i = 1; i <= p_; ++i)
		if (as_[i] < retval[0])
			retval[0] = as_[i];
	for (i = 0; i < q_; ++i)
		if (bs_[i] < retval[0])
			retval[0] = bs_[i];

	// 2nd: 1-\sum
	retval[1] = 1.0;
	for (i = 1; i <= p_; ++i)
		retval[1] -= as_[i];
	for (i = 0; i < q_; ++i)
		retval[1] -= bs_[i];

	return retval;
}
#ifdef _ALLOW_SPECTRAL_METHODS
QuantLib::Array GARCHModel::ComputeSpectralDensity(QuantLib::Array& omegas) const
// GARCH is special case of WN->hence has flat spectrum
{
	int i;
	QuantLib::Array retval(omegas.size()), acf(1);
	ComputeACFPACF(acf, NULL, false);  // get the variance
	for (i = 0; i < omegas.size(); ++i)
		retval[i] = acf[0] / (2 * M_PI);

	return retval;
}
#endif

QuantLib::Array TS::GARCHModel::GetMask() const
{
	int i;
	const size_t ll = p_ + q_ + 2;
	QuantLib::Array retval(ll);
	for (i = 0; i < std::min(estimation_mask_.size(), ll); ++i)
		retval[i] = estimation_mask_[i];
	for (; i < p_ + q_ + 2; ++i)
		retval[i] = 1;
	return retval;
}

QuantLib::Array TS::GARCHModel::GetDefaultParameters(QuantLib::Array& mask, const TimeSeries& tsp)
// can modify mask if necessary, for instance so as not to use search for best mean
{
	int i;
	const int nobs = tsp.GetN();
	const int pstar = std::max(p_, q_);
	QuantLib::Array parms = ParameterBundle(), acf(1);

	// mean is handled differently
	if (mask[0]) // mean is to be estimated
	{
		mask[0] = 0;
		parms[0] = tsp.SampleMean();
	}

	// initialize all non-masked parameters now
	tsp.ComputeSampleACF(acf, nullptr, 0);

	// first find a reasonable starting point
	QuantLib::Array localas(p_ + 1), localbs(q_ + 1);
	if (p_ + q_ > 0)
	{
		// set up a simple regression
		QuantLib::Matrix X(nobs - pstar, pstar + 1), XtX;
		QuantLib::Array y(nobs - pstar);
		for (i = pstar; i < nobs; ++i)
		{
			y[i - pstar] = tsp[i] * tsp[i];
			for (int j = 0; j < pstar; ++j)
				X[i - pstar][j + 1] = tsp[i - j - 1] * tsp[i - j - 1];
			X[i - pstar][0] = 1.0;
		}
		//y = transpose(X) * y;
		//XtX = transpose(X) * X;
		//XtX.symm_solve_system(&y);
		y = ql_solve_ls(X, y);
		if (y[0] < zero)
			y[0] = zero;
		for (i = 1; i < y.size(); ++i)
			if (y[i] < 0)
				y[i] = 0.0;
		localas[0] = y[0];
		for (i = 0; i < p_; ++i)
			if (i < q_)
				localas[i + 1] = y[i + 1] / 2.0;
			else
				localas[i + 1] = y[i + 1];
		for (i = 0; i < q_; ++i)
			if (i < p_)
				localbs[i] = y[i + 1] / 2.0;
			else
				localbs[i] = y[i + 1];
	}
	else
		localas[0] = acf[0];

	// copy all free parameters into return vector
	for (i = 0; i <= p_; ++i)
		if (mask[i + 1])
			parms[i + 1] = localas[i];
	for (i = 0; i < q_; ++i)
		if (mask[i + p_ + 2])
			parms[i + p_ + 2] = localbs[i];

	return parms;
}


//---------------------------------------------------+
//       EGARCH stuff follows                        |
//---------------------------------------------------+
TS::EGARCHModel::EGARCHModel()
	: GARCHModel()
{
	gs.resize(100);
	ql_zeros(gs);
}

int TS::EGARCHModel::FitModel(TimeSeries* ts, const int method, const int ilimit,
                              void (*itercallback)(int, void*), void* cb_parms,
                              std::ostringstream& msg, std::ostringstream& supplemental,
                              bool get_parameter_cov)
{
	QuantLib::Array localas(p_ + 1), localbs(q_);
	QuantLib::Array acf(1);
	QuantLib::Matrix parmcovs;

	iterationlimit_ = ilimit;
	QuantLib::Array mask = GetMask();  // get current mask (pad if necessary with default values)
	auto res = UNABLE;
	if (method == METHOD_MLE)
	{
		if (ts->HasAnyMissing())
		{
			msg << "Unable to fit EGARCH model when time series has missing observations."
				<< std::ends;
			return res;
		}

		parmcovs = MLEFit(*ts, mask, itercallback, cb_parms, get_parameter_cov);
		StreamMLESummary(supplemental, parmcovs, get_parameter_cov);

		msg << "EGARCH model fit in " << iterationcounter_ << " iterations." << std::ends;

		CheckModel(false);
		res=SUCCESS;
	}
	else if (method == METHOD_BAYESIAN)
	{
		msg << "MCMC estimation of EGARCH models is not yet supported." << std::ends;
	}
	return res;
}

void TS::EGARCHModel::RenderInto(std::ostringstream& os)
{
	int i;
	os << "\\b";  // make it bold
	os << "EGARCH(" << p_ << "," << q_ << ") Model" << std::endl;
	os << "\\n";  // make it normal
	os << "X(t) = \\gs\\n(t) Z(t)" << std::endl;
	os << "log(\\gs\\n(t)\\s2\\n) = ";
	os << std::setiosflags(std::ios::fixed) << std::setprecision(3) << as_[0];
	for (i = 1; i <= p_; ++i)
		os << " + " << std::setiosflags(std::ios::fixed) << std::setprecision(3) << as_[i] << "[|Z(t-" << i << ")| + "
		<< gs[i] << "Z(t-" << i << ")]";
	for (i = 1; i <= q_; ++i)
		os << " + " << std::setiosflags(std::ios::fixed) << std::setprecision(3) << bs_[i - 1] << "log(\\gs(t-" << i << ")\\s2\\n)";
	os << std::endl << "Mean = " << std::setprecision(5) << mean_ << ",  " << std::endl;
	if (loglikelihood_is_valid_)
	{
		os << std::endl << std::endl << "Log Likelihood = " << GetLogLikelihood() << std::endl;
		os << "AICC = " << GetAICC() << std::endl;
	}
	else
	{
		os << std::endl << std::endl << "Log Likelihood = <Not Computed>" << std::endl;
		os << "AICC = <Not Computed>" << std::endl;
	}
}

double TS::EGARCHModel::OneStepVariance(double* xt, double* st, int t, double /*gamma0*/) const
{
	double sum = 0, zt, tx;
	int j;
	const int m = std::max(p_, q_);
	if (t + 1 >= m)
	{
		sum = as_[0];
		for (j = 1; j <= p_; ++j)
		{
			zt = xt[1 - j] / sqrt(st[1 - j]);
			sum += as_[j] * (fabs(zt) + gs[j] * zt);
		}
		for (j = 0; j < q_; ++j)
			sum += bs_[j] * std::log(st[-j]);
	}
	else
	{
		// find E(log(sigma_t^2)) first
		sum = as_[0];
		for (j = 1; j <= p_; ++j)
			sum += 0.797 * as_[j];
		for (j = 1, tx = 1.0; j <= q_; ++j)
			tx -= bs_[j - 1];
		tx = sum / tx;  // tx is expected value

		sum = as_[0];
		for (j = 1; j <= p_; ++j)
		{
			if (1 - j >= -t)
			{
				zt = xt[1 - j] / sqrt(st[1 - j]);
				sum += as_[j] * (fabs(zt) + gs[j] * zt);
			}
			else
				sum += as_[j] * 0.797885;
		}
		for (j = 0; j < q_; ++j)
			if (-j >= -t)
				sum += bs_[j] * std::log(st[-j]);
			else
				sum += bs_[j] * tx;
	}
	// simple thresholding to prevent numerical over/under-flows
	if (sum > 20)
		return exp(20);
	else
		if (sum < -200)
			return exp(-200);
		else
			return exp(sum);
}

void TS::EGARCHModel::ComputeACFPACF(QuantLib::Array& acf, QuantLib::Array* pacf, bool normalize) const
{
	int j;
	double tx;

	ql_zeros(acf);
	if (pacf != nullptr)
	{
		(*pacf)[0] = 1.0;
		ql_zeros(*pacf);
	}
	if (normalize)
		acf[0] = 1.0;
	else
	{
		// find E(log(sigma_t^2)) first
		double sum = as_[0];
		for (j = 1; j <= p_; ++j)
			sum += 0.797 * as_[j];
		for (j = 1, tx = 1.0; j <= q_; ++j)
			tx -= bs_[j - 1];
		tx = sum / tx;  // tx is expected value

		acf[0] = exp(tx); // incorrect!
	}
}

void TS::EGARCHModel::StabilizeBs()
{
	int i;
	int q2 = 0;

	// first construct moving average polynomial
	for (i = 0; i < q_; ++i)
		if (fabs(bs_[i]) > zero)
			q2 = i + 1;					// p2 is the effective MA order
	if (q2 == 0)
		return;

	QuantLib::Array cp(q_ + 1);
	cp[0] = 1.0;
	for (i = 1; i <= q_; ++i)
		cp[i] = -bs_[i - 1];

	auto roots = FindComplexRoot(q2, cp);

	// now find minimum magnitude
	double min = abs(roots[0]);
	for (i = 1; i <= q2; ++i)
		if (abs(roots[i - 1]) < min)
			min = abs(roots[i - 1]);
	if (min > 1 + root_margin)
	{
		return;
	}

	for (i = 1; i <= q2; ++i)
	{
		double tx = abs(roots[i - 1]);
		double ty = arg(roots[i - 1]);
		if (tx < 1 + root_margin)
			tx = 1 + root_margin;
		roots[i - 1] = std::polar(tx, ty);
	}
	auto recon = poly::CoefficientsOf(q2, roots);
	for (i = 0; i < q2; ++i)
		bs_[i] = -std::real(recon[i + 1] / recon[0]);
}

bool TS::EGARCHModel::CheckModel(bool flipit)
{
	bool retval;
	QuantLib::Array tv = ValidityConstraint();
	if (!flipit)
	{
		retval = (tv[0] >= 0);
	}
	else // actually fix it
	{
		if (tv[0] < 0) // then it's not OK
			StabilizeBs();
		retval = true;
	}
	return retval;
}

QuantLib::Array TS::EGARCHModel::ValidityConstraint() const
{
	QuantLib::Array retval(1);

	// first constraint: beta_i roots outside unit circle
	int i, q2 = 0;
	double min;

	// first construct polynomial from b coefficients
	for (i = 0; i < q_; ++i)
		if (fabs(bs_[i]) > zero)
			q2 = i + 1;					// q2 is the effective order, is < q if bs_q=0
	if (q2 == 0)
		min = 1e6;                                  // a bad approximation to \infty
	else
	{
		QuantLib::Array cp(q2 + 1);
		cp[0] = 1.0;
		for (i = 1; i <= q2; ++i)
			cp[i] = -bs_[i - 1];
		auto roots = FindComplexRoot(q2, cp);

		// now find minimum magnitude
		min = abs(roots[0]);
		for (i = 1; i <= q2; ++i)
		{
			double tx = abs(roots[i - 1]);
			if (tx < min)
				min = tx;
		}
	}

	retval[0] = min - 1 - root_margin;
	return retval;
}

QuantLib::Array TS::EGARCHModel::ParameterBundle() const
{
	int i;
	QuantLib::Array pvec(2 * p_ + q_ + 2);
	pvec[0] = mean_;
	pvec[1] = as_[0];
	for (i = 1; i <= p_; ++i)
	{
		pvec[i + 1] = as_[i];
		pvec[i + p_ + 1] = gs[i];
	}
	for (i = 0; i < q_; ++i)
		pvec[i + 2 * p_ + 2] = bs_[i];
	return pvec;
}

void TS::EGARCHModel::UnbundleParameters(const QuantLib::Array& pvec)
{
	int i;
	mean_ = pvec[0];
	as_[0] = pvec[1];
	for (i = 1; i <= p_; ++i)
	{
		as_[i] = pvec[i + 1];
		gs[i] = pvec[i + p_ + 1];
	}
	for (i = 0; i < q_; ++i)
		bs_[i] = pvec[i + 2 * p_ + 2];
}

void TS::EGARCHModel::StreamParameterName(std::ostringstream& os, int pnum)
{
	if (pnum == 0)
		os << /*"\u03bc*/" = mean";
	else if (pnum <= (p_ + 1))
		os << /*"\u03b1*/"(" << (pnum - 1) << ")";
	else if (pnum <= (2 * p_ + 1))
		os << /*"\u03b6*/"(" << (pnum - 1 - p_) << ")";
	else
		os << /*"\u03b2*/"(" << (pnum - 2 * p_ - 1) << ")";
}


void TS::EGARCHModel::SetParameters(int p0, int q0, QuantLib::Array& as0, QuantLib::Array& gs0, QuantLib::Array& bs0, double m)
{
	int i;
	p_ = p0;   q_ = q0;
	as_.resize(p_ + 1);
	gs.resize(p_ + 1);
	bs_.resize(q_);
	as_[0] = as0[0];
	for (i = 1; i <= p_; ++i)
	{
		as_[i] = as0[i];
		gs[i] = gs0[i];
	}
	for (i = 0; i < q_; ++i)
		bs_[i] = bs0[i];
	mean_ = m;
}

void TS::EGARCHModel::GetParameters(int& p1, int& q1, QuantLib::Array& as1, QuantLib::Array& gs1, QuantLib::Array& bs1, double& mu) const
{
	p1 = p_;   q1 = q_;
	as1 = as_;   bs1 = bs_;   gs1 = gs;
	mu = mean_;
}


QuantLib::Array TS::EGARCHModel::GetMask() const
// returns estimation_mask, padded with defaults
{
	int i;
	const size_t ll = 2 * p_ + q_ + 2;
	QuantLib::Array retval(ll);
	for (i = 0; i < std::min(estimation_mask_.size(), ll); ++i)
		retval[i] = estimation_mask_[i];
	for (; i < 2 * p_ + q_ + 2; ++i)
		retval[i] = 1;
	return retval;
}

QuantLib::Array TS::EGARCHModel::GetDefaultParameters(QuantLib::Array& mask, const TimeSeries& tsp)
// returns initial guesses, in form of bundled parameter vector
{
	int i, j, pstar = std::max(p_, q_), nobs = tsp.GetN();
	QuantLib::Array parms = ParameterBundle(), acf(1);
	double tx;

	// mean is handled differently
	if (mask[0]) // mean is to be estimated
	{
		mask[0] = 0;
		parms[0] = tsp.SampleMean();
	}

	// initialize all non-masked parameters now
	tsp.ComputeSampleACF(acf, nullptr, false);

	QuantLib::Array localas(p_ + 1);
	QuantLib::Array localbs(q_ + 1);
	QuantLib::Array localgs(p_ + 1);
	QuantLib::Array zts(nobs);
	for (i = 0; i < nobs; ++i)
		zts[i] = tsp[i];
	//tx = zts.var();
	tx = ql_var(zts);
	zts = zts * (1.0 / sqrt(tx));
	if (p_ + q_ > 0)
	{
		// set up a simple regression
		QuantLib::Matrix X(nobs - pstar, 2 * p_ + q_ + 1), XtX;
		QuantLib::Array y(nobs - pstar);
		for (i = pstar; i < nobs; ++i)
		{
			y[i - pstar] = std::log(tsp[i] * tsp[i] + 1e-6);    // log(X_t^2+1e-6) is a proxy for log(sigma^2)
			X[i - pstar][0] = 1.0;
			for (j = 0; j < p_; ++j)
			{
				X[i - pstar][j + 1] = fabs(zts[i - j - 1]);
				X[i - pstar][j + p_ + 1] = zts[i - j - 1];
			}
			for (j = 0; j < q_; ++j)
				X[i - pstar][j + 2 * p_ + 1] = std::log(tsp[i - j - 1] * tsp[i - j - 1] + 1e-6);
		}
		//y = transpose(X) * y;
		//XtX = transpose(X) * X;
		//XtX.symm_solve_system(&y);
		y = ql_solve_ls(X, y);

		localas[0] = y[0];
		for (i = 0; i < p_; ++i)
		{
			localas[i + 1] = y[i + 1];
			localgs[i + 1] = y[i + 1 + p_];
		}
		for (i = 0; i < q_; ++i)
			localbs[i] = y[i + 2 * p_ + 1];
	}
	else
		localas[0] = std::log(acf[0]);

	// copy all free parameters into return vector
	// (note: we already did the mean above)
	for (i = 0; i <= p_; ++i)
	{
		if (mask[i + 1])
			parms[i + 1] = localas[i];
		if (mask[i + p_ + 1])
			parms[i + p_ + 1] = localgs[i + 1];
	}
	for (i = 0; i < q_; ++i)
		if (mask[i + 2 * p_ + 2])
			parms[i + 2 * p_ + 2] = localbs[i];

	return parms;
}