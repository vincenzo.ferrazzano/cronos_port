/*
 *
 * polynomial format:
 *  a_0 + a_1 z + a_2 z^2 + ...
 *  a_0 = first coefficient, a_1 = second coefficient, ...
 */

#include "pch.hpp"
#include "poly.hpp"
#include "zsolve.hpp"
/*
	* It creates a new complex array containing all the roots of the specified polynomial.
	*/
std::vector<complex> TS::poly::RootsOf(int order, const QuantLib::Array& coeffs)
{
	std::vector<complex> roots(order);
	QuantLib::Array z(order * 2);
	complex_workspace gwp(order + 1);
	poly_complex_solve(coeffs, order + 1, gwp, z);
	//unpacking the results
	for (int i = 0; i < order; ++i)
		roots[i] = complex(z[2 * i], z[2 * i + 1]);
	return roots;
}

/*
* Given the roots of a polynomial, it constructs the coefficients
* of the polynomial in which the $a_p$ coefficient is
* equal to 1.
*/
std::vector<complex> TS::poly::CoefficientsOf(int order, const std::vector<complex>& roots, complex normalize)
// This procedure works recursively
{
	std::vector<complex> coeffs(order + 1);
	if (order == 1)
	{
		coeffs[0] = -roots[0];
		coeffs[1] = 1.0;
	}
	else
	{
		auto sub = CoefficientsOf(order - 1, roots);
		const complex c1 = -1.0;
		complex c2 = sub[0];
		complex c3 = roots[order - 1];
		coeffs[0] = c1 * c2 * c3;
		for (int i = 1; i < order; ++i) {
			coeffs[i] = c1 * sub[i] * c3 + sub[i - 1];
		}
		coeffs[order] = 1.0;
	}
	for (int i = 0; i <= order; ++i)
		coeffs[i] *= normalize;
	return coeffs;
}

