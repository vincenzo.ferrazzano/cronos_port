/*
 * -------------------------------------------------------------------
 *
 * Copyright 2004, 2005, 2006 Anthony Brockwell
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * -------------------------------------------------------------------
 */

 /*
 ** July 2006 update: adapted TimeSeries class to handle
 ** multivariate time series (but not yet TimeSeriesModel).
 ** is backwards-compatible, i.e. ts[i] returns i'th element of
 ** first component TS if multivariate
 */
#pragma once

#ifndef TIMESERIES_H
#define TIMESERIES_H

namespace TS {
	const int maxorder = 100;
	const int maxtransforms = 50;
	const int METHOD_MLE = 1;
	const int METHOD_BAYESIAN = 2;

	class TimeSeriesModel;
	class ARMAModel;
	///Compute the sample PACF using the Durbin-Levinson algorithm (p.169 B&D)
	/**
	 *
	 */
	inline
		void ACFtoPACF(const QuantLib::Array& acvf /**< Source ACF*/, QuantLib::Array& pacfstorage /**< Target PACF*/)
	{
		int j;
		const size_t maxlag = acvf.size() - 1;
		if (maxlag <= 0)
		{
			pacfstorage[0] = 0;
			return;
		}

		QuantLib::Array phis(maxlag);
		QuantLib::Array phis2(maxlag);
		phis[0] = acvf[1] / acvf[0];
		pacfstorage[0] = 1.0;
		pacfstorage[1] = phis[0];
		double vi = acvf[0];
		vi = vi * (1 - phis[0] * phis[0]);
		for (int i = 2; i <= maxlag; ++i) // i=iteration number
		{
			for (j = 0; j < i - 1; ++j)
				phis2[j] = phis[i - j - 2];
			double phinn = acvf[i];
			for (j = 1; j < i; ++j)
				phinn -= phis[j - 1] * acvf[i - j];
			phinn /= vi;
			for (j = 0; j < i - 1; ++j)
				phis[j] -= phinn * phis2[j];
			vi = vi * (1 - phinn * phinn);
			pacfstorage[i] = phis[i - 1] = phinn;
		}
	}

	// The first class is simply a data structure containing
	// observed values of a time series.

	class TimeSeries {
	protected:
		void ParseDescriptor(char*);
		std::string title_ = "";
		std::string description_ = "";

	public:
		bool anymissing = false;
		int current_length = 0;
		int nallocated = 0;
		int n_components = 1;
		int current_component = 0;
		QuantLib::Matrix data;
		QuantLib::Matrix missing;
	public:
		TimeSeries();
		TimeSeries(int nobs, int n_components);
		~TimeSeries() = default;

		void ClearOut(int dimension = 0);              // clears it out and sets dimension (0 means leave alone)
		void Append(double x, bool msng = false);
		void SetValues(int pos, double x, bool miss = false);
		void Append(QuantLib::Array& x, QuantLib::Array& msng);
		[[nodiscard]]
		int GetN() const { return current_length; }
		[[nodiscard]]
		int GetDim() const { return n_components; }
		[[nodiscard]]
		bool HasAnyMissing() const;
		[[nodiscard]]
		QuantLib::Array GetDataVector() const;
		[[nodiscard]]
		QuantLib::Matrix  GetDataMatrix() const;
		[[nodiscard]]
		QuantLib::Array GetMissingVector() const;
		[[nodiscard]]
		QuantLib::Matrix  GetMissingMatrix() const;
		void SetMissingVector(QuantLib::Array&);
		void SetMissing(int t, bool msng);
		void Clip(int n0, int n1);
		void RenderInto(std::ostringstream&) const;
		std::string& GetDescription() { return description_; }
		std::string& GetTitle() { return title_; }
		void SetTitle(std::string& t) { title_ = t; }
		//double* GetData() const { return &data[current_component][0]; }
		//double& operator[](int index) const { return data[current_component][index]; }
		[[nodiscard]]
		double* GetData() const;
		double& operator[](int index) const;
		//bool IsMissing(int t) const { return missing[current_component][t]; }
		//bool IsMissing(int component, int t) const { return missing[component][t]; }
		[[nodiscard]]
		bool IsMissing(int t) const;
		[[nodiscard]]
		bool IsMissing(int component, int t) const;


		// useful stuff
		[[nodiscard]]
		double SampleMean() const;
		[[nodiscard]]
		double SampleMin() const;
		void ComputeSampleACF(QuantLib::Array& acf, QuantLib::Array* pacf,
			bool normalize_acf = true) const;
		QuantLib::Matrix GetInnovations(double& vhat, int m) const;
		TimeSeries& operator=(const TimeSeries& other);
		bool operator==(const TimeSeries& other) const;

		friend std::ostream& operator<<(std::ostream&, TimeSeries&);
		friend std::istream& operator>>(std::istream&, TimeSeries&);
#ifdef _ALLOW_SPECTRAL_METHODS
		QuantLib::Array ComputePeriodogram(QuantLib::Array* omegas = nullptr) const;
#endif
	};
}
#endif
