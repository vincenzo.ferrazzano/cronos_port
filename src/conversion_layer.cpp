#include "pch.hpp"
#include "conversion_layer.hpp"
#include "centered_fin_diff.hpp"
#include "poly.hpp"

int nlopt_res_converter(const nlopt::result& nlopt_res)
{
	int res = FAILURE;
	if (nlopt_res == nlopt::result::MAXEVAL_REACHED)
	{
		res = NONCONVERGENT;
	}
	else
	{
		if (nlopt_res > 0)
		{
			res = SUCCESS;
		}
	}
	return res;
}

funpar::funpar(double (* target_fun)(const double*, void*), void* add_ptr, int* iteration,
               void (* callback)(int, void*)): target_fun(target_fun),
                                               add_ptr{add_ptr},
                                               evaluation{iteration},
                                               callback{callback}
{
}

QuantLib::Matrix ql_transpose_vec(const QuantLib::Array& vec)
{
	QuantLib::Matrix res(1, vec.size());
	for (size_t p = 0; p < vec.size(); ++p)
	{
		res(p, 0) = vec[p];
	}
	return res;
}

QuantLib::Real ql_var(const QuantLib::Array& vec)
{
	const auto l = vec.size();
	const auto avg = ql_sum(vec) / l;
	const auto sum2 = ql_sum(vec * vec);
	return sum2 / 2 - avg * avg;
}

QuantLib::Matrix ql_resize(const QuantLib::Matrix& m, int b, int a)
{
	QuantLib::Matrix res(b, a);

	const auto ysize = m.rows();
	const auto xsize = m.columns();
	// always clips or pads
	for (int i = 0; i < (ysize > b ? ysize : b); ++i)
		for (int j = 0; j < (xsize > a ? xsize : a); ++j)
		{
			if ((i < b) && (j < a))
			{
				if ((i < ysize) && (j < xsize))
					res(i, j) = m(i, j);
				else
					res(i, j) = 0;
			}
		}
	return res;
}

QuantLib::Real norm_random(const QuantLib::MersenneTwisterUniformRng& local_mt, QuantLib::Real mean, QuantLib::Real std)
{
	const static QuantLib::InverseCumulativeNormal inverse;
	return mean + inverse(local_mt.next().value) * std;
}

QuantLib::Array to_array(const QuantLib::Matrix& mat)
{
	if (mat.rows() != 1)
	{
		throw std::runtime_error("WTF!");
	}
	QuantLib::Array res(mat.columns());
	size_t p = 0;
	for (const auto& v : mat)
	{
		res[p++] = v;
	}
	return res;
}

double lognorm_density(double x, double mu, double sig2)
{
	return (-log(sqrt(2 * M_PI * sig2)) - 0.5 * (x - mu) * (x - mu) / sig2);
}

QuantLib::Array ql_solve_ls(const QuantLib::Matrix& X, const QuantLib::Array&)
{
	/*y = transpose(X) * y;
	XtX = transpose(X) * X;
	XtX.symm_solve_system(&y);*/
	QuantLib::Array res(X.columns());
	throw std::runtime_error("Not implemented! return a beta!");
	//return res;
}

QuantLib::Array ql_solve_system(const QuantLib::Matrix& X, const QuantLib::Array& y)
{
	//TODO: change to something else!
	return QuantLib::inverse(X) * y;
}

QuantLib::Matrix ql_submatrix(const QuantLib::Matrix& mat, int r1, int c1, int r2p1, int c2p1)
{
	QuantLib::Matrix tm(r2p1 - r1, c2p1 - c1);
	for (int i = r1; i < r2p1; ++i)
	{
		// copy a row
		//double* tx = &(stuff[i * ncols() + c1]);
		double* tx = &mat(i, c1);
		for (int j = 0; j < c2p1 - c1; ++j)
			tm[i - r1][j] = tx[j];
	}
	return tm;
}

QuantLib::Matrix ql_append_bottom(const QuantLib::Matrix& upper_mat, const QuantLib::Matrix& appending, int trans)
{
	QuantLib::Matrix bottom;
	if (trans)
	{
		bottom = QuantLib::transpose(bottom);
	}
	else
	{
		bottom = appending;
	}
	const size_t nr = upper_mat.rows();
	const size_t nc = upper_mat.columns();
	const size_t upper_len = nr * nc;

	const size_t nrb = bottom.rows();
	const size_t ncb = bottom.columns();
	const size_t bottom_len = nrb * ncb;

	if (nc != ncb)
	{
		std::cout << "Matrix::ql_append_bottom() error, mismatching # of columns" << std::endl;
		throw std::runtime_error("Matrix::ql_append_bottom() error, mismatching # of columns");
	}

	QuantLib::Matrix res(nr + nrb, nc);
	auto* dest = res.begin();
	const auto* src = upper_mat.begin();
	for (size_t p = 0; p < upper_len; ++p)
	{
		dest[p] = src[p];
	}
	const auto* src2 = bottom.begin();
	for (size_t p = 0; p < bottom_len; ++p)
	{
		dest[upper_len + p] = src2[p];
	}
	return res;
}

QuantLib::Matrix ql_append_bottom(const QuantLib::Matrix& upper_mat, const QuantLib::Array& appending)
{
	const size_t nr = upper_mat.rows();
	const size_t nc = upper_mat.columns();
	const size_t upper_len = nr * nc;

	const size_t ncb = appending.size();

	if (nc != ncb)
	{
		std::cout << "Matrix::ql_append_bottom() error, mismatching # of columns" << std::endl;
		throw std::runtime_error("Matrix::ql_append_bottom() error, mismatching # of columns");
	}

	QuantLib::Matrix res(nr + 1, nc);
	auto* dest = res.begin();
	const auto* src = upper_mat.begin();
	for (size_t p = 0; p < upper_len; ++p)
	{
		dest[p] = src[p];
	}
	const auto* src2 = appending.begin();
	for (size_t p = 0; p < ncb; ++p)
	{
		dest[upper_len + p] = src2[p];
	}
	return res;
}

std::vector<complex> FindComplexRoot(int order, const QuantLib::Array& coeffs)
{
	return TS::poly::RootsOf(order, coeffs);
}

int partial_derivative(double (* fun)(const double*, void*), void* param, const double* x, unsigned pos, unsigned n,
                       double h, double& result, double& abserr)
{
	packed_fun pkt_fun;
	pkt_fun.univariate_function = [n,pos,x, fun,param](double val, void* /*param*/)
	{
		QuantLib::Array other_val(n);
		std::copy_n(x, n, other_val.begin());
		other_val[pos] = val;
		const double res = fun(other_val.begin(), param);
		return res;
	};

	return deriv_central(&pkt_fun, x[pos], h, &result, &abserr);
}

QuantLib::Array opt_routine_nlopt(void* add_func_parameter, const QuantLib::Array& subparms, const int nump, const int,
                                  int& iteration_counter, int& error_code, void (* callback)(int, void*),
                                  double (* minus_log_like_for)(const double*, void*), void*, nlopt::algorithm algo)
{
	nlopt::opt opt(algo, nump);

	auto t_fun = [](unsigned n, const double* x, double* grad, void* packed_shit)
	{
		auto* unpacked = static_cast<funpar*>(packed_shit);
		const auto fun = unpacked->target_fun;
		void* par = unpacked->add_ptr;
		int& evals = *unpacked->evaluation;
		auto callback = unpacked->callback;

		if (callback != nullptr)
		{
			//add something
		}
		if (grad != nullptr)
		{
			double abs_error;
			for (unsigned int i = 0; i < n; ++i)
			{
				const double h = std::max(fabs(x[i] / 100.0), 0.01);
				partial_derivative(fun, par, x, i, n, h, grad[i], abs_error);
			}
		}
		const double val = fun(x, par);
		evals++;
		return val;
	};

	void* packed_shit = new funpar(minus_log_like_for,
	                               add_func_parameter, &iteration_counter, callback);
	opt.set_min_objective(t_fun, packed_shit);
	const double epsabs = 1e-6;
	opt.set_xtol_abs(epsabs);
	std::vector<double> x, step;
	for (int i = 0; i < nump; ++i)
	{
		x.push_back(subparms[i]);
		step.push_back(0.2);
	}
	opt.set_initial_step(step);
	double minf;
	nlopt::result result = nlopt::FAILURE;
	try
	{
		result = opt.optimize(x, minf);
	}
	catch (std::exception& e)
	{
		std::cout << "nlopt failed: " << e.what() << std::endl;
	}
	error_code = nlopt_res_converter(result);
	auto res = QuantLib::Array(x.begin(), x.end());
	return res;
}
