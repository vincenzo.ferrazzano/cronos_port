﻿#include "pch.hpp"
#include "timeseries_model.hpp"
#include "centered_fin_diff.hpp"


//-----------------------------------------------+
// TimeSeriesModel stuff                         :
//-----------------------------------------------+

// the following two functions apply a mask to the
// parameter vector: the mask contains 1s if the respective
// component is to be included, 0s otherwise

QuantLib::Array TS::TimeSeriesModel::param_mask(const QuantLib::Array& parms, const QuantLib::Array& mask)
{
	int i, j, sz = static_cast<int>(ql_sum(mask) + 0.5); // round it
	QuantLib::Array sub(sz);
	for (i = 0, j = 0; i < parms.size(); ++i)
		if (mask[i])
			sub[j++] = parms[i];
	return sub;
}

double TS::TimeSeriesModel::GetLogLikelihood() const
{
	return loglikelihood_;
}

double TS::TimeSeriesModel::GetAICC() const
{
	return aicc_;
}

double TS::TimeSeriesModel::GetAIC() const
{
	return aic_;
}

double TS::TimeSeriesModel::GetBIC() const
{
	return bic_;
}

//Evaluate the ll on a slighly changed valued: x instead what is contained in parms,
double ql_other_partial(double x, void* parms)
{
	const auto* mp = static_cast<TS::ql_mll_parm*>(parms);
	QuantLib::Array z = mp->x;
	z[mp->i] = x;
	const double retval = TS::minus_log_like_for<QuantLib::Array&>(z, mp->minparms);
	return retval;
}

//Computes the i-th derivate at y.
double ql_ith_partial(const QuantLib::Array& y, void* parms)
{
	auto* mp = static_cast<TS::ql_mll_parm*>(parms);
	const int i = mp->i;
	const double tx = y[i];
	auto backup_copy = mp->x;
	mp->x = y;
	double h = std::max(fabs(tx / 100.0), 0.01);
	double abserr;
	double partial_derivative;
	packed_fun F;
	F.univariate_function = &ql_other_partial;
	F.params = parms;
	deriv_central(&F, tx, h, &partial_derivative, &abserr);
	mp->x = backup_copy;
	return partial_derivative;
}


//--- end of messy 2nd derivative calculation stuff ---
//---  (apart from section below at end of MLEFit)  --

double ql_partial_minus_log_like_for(double x, void* parms)
// parms is a pointer to a block of 2 pointers: one to an int = index, one to a QL::Array
// this should return the i-th partial derivative!
{
	auto* mp = static_cast<TS::ql_mll_parm*>(parms);
	QuantLib::Array y = mp->x;
	y[mp->j] = x;
	const double retval = ql_ith_partial(y, parms);
	return retval;
}

//Function that actually computes the Hessian
void compute_hessian(const QuantLib::Array& evaluation_point,
	TS::MinimizerInfoContainer& ic,
	QuantLib::Matrix& hessian_storage,
	QuantLib::Array& error_storage,
	void (*callback)(int, void*),
	void* callback_add_params)
{
	const size_t nump = hessian_storage.rows();
	if (hessian_storage.columns() != nump || nump != error_storage.size()) {
		throw std::runtime_error("Dimension mismatch");
	}
	TS::ql_mll_parm temp_holder;
	packed_fun F;
	F.univariate_function = &ql_partial_minus_log_like_for;
	F.params = &temp_holder;
	temp_holder.x = evaluation_point;
	temp_holder.minparms = &ic;
	for (int i = 0; i < nump; ++i)
	{
		temp_holder.i = i;
		for (int j = 0; j <= i; ++j)
		{
			if (callback != nullptr)
				(*callback)(1, callback_add_params);
			temp_holder.j = j;
			const double tx = evaluation_point[temp_holder.j];
			const double h = std::max(fabs(tx / 100), 0.01);
			double pd, abserr;
			deriv_central(&F, tx, h, &pd, &abserr);
			hessian_storage[temp_holder.i][temp_holder.j] = pd;
			hessian_storage[temp_holder.j][temp_holder.i] = pd;
		}
		// now check to see if the i'th parameter is "ok", i.e.
		// its row doesn't make the matrix non-positive-definite
		const QuantLib::Matrix sub = ql_submatrix(hessian_storage, 0, 0, i + 1, i + 1);
		//sub.singular(&tx);  // compute determinant
		double tx = QuantLib::determinant(sub);
		if (tx <= 0) // we have a problem!  mark this parameter as bad!
		{
			for (int j = 0; j < i; ++j)
				hessian_storage[i][j] = hessian_storage[j][i] = 0.0;
			hessian_storage[i][i] = 1.0;
			error_storage[i] = 1.0;   // mark this as bad!
		}
	}
}

QuantLib::Matrix TS::TimeSeriesModel::MLEFit(const TimeSeries& ts,
	QuantLib::Array& mask,
	void (*callback)(int, void*),
	void* cb_parms,
	bool get_parameter_cov)
{
	const size_t total_parameters = mask.size();
	MinimizerInfoContainer ic;

	iterationcounter_ = 0;

	//
	// Basic idea: Use a standard miminization scheme on -(log likelihood), but
	// add a penalty function for degree of constraint violation
	// Successively: minimize, then check to see if constraints are violated,
	//               if they are, increase the penalty function and try again, otherwise stop.
	//
	//

	QuantLib::Array parms = GetDefaultParameters(mask, ts);
	const auto nump = static_cast<int>(ql_sum(mask));
	QuantLib::Array initialparms = parms;

	ic.tsp = &ts;
	ic.tsmp = this;
	ic.themask = mask;

	QuantLib::Array res;
	QuantLib::Matrix bigcov(total_parameters, total_parameters);  // used to return cov. matrix of estimator

	if (nump > 0) // i.e. at least one param to estimate
	{
		penaltymultiplier = ts.GetN() / 10.0;
		bool isvalid = false;
		iterationcounter_ = 0;
		while (!isvalid)
		{
			penaltymultiplier *= 3;
			UnbundleParameters(initialparms);
			parms = initialparms;

			QuantLib::Array subparms = param_mask(parms, mask);
			res = opt_routine_nlopt(&ic,
				subparms,
				nump,
				iterationlimit_,
				iterationcounter_,
				error_code_,
				callback,
				&minus_log_like_for,
				cb_parms,
				nlopt::algorithm::LN_NELDERMEAD);
			// copy results back into the current model
			parms = param_expand(res, parms, mask);
			UnbundleParameters(parms);
			isvalid = CheckModel(false);
		}

		// evaluate log-likelihood !!!
		this->loglikelihood_ = ComputeLogLikelihood(ts);
		this->loglikelihood_is_valid_ = true;
		// After fitting, we set the additional stats
		ComputeAdditionalStats(ts);

		// also compute partial derivatives of log-likelihood, 
		// for information matrix to get parameter uncertainty
		QuantLib::Array badones(nump);
		if (get_parameter_cov)
		{
			//Filling with zeros
			ql_zeros(bigcov);
			ql_zeros(badones);            // start with no problems in Hessian
			QuantLib::Matrix parmcov(nump, nump);
			//compute the hessian
			compute_hessian(res, ic, parmcov, badones, callback, cb_parms);
			std::cout << parmcov;
			// translate back into full matrix of parameter estimate covariances
			parmcov = QuantLib::inverse(parmcov);

			// and finally, map into full size matrix by padding
			// with zeroes for non-estimated parameters
			QuantLib::Matrix tm(total_parameters, nump);
			ql_zeros(tm);

			for (int i = 0, j = 0; i < total_parameters; ++i)
				if (mask[i])
				{
					tm[i][j] = 1.0 - badones[j];  // if it's a bad parameter, zero out the row/col of cov. matrix
					++j;
				}
			bigcov = tm * parmcov * transpose(tm);
			for (int i = 0, j = 0; i < total_parameters; ++i)
				if (mask[i])
				{
					if (badones[j])
						bigcov[i][i] = -1.0;       // in fact, mark bad ones with Var=-1
					  // this differentiates from Var=0 case
					  // which simply means parm. unestimated
					++j;
				}
		} // end of if get_parameter_cov
	}
	else // WN model
		std::cout << "Error: no parameters to estimate!" << std::endl;

	UnbundleParameters(parms); // restore parms

	return bigcov;
}
