/*
 * -------------------------------------------------------------------
 *
 * Copyright 2004, 2005, 2006 Anthony Brockwell
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * -------------------------------------------------------------------
 */

#ifndef GARCH_H
#define GARCH_H
#include "timeseries_model.hpp"

namespace TS
{



	const double root_margin = 0.001;


	class GARCHModel : public TimeSeriesModel {
	protected:
		int p_, q_;
		double mean_;
		QuantLib::Array as_;
		QuantLib::Array bs_;

		virtual double OneStepVariance(double* xt, double* st, int t, double gamma0) const;
		// returns one-step predictive volatility, t starts at 0, goes up to T-1.

	public:
		GARCHModel();
		~GARCHModel() override = default;

		int FitModel(const TimeSeries& ts, const int method, int numits,
			void (*itercallback)(int, void*), void* cb_parms,
			std::ostringstream& msg, std::ostringstream& supplementalinfo,
			bool get_parameter_cov) override;
		void SimulateInto(TimeSeries&, int n, bool ovw) const override;
		[[nodiscard]]
		QuantLib::Matrix SimulatePredictive(const TimeSeries& ts, int nsteps, int nsims) const override;
		void RenderInto(std::ostringstream&) override;
		bool RenderCtsVersionInto(std::ostringstream& output, std::ostringstream& error, double delta) override;
		void ComputeACFPACF(QuantLib::Array& acf, QuantLib::Array* pacf, bool normalizeacf = true) const override;
		void Forecast(const TimeSeries&, int nsteps, double* fret, double* fmse) override;
		void ComputeStdResiduals(const TimeSeries& data, TimeSeries& resids) override;
		[[nodiscard]]
		double ComputeLogLikelihood(const TimeSeries& tp) const override;
		void ComputeAdditionalStats(const TimeSeries&) override;
		[[nodiscard]]
		QuantLib::Array ParameterBundle() const override;
		void UnbundleParameters(const QuantLib::Array& v) override;

		[[nodiscard]]
		QuantLib::Array GetMask() const override;          // returns estimation_mask, padded with defaults
		QuantLib::Array GetDefaultParameters(QuantLib::Array& mask, const TimeSeries& tsp) override;     // returns initial guesses
		void StreamParameterName(std::ostringstream& os, int pnum) override;
		bool CheckModel(bool flipit) override;
		[[nodiscard]]
		QuantLib::Array ValidityConstraint() const override;

		[[nodiscard]]
		double GetVar() const;
		void BayesianFit(int new_p, int new_q, const TimeSeries& ts,
			void (*callback)(int, void*) = nullptr, void* cb_parms = nullptr);
		double LogPrior(bool& isok) const;
		void SetParameters(int p, int q, QuantLib::Array& as, QuantLib::Array& bs, double mu);
		void GetParameters(int& p, int& q, QuantLib::Array& as, QuantLib::Array& bs, double& mu) const;
		bool IsCausal(bool causalify = false);

#ifdef _ALLOW_SPECTRAL_METHODS
		QuantLib::Array ComputeSpectralDensity(QuantLib::Array& omegas) const;
#endif
	};


	class EGARCHModel : public GARCHModel
	{
	protected:
		QuantLib::Array gs;             // has as and bs inherited from GARCH, gs are new

		double OneStepVariance(double* xt, double* st, int t, double gamma0) const override;
		// returns one-step predictive volatility, t starts at 0, goes up to T-1.

		void StabilizeBs();    // makes sure b polynomial is well-behaved


	public:
		EGARCHModel();
		~EGARCHModel() override = default;

		int FitModel(TimeSeries* ts, const int method, int numits,
			void (*itercallback)(int, void*), void* cb_parms,
			std::ostringstream& msg, std::ostringstream& supplementalinfo,
			bool get_parameter_cov);

		void ComputeACFPACF(QuantLib::Array& acf, QuantLib::Array* pacf, bool normalizeacf = true) const override;
		void RenderInto(std::ostringstream&) override;
		bool CheckModel(bool flipit) override;
		[[nodiscard]]
		QuantLib::Array ValidityConstraint() const override;


		[[nodiscard]]
		QuantLib::Array ParameterBundle() const override;
		void UnbundleParameters(const QuantLib::Array& v) override;

		void SetParameters(int p, int q, QuantLib::Array& as, QuantLib::Array& gs, QuantLib::Array& bs,
			double mu);
		void GetParameters(int& p, int& q, QuantLib::Array& as, QuantLib::Array& gs, QuantLib::Array& bs,
			double& mu) const;
		[[nodiscard]]
		QuantLib::Array GetMask() const override;          // returns estimation_mask, padded with defaults
		QuantLib::Array GetDefaultParameters(QuantLib::Array& mask, const TimeSeries& tsp) override;     // returns initial guesses
		void StreamParameterName(std::ostringstream& os, int pnum) override;
#ifdef _ALLOW_SPECTRAL_METHODS

#endif
	};


#endif
}
