#pragma once
#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#define _SILENCE_CXX17_STRSTREAM_DEPRECATION_WARNING
#define _SILENCE_CXX17_OLD_ALLOCATOR_MEMBERS_DEPRECATION_WARNING
#endif
//#define _ALLOW_SPECTRAL_METHODS
#include <cmath>
#include <strstream>
#include <complex>
#include <string>
#include <fstream>
typedef std::complex<double> complex;
//Disable warnings on libraries we do not control
#pragma warning( push )
#pragma warning( disable : 4616 4458 )
#include <ql/math/randomnumbers/all.hpp>
#include <ql/math/generallinearleastsquares.hpp>
#include <ql/math/matrix.hpp>
#include <nlopt.hpp>
#pragma warning( pop )
const inline QuantLib::MersenneTwisterUniformRng MT(42);
//Necessary to use whittle/spectral methods
#ifdef _ALLOW_SPECTRAL_METHODS
#include <gsl/gsl_sf.h>
#include <gsl/gsl_fft_real.h>
#include <gsl/gsl_fft_halfcomplex.h>
#endif

inline const int alloc_unit = 512;
inline const int KFburnin = 12;
const double zero = 1e-10;
#define TINY 1e-20
