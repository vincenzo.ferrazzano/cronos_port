#pragma once
#include "pch.hpp"

struct packed_fun
{
	std::function<double(double, void*)> univariate_function;
	void* params = nullptr;
};

inline
void central_deriv_aux_(const packed_fun* f, double x, double h,
	double* result, double* abserr_round, double* abserr_trunc)
{
	/* Compute the derivative using the 5-point rule (x-h, x-h/2, x,
	   x+h/2, x+h). Note that the central point is not used.
	   Compute the error using the difference between the 5-point and
	   the 3-point rule (x-h,x,x+h). Again the central point is not
	   used. */
	constexpr auto eps = std::numeric_limits<double>::epsilon();

	const double fm1 = f->univariate_function(x - h, f->params);
	const double fp1 = f->univariate_function(x + h, f->params);

	const double fmh = f->univariate_function(x - h / 2.0, f->params);
	const double fph = f->univariate_function(x + h / 2.0, f->params);

	const double r3 = 0.5 * (fp1 - fm1);
	const double r5 = (4.0 / 3.0) * (fph - fmh) - (1.0 / 3.0) * r3;
	const double e3 = (fabs(fp1) + fabs(fm1)) * eps;
	const double e5 = 2.0 * (fabs(fph) + fabs(fmh)) * eps + e3;

	/* The next term is due to finite precision in x+h = O (eps * x) */

	double dy = std::max(fabs(r3 / h), fabs(r5 / h)) * (fabs(x) / h) * eps;

	/* The truncation error in the r5 approximation itself is O(h^4).
	   However, for safety, we estimate the error from r5-r3, which is
	   O(h^2).  By scaling h we will minimise this estimated error, not
	   the actual truncation error in r5. */

	*result = r5 / h;
	*abserr_trunc = fabs((r5 - r3) / h); /* Estimated truncation error O(h^2) */
	*abserr_round = fabs(e5 / h) + dy;   /* Rounding error (cancellations) */
}

inline
int deriv_central(const packed_fun* f, const double x, double h,
	double* result, double* abserr)
{
	double r_0, round, trunc, error;
	central_deriv_aux_(f, x, h, &r_0, &round, &trunc);
	error = round + trunc;

	if (round < trunc && (round > 0 && trunc > 0))
	{
		double r_opt, round_opt, trunc_opt, error_opt;

		/* Compute an optimised stepsize to minimize the total error,
		   using the scaling of the truncation error (O(h^2)) and
		   rounding error (O(1/h)). */

		const double h_opt = h * pow(round / (2.0 * trunc), 1.0 / 3.0);
		central_deriv_aux_(f, x, h_opt, &r_opt, &round_opt, &trunc_opt);
		error_opt = round_opt + trunc_opt;

		/* Check that the new error is smaller, and that the new derivative
		   is consistent with the error bounds of the original estimate. */

		if (error_opt < error && fabs(r_opt - r_0) < 4.0 * error)
		{
			r_0 = r_opt;
			error = error_opt;
		}
	}

	*result = r_0;
	*abserr = error;
	return 1;
}