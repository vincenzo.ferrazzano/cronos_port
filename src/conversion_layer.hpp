#pragma once
#include "pch.hpp"

template <typename _arrayType>
constexpr void type_check()
{
	constexpr bool plain = std::is_same<_arrayType, double*>::value || std::is_same<_arrayType, const double*>::value;
	constexpr bool ql = std::is_same<_arrayType, QuantLib::Array&>::value || std::is_same<_arrayType, const QuantLib::Array&>::value;
	static_assert(plain || ql, "not supported!");
}

enum OPT_RES {
	SUCCESS = 1,
	FAILURE = -3,
	NONCONVERGENT = 0,
	UNABLE = -1
};

inline
int nlopt_res_converter(const nlopt::result& nlopt_res);

struct funpar {
	funpar(double (*target_fun)(const double*, void*),
	       void* add_ptr,
	       int* iteration,
	       void (*callback)(int, void*) = nullptr);
	double(*target_fun)(const double*, void*);
	void* add_ptr;
	int* evaluation;
	void (*callback)(int, void*) = nullptr;
};

template< typename T>
void ql_fill(T& mat, QuantLib::Real val)
{
	for (auto& v : mat)
	{
		v = val;
	}
}

template< typename T>
void ql_zeros(T& mat)
{
	ql_fill(mat, 0.0);
}

template< typename T>
QuantLib::Real ql_sum(const T& mat)
{
	QuantLib::Real res = 0.0;
	for (const auto& v : mat)
	{
		res += v;
	}
	return res;
}

QuantLib::Matrix ql_transpose_vec(const QuantLib::Array& vec);

QuantLib::Real ql_var(const QuantLib::Array& vec);

QuantLib::Matrix ql_resize(const QuantLib::Matrix& m, int b, int a);

QuantLib::Real norm_random(const QuantLib::MersenneTwisterUniformRng& local_mt, QuantLib::Real mean = 0.0,
                           QuantLib::Real std = 1.0);

QuantLib::Array to_array(const QuantLib::Matrix& mat);

double lognorm_density(double x, double mu, double sig2);

QuantLib::Array ql_solve_ls(const QuantLib::Matrix& X, const QuantLib::Array& /*y*/);

QuantLib::Array ql_solve_system(const QuantLib::Matrix& X, const QuantLib::Array& y);

QuantLib::Matrix ql_submatrix(const QuantLib::Matrix& mat, int r1, int c1, int r2p1, int c2p1);

QuantLib::Matrix ql_append_bottom(const QuantLib::Matrix& upper_mat, const QuantLib::Matrix& appending, int trans);

QuantLib::Matrix ql_append_bottom(const QuantLib::Matrix& upper_mat, const QuantLib::Array& appending);

std::vector<complex> FindComplexRoot(int order, const QuantLib::Array& coeffs);

int partial_derivative(double (*fun)(const double*, void*), void* param, const double* x, unsigned pos, unsigned n,
                       double h, double& result, double& abserr);

QuantLib::Array opt_routine_nlopt(void* add_func_parameter,
                                  const QuantLib::Array& subparms,
                                  const int nump,
                                  const int /*iteration_limit*/,
                                  int& iteration_counter,
                                  int& error_code,
                                  void (*callback)(int, void*),
                                  double (*minus_log_like_for)(const double*, void*),
                                  void* /*cb_parms*/,
                                  nlopt::algorithm algo = nlopt::LN_NELDERMEAD);
//end TODO
