// run_test.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#define _SILENCE_CXX17_STRSTREAM_DEPRECATION_WARNING
#define _SILENCE_CXX17_OLD_ALLOCATOR_MEMBERS_DEPRECATION_WARNING

#include <iostream>
#include <ql/math/matrix.hpp>
#include "matrix.hpp"
#include "../src/conversion_layer.hpp"
#include "../src/arma_model.hpp"
using namespace TS;
void compare_matrix_flatten(const QuantLib::Matrix& A, const mslib::Matrix& B)
{
	bool check = A.rows() == B.nrows() && A.columns() == B.ncols();
	QuantLib::Real v1 = -1, v2 = -1;
	int r = 0;
	if (check) {
		const size_t len = A.rows() * A.columns();
		auto* a = A.begin();
		const auto* b = B.get_stuff();
		for (r = 0; r < len; r++)
		{
			v1 = a[r];
			v2 = b[r];
			check = check && std::abs(v1 - v2) < 1e-6;
			if (!check)
			{
				break;
			}
		}
	}
	else
	{
		throw std::runtime_error("Different sizes!");
	}
	if (!check)
	{
		std::cout << r << " " << v1 << " " << v2 << "\n";
		throw std::runtime_error("Different matrices in the flatten reps");
	}
}

void compare_matrix_accesser(const QuantLib::Matrix& A, const mslib::Matrix& B)
{
	bool check = A.rows() == B.nrows() && A.columns() == B.ncols();
	if (check) {

		for (int r = 0; r < A.rows(); r++)
		{
			for (int c = 0; c < A.columns(); c++)
			{
				const auto v1 = A(r, c);
				const auto v2 = B[r][c];
				check = check && std::abs(v1 - v2) < 1e-6;
			}
		}
	}
	else
	{
		throw std::runtime_error("Different sizes!");
	}
	if (!check)
	{
		throw std::runtime_error("Different matrices in the API");
	}
}
void compare_matrix(const QuantLib::Matrix& A, const mslib::Matrix& B)
{
	compare_matrix_accesser(A, B);
	compare_matrix_flatten(A, B);
}
void copy_new_into_old(const QuantLib::Matrix& A, mslib::Matrix& B)
{
	const bool check = A.rows() == B.nrows() && A.columns() == B.ncols();
	if (!check)
	{
		throw std::runtime_error("");
	}

	for (int r = 0; r < A.rows(); r++)
	{
		for (int c = 0; c < A.columns(); c++)
		{
			B[r][c] = A(r, c);
		}
	}
}

void copy_old_into_new(QuantLib::Matrix& A, const mslib::Matrix& B)
{
	bool check = A.rows() == B.nrows() && A.columns() == B.ncols();
	if (!check)
	{
		throw std::runtime_error("");
	}

	for (int r = 0; r < A.rows(); r++)
	{
		for (int c = 0; c < A.columns(); c++)
		{
			A(r, c) = B[r][c];
		}
	}
}

void fill_new(QuantLib::Matrix& mat_to_fill)
{
	int p = 0;
	for (auto& v : mat_to_fill)
	{
		v = p++;
	}
}

int matrix_tests()
{
	const int n_rows = 12;
	const int n_cols = 34;
	//Testing matrices_representation and accessing
	QuantLib::Matrix ql_test(n_rows, n_cols);
	fill_new(ql_test);
	mslib::Matrix old_test(n_rows, n_cols);
	copy_new_into_old(ql_test, old_test);
	compare_matrix(ql_test, old_test);



	std::cout << "r:" << ql_test.rows() << ", c:" << ql_test.columns() << "\n";
	std::cout << "r:" << old_test.nrows() << ", c:" << old_test.ncols() << "\n";
	std::cout << ql_test;
	std::cout << old_test << "\n";

	std::cout << "access 1\n: ";
	std::cout << ql_test(1, 2) << " ";
	std::cout << old_test[1][2] << "\n";

	std::cout << "access 2\n: ";
	std::cout << ql_test[1][2] << " ";
	std::cout << old_test[1][2] << "\n";

	for (int r = 1; r < n_rows; r++)
	{
		for (int c = 1; c < n_cols; c++)
		{
			std::cout << r << "," << c << "\n";
			const auto resized_ql = ql_resize(ql_test, r, c);
			auto resized_old = old_test;
			resized_old.resize(r, c);
			compare_matrix(resized_ql, resized_old);
		}
	}

	//Testing append_bottom
	QuantLib::Matrix up_new(5, 10);
	fill_new(up_new);
	const QuantLib::Array bottom_new(10);
	const auto concat_new = ql_append_bottom(up_new, bottom_new);
	mslib::Matrix up_old(5, 10);
	copy_new_into_old(up_new, up_old);
	compare_matrix(up_new, up_old);
	mslib::Vector old_bottom(10);
	up_old.append_bottom(old_bottom, 1);
	compare_matrix(concat_new, up_old);
	return 0;
}

static double compute_ma_acf(int lag, const QuantLib::Array& coeff, double sigma, bool normalized)
{
	const double sigma2 = sigma * sigma;
	const size_t L = coeff.size();
	double res = 0;
	if (lag == 0) {
		res = 1;
		if (!normalized) {
			for (int p = 0; p < L; ++p)
			{
				res += coeff[p] * coeff[p];
			}
			res *= sigma2;
		}
	}
	else {
		if (lag <= L)
		{
			res = coeff[lag - 1];
			for (int j = 0; j < L - lag; ++j)
			{
				res += coeff[j] * coeff[j + lag];
			}
		}
		res = sigma2 * res;
		if (normalized)
		{
			res /= compute_ma_acf(0, coeff, sigma, false);
		}
	}
	return res;
}

TS::ARMAModel fit_model(int p, int q, TS::TimeSeries& ts, int method_type = TS::METHOD_MLE, int num_iteration=5000) {
	TS::ARMAModel model;
	model.SetOrder(p, q);
	model.SetFracDiff(0.0);
	std::ostringstream res, supplemental;

	int result = model.FitModel(ts,
		method_type,
		num_iteration,
		nullptr,
		nullptr,
		res,
		supplemental,
		true);
	return model;
}

void fill_ts(const std::string& filename, TS::TimeSeries& ts)
{
	std::ifstream infile(filename);
	std::cout << infile.good();
	infile >> ts;
}

int main()
{
	const int acf_lags = 12;
	QuantLib::Array cronos_ar_coeff;
	QuantLib::Array cronos_ma_coeff;
	TS::TimeSeries ts;
	const std::string path = "../deps/cronos/data/sunspots.dat";
	fill_ts(path, ts);
	const int q = 10;
	std::cout << q<<"\n";
	auto model = fit_model(0, q, ts, 1, 5000);
	model.GetCoefficients(cronos_ar_coeff, cronos_ma_coeff);
	const double sigma = model.GetSigma();
	QuantLib::Array acf(acf_lags);
	model.ComputeACFPACF(acf, nullptr, true);
	for (int i = 0; i < acf_lags; ++i)
	{
		const double expected_val = compute_ma_acf(i, cronos_ma_coeff, sigma, true);
		bool chk = std::abs(expected_val - acf[i])<1e-6;
		if (!chk)
		{
			std::runtime_error("aaaa");
		}
	}
}