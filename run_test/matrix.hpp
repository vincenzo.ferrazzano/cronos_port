/*
 * -------------------------------------------------------------------
 *
 * Copyright 2004,2005,2006,2007 Anthony Brockwell
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * -------------------------------------------------------------------
 *
 *  Here are declarations of matrix and vector
 *  classes.  Started Sep. 1992, by Anthony Brockwell.
 *
 *  Use of LAPACK routines incorporated around late 2002.
 */
#pragma once

#ifndef alreadymatrix
#define alreadymatrix
#include "../src/pch.hpp"

namespace mslib {

	const int MATLAB_MODE = 0, BINARY_MODE = 1, DISP_MODE = 2, ROW_MODE = 3; // used for I/O

	class Double_Store {
	protected:
		double* stuff;
		int num_refs;
	public:
		Double_Store(int len);
		~Double_Store();
		double& operator[](int i) const
		{
			return stuff[i];
		}
	};

	class Vector;

	class Matrix {
	protected:
		int xsize, ysize, maxels;
		double* stuff;
		int iomode;

	public:
		Matrix();         // no param. constructor
		Matrix(const Matrix&); // copy constructor
		Matrix(int, int, double** dp = nullptr);  // constructors
		~Matrix();        // destructor

		Matrix& operator=(const Matrix&);
		Matrix& operator=(double** dp);
		Matrix& operator=(const double);
		Matrix& operator+=(const Matrix&);
		Matrix& operator-=(const Matrix&);
		Matrix& operator+=(double other);
		Matrix& operator-=(double other);
		Matrix operator+(const Matrix&) const;
		//friend const Matrix operator*(Matrix& left, Matrix& right);
		Matrix operator-(const Matrix&) const;
		Matrix operator*(const Matrix&) const;
		Vector operator*(const Vector&) const;
		Matrix operator*(double) const;
		Matrix operator/(double) const;
		Matrix& operator*=(double);
		Matrix& operator/=(double);
		bool operator<=(const Matrix&) const;	// elementwise comparison
		inline double* operator[](int i) const
		{
			return (&stuff[i * xsize]);
		}
		Vector extract_row(int rownum) const;
		Vector extract_column(int colnum) const;

		Matrix LU_decomp(int* retsign = nullptr, Matrix* permutations = nullptr,
			int* indices = nullptr) const;
		Matrix inverse() const;
		Matrix g4_inverse(double epsilon) const;

		double symm_cond_num() const;   // for symmetric A, returns condition number
		void solve_system(Vector* b) const;     // solve Ax=b, (ret. soln in b)
		void symm_solve_system(Vector* b) const; // same but for symmetric A
		void robust_symm_solve_system(Vector* b) const;  // handles near-singular matrices

		Matrix transpose() const;
		Matrix submatrix(int r1, int c1, int r2p1, int c2p1) const;
		bool singular(double*) const;	// returns determinant and flag
		int nrows() const { return ysize; };
		int ncols() const { return xsize; };
		double* get_stuff() const { return stuff; };
		complex* eigenvalues() const;
		Matrix symm_eigenvectors(double* evals) const;  // a crude routine: LAPACK dsyev should be used when possible
		double norm() const;
		double max() const;
		double min() const;
		Vector mean() const;        // returns mean down the columns
		Matrix covariance() const;  // returns sample covariance (each row is a datapoint)

		void identity() const;
		void zeroes() const;
		void resize(int, int);
		void set_data(double*, int, int, int);
#ifdef USE_LAPACK
		double symm_quad_form(Vector& v);
#endif
		int read_file(const char*);      // read data set as col. vec. and return # points
		void write_file(const char*) const;    // write file
		void set_iomode(int im) { iomode = im; }
		void append_bottom(Matrix& newstuff, int trans = 0);

		friend std::ostream& operator<<(std::ostream&, Matrix&);
		friend std::istream& operator>>(std::istream&, Matrix&);
		double kernel_function(double arg, int n, double sd) const;  // used by quantiles function below
		double kernel_cdf(double arg, int n, double sd) const;       // also used by quantiles f-n
		// returns vector of quantiles down columns,
		// alpha is between 0 and 1
		Vector quantiles(double alpha, bool kernel_smooth = false);
	};

	// Vectors are implemented as a special case of matrices.

	class Vector : public Matrix {
	public:
		Vector();
		Vector(int);

		double& operator[](int i) const { return stuff[i]; }
		Vector& operator=(const Matrix&);
		Vector operator-(Vector) const;
		Vector operator*(const double&) const;
		double dot(const Vector&) const;
		double norm() const;
		double sum() const;
		double mean() const;
		double var() const;
		double angle() const;  // returns angle for 2-d vectors
		Matrix outer(const Vector&) const;
		int size() const { return ysize; }
		void resize(int nrows) { Matrix::resize(nrows, 1); }
		void append(double);
		Vector subvector(int r1, int r2) const;
		void sort() const;     // heapsort elements of vector

		friend std::ostream& operator<<(std::ostream&, Vector&);
		friend std::istream& operator>>(std::istream&, Vector&);
	};

	// Here are some miscellaneous functions

	Matrix blockmatrix(Matrix& A, Matrix& B, Matrix& C, Matrix& D);
	Matrix vblockmatrix(Matrix& A, Matrix& B);
	Matrix hblockmatrix(Matrix& A, Matrix& B);
}

#endif
